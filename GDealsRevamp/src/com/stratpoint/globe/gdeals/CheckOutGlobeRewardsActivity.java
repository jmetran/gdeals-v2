package com.stratpoint.globe.gdeals;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.stratpoint.globe.gdeals.base.BaseActivity;
import com.stratpoint.globe.gdeals.base.Constants;
import com.stratpoint.globe.gdeals.base.GDeals;
import com.stratpoint.globe.gdeals.helpers.AqHelper;
import com.stratpoint.globe.gdeals.helpers.SignatureHelper;
import com.stratpoint.globe.gdeals.models.Deal;
import com.stratpoint.globe.gdeals.models.GlobeRewardsMerchant;
import com.stratpoint.globe.gdeals.models.Payment;
import com.stratpoint.globe.gdeals.models.ResultMessage;
import com.stratpoint.globe.gdeals.utils.AlertUtil;
import com.stratpoint.globe.gdeals.utils.HelperFunctions;
import com.stratpoint.globe.gdeals.utils.JsonUtil;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.app.ProgressDialog;
import android.os.Bundle;

public class CheckOutGlobeRewardsActivity extends BaseActivity{
	
	private Bundle extras;
	
	private String availablePoints;
	private String qrCode;
	private Deal deal;
	
	private HelperFunctions hFunction;
	private AqHelper aqHelper;
	private AlertUtil alertUtil;
	
	private ProgressDialog progress;
	private EditText edit_txt_points;
	
	
//	private LocationAjaxCallback location;
//	protected LocationManager locManager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reward_checkout);
		
		setUpHeader(getString(R.string.reward_instruction_title), null, false, false, true, null);
		
		init();	
		
		edit_txt_points = aq.id(R.id.edit_txt_points).getEditText();
		edit_txt_points.setText(deal.details.dealMinRewardsPoints);
		
		getMerchant();
		aq.id(R.id.txt_view_rewards).text(String.format(getString(R.string.reward_redemption_pts), availablePoints));
		
    }
	
	private void init(){
		hFunction = new HelperFunctions();
		aqHelper = new AqHelper(this);
		alertUtil = new AlertUtil(this);
//		locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		extras = getIntent().getExtras();
		availablePoints = extras.getString("available_points");
		qrCode = extras.getString("qr_code");
		deal = (Deal) hFunction.stringToObject(extras.getString("deal"), Deal.class);
	}
	
	private void getMerchant(){
		progress = new ProgressDialog(this);
		progress.setMessage("Please wait...");
		progress.setCancelable(false);
		progress.show();
			    
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.GLOBE_REWARDS_MERCHANT_INFO+ "?access_token=" +GDeals.appAccessToken()+"&qr_code="+qrCode , "getMerchantCb");  
		aq.progress(progress).ajax(cb);
	}
	
	public void getMerchantCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		
		Gson gson = new GsonBuilder().create();
		if (_status.getCode() == 200){	
			
			GlobeRewardsMerchant merchant = gson.fromJson(JsonUtil.getJSONObject("result", _jo).toString(), GlobeRewardsMerchant.class);
			
			aq.id(R.id.txt_view_name).text(merchant.name);
			aq.id(R.id.txt_view_address).text(merchant.address.getCompleteAddress());
			aq.id(R.id.txt_view_contact_no).text(TextUtils.join("", merchant.details.deliveryNo));
			
		}else if(_status.getCode() == 400){	
			ResultMessage result = gson.fromJson(_status.getError(), ResultMessage.class);
			alert(result.message);
		}else{
			errorMsg(_status.getMessage());
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}		
	}
	
	private boolean isEnteredPointsValid(){
		String consumePoints = edit_txt_points.getText().toString();
		
		if(TextUtils.isEmpty(consumePoints)){
			alert(String.format(getString(R.string.required), getString(R.string.reward_hint_points)));
			return false;
		}
		
		if (Double.parseDouble(deal.details.dealMinRewardsPoints) > Double.parseDouble(consumePoints)) {
//			alertUtil.alertInfo(getString(R.string.rewards_insufficient_notification_title), String.format(getString(R.string.rewards_insufficient_notification_allowed_points), deal.details.dealMinRewardsPoints)  , null);
			alertUtil.alertInfo(getString(R.string.globe_rewards_title), getString(R.string.used_rewards_below_minimum), null);
			return false;
		}else if(Double.parseDouble(availablePoints) < Double.parseDouble(consumePoints)){
			alertUtil.alertInfo(getString(R.string.globe_rewards_title), getString(R.string.rewards_insufficient_notification_title), null);
			return false;
		}
		return true;
	}
	
//	private void checkOut(Location loc){
	private void checkOut(){
		progress = new ProgressDialog(this);
		progress.setMessage("Please wait...");
		progress.setCancelable(false);
		progress.show();
			  
		String accessToken = GDeals.appAccessToken();
//		String lat = String.valueOf(loc.getLatitude());
//		String lon = String.valueOf(loc.getLongitude());
		String lon = "121.027";
		String lat = "14.583333";
		String deal_id = deal.id;
		String consumePoints = edit_txt_points.getText().toString();
		
		Map<String, Object> params = new HashMap<String, Object>();
	    params.put("access_token", accessToken);
		params.put("latitude", lat);
	    params.put("longitude", lon);
	    params.put("points", consumePoints);
	    params.put("qr_code", qrCode);
	    params.put("deal_id", deal_id);
	    params.put("signature", new SignatureHelper().generateSignature(new String[] { accessToken, deal_id, lat, lon, consumePoints, qrCode }));
//	    params.put("signature", new SignatureHelper().generateSignature(new String[] { accessToken, consumePoints, qrCode }));
	    
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.GLOBE_REWARDS_URL, "checkOutCb");  
		cb.params(params);
		cb.method(AQuery.METHOD_POST);
		aq.progress(progress).ajax(cb);
	}
	
	public void checkOutCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		Gson gson = new GsonBuilder().create();
		if(_status.getCode() == 200){	
			
			iUtil.startIntent(GenericThankYouPageActivity.class, new String[] { "paymentType", "deal" }, new Object[] { Payment.TYPE_REWARD_POINTS, deal });
			finish();
		}else if(_status.getCode() == 400){	
			ResultMessage result = gson.fromJson(_status.getError(), ResultMessage.class);
			alert(result.message);
		}else{
			errorMsg(_status.getMessage());
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}		
	}
	/*
	public void getLocation(){
		if (locManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
			location = new LocationAjaxCallback();
			location.weakHandler(this, "locationCb").timeout(40*1000);
			location.async(this);
		}else{
			alert(getString(R.string.gps_network_error));
		}
	}
	
	public void locationCb(String url, Location loc, AjaxStatus _status){
		if(loc != null){
			checkOut(loc);		
		}else{		
			alert(getString(R.string.unable_to_get_location));			
		}
		location.stop();
	}
	*/
	public void onClick(View v){
		switch (v.getId()) {
		case R.id.btn_ok:
			if(isEnteredPointsValid()){
//				getLocation();
				checkOut();
				flurryLogEvent(getString(R.string.checkout));
			}			
			break;
		case R.id.btn_cancel:
			onBackPressed();
			flurryLogEvent("Cancelled");
			break;
		}
	}
	
}
