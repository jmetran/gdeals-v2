package com.stratpoint.globe.gdeals.base;

import com.androidquery.callback.BitmapAjaxCallback;
import com.orm.SugarApp;
import com.stratpoint.globe.gdeals.helpers.SharedPrefHelper;

public class GDeals extends SugarApp{

	private static SharedPrefHelper sharedPref;
	
	@Override
    public void onCreate() {
		super.onCreate();
		sharedPref = new SharedPrefHelper(this);
	}
	
	@Override
	public void onLowMemory(){	
    	BitmapAjaxCallback.clearCache();
    }
	
	public static String appAccessToken(){
		return sharedPref.getString(SharedPrefHelper.ACCESS_TOKEN);
	}
	
	public static String userId(){
		return sharedPref.getString(SharedPrefHelper.USER_ID);
	}
	
	public static String mobileNo(){
		return sharedPref.getString(SharedPrefHelper.MOBILE_NO);
	}
	
}
