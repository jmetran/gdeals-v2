package com.stratpoint.globe.gdeals.base;

import java.util.Arrays;
import java.util.List;

public interface Constants {
	//API URL SETTINGS
	public static final boolean PRODUCTION = true;
	
	//API URL
	public static final String DEV_URL = "http://dev.api.gdeals.ph/";
	public static final String STAGING_URL = "http://staging.api.gdeals.ph/";
	public static final String PRODUCTION_URL = "https://api.gdeals.ph/";

	public static final String GDEAL_FAQ_URL = "http://gdeals.ph/faq";
	
	public static final String BASE_URL = PRODUCTION ? PRODUCTION_URL : STAGING_URL;
//	public static final String BASE_URL = STAGING_URL;
	public static final String REQUEST_VERIFICATION_CODE_URL = BASE_URL + "sms/v3/send_verification_code/";
	
	// OAUTH URL's
	public static final String OAUTH_BASE = "oauth/v2/";
	public static final String REQUEST_ACCESS_TOKEN_URL = BASE_URL + OAUTH_BASE + "request_token";
	public static final String ACCESS_TOKEN_URL = BASE_URL + OAUTH_BASE + "access_token";
	
	//REGISTRATIONS URL's
	public static final String FB_OAUTH_REGISTRATION_URL = BASE_URL + OAUTH_BASE + "facebook";
	public static final String USER_MOBILE_REGISTRATION_URL = BASE_URL + "signup/v3/mobile_number/";
	public static final String VERIFY_MOBILE_NO_URL = BASE_URL + "user/v2/verify_mobile_number/";
	
	//FB TOKEN EXPIRATION UPDATE URL
	public static final String FB_TOKEN_EXPIRATION_UPDATE = BASE_URL + "user/v2/fb_access_token_update";
	
	//MOBILE LOGIN
	public static final String MOBILE_LOGIN_URL = BASE_URL + "oauth/v2/mobile_number/";

	// CATEGORIES
	public static final String CATEGORY_URL = BASE_URL + "category/v1/all/";
	
	//TAGS
	public static final String TAGS_URL = BASE_URL + "user/v2/tags/";
	
	//GDEALS APP OAUTH ID AND SECRET
	public static final String GDEALS_APP_CLIENT_ID = "dc8150c24f4efb24655b56be418ba9ee";
	public static final String GDEALS_APP_CLIENT_SECRET = "037c7b9ab79321a40100848cc44d687e";
	
	//DEALS URL
	public static final String SECTION_FEEDS = "feed/v4/";
	public static final String FEEDS_DEALS = BASE_URL + SECTION_FEEDS + "deals";
	public static final String DEALS_FEATURED_URL = FEEDS_DEALS + "/featured/";
	
	//EVENT DEALS URL
	public static final String EVENT_DEALS_URL = FEEDS_DEALS;
	
	//ANALYTICS
	public static final String SECTION_ANALYTICS = "analytics/v2/";
	public static final String ANALYTICS_DEAL_VIEWS_URL = BASE_URL + SECTION_ANALYTICS + "deal_view/";
	
	//PROFILE
	public static final String PROFILE_UPDATE_URL = BASE_URL + "user/v2/update/";
	
	// CHECKOUT
	public static final String SECTION_DEAL = "deal/v2/";
	//OKAY OKAY
	public static final String PAYMENT_OKAYOKAY_URL = BASE_URL + SECTION_DEAL +"reserve_okayokay_sku/";
	//ADEALS
	public static final String PAYMENT_ADEALS_URL = BASE_URL + SECTION_DEAL + "reserve/";
	//GLOBE REWARDS
	public static final String GLOBE_REWARDS_URL = BASE_URL + "user/v2/globe_reward_points";
	
	public static final String GLOBE_REWARDS_MERCHANT_INFO = BASE_URL + "merchant/v2/rewards_qr_code";
	
	//SEARCH
	public static final String SEARCH_DEAL_DETAILS_URL = BASE_URL + "deal/v2/detail/";
	public static final String SEARCH_DEALS_URL = BASE_URL + "feed/v1/search/deal/";
	
	//VOUCHERS
	public static final String VOUCHERS_URL = BASE_URL + "deal/v2/redemption_summary/";
	
	//PARTICIPATING STORE URL
	public static final String PARTICIPATING_STORE_URL = BASE_URL + "deal/v3/info/";
	
	//PARTICIPATING MERCHANTS URL
	public static final String PARTICIPATING_MERCHANTS_URL = BASE_URL + "feed/v1/participating_merchants/";
	
	//LOCATIONS URL
	public static final String LOCATIONS_URL = BASE_URL + "zap/v2/location_list/";
	
	//DELIVERY ADDRESS URL
	public static final String DELIVERY_ADDRESS_URL = BASE_URL + "user/v1/delivery_address/";
	
	//BANNERS URL
	public static final String BANNER_URL = BASE_URL + "carousel/v2/carousel_valid_list/active";
	
	//FB Permission	
	public static final List<String> FB_READ_PERMISSIONS = Arrays.asList("email user_birthday user_hometown user_location user_relationships user_education_history user_likes user_relationship_details user_groups user_website user_photos user_status read_stream user_events user_checkins");
	public static final List<String> FB_PUBLISH_PERMISSIONS = Arrays.asList("publish_actions publish_stream");
	
	//SUPPORTED DEALS
	public static final String ADEALS = "adeals";
	public static final String ZAP = "zap";
	public static final String GLOBE_REWARDS = "globe_rewards";
	public static final String OKAYOKAY = "okayokay";
	
	//REDIRECT DEALS 
	public static final List<String> REDIRECT_DEALS = Arrays.asList("groupon", "ensogo", "cashcashpinoy");
	
	//OTHERS
	public static final long CACHE_EXPIRE_BY_WEEK = (7 * (24 * 60)) * 60 * 1000;
	public static final long CACHE_DEFAULT_EXPIRE = 5 * 60 * 1000;
	public static final long CACHE_RESET = -1;
	public static final long NO_EXPIRATION = 0;
	
	//NETWORK REQUEST
	public static final int NETWORK_TIMEOUT = 30 * 1000;
	
	public static final String MOBILE_AGENT = "Mozilla/5.0 (Linux; U; Android 2.2) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533 gzip";		
	
	public static final String HMAC_SHA1_KEY = "gdeal.SHA1!";
	
	public static final int VERIFICATION_CODE_LENGTH = 4;
	
	//FLURRY
	public static final String FLURRY_ID = "QHC63TNBDN5P5PNFF8QZ";
	public static final String FLURRY_CLICK_EVENT = "CLICKED";
	
	//GCM PUSH NOTIFICATION
	public static final String SENDER_ID = "450319217375";
	public static final String PUSH_NOTIFICATION_REGISTRATION_URL = BASE_URL + "user/v2/device_token/";
	
	//RECO ENGINE DEFAULTS
	public static final String RECO_ENGINE_ACTION_VIEW = "00";
	public static final String RECO_ENGINE_ACTION_BUY = "01";
	public static final String RECO_ENGINE_ACTION_SHARE = "02";
	
	public static final String RECO_ENGINE_PAGE_LEGEND_LANDING_PAGE = "LP";
	public static final String RECO_ENGINE_PAGE_LEGEND_DEAL_DETAILS = "DD";
	public static final String RECO_ENGINE_PAGE_LEGEND_MOBILE_VOUCHER = "MV";
	
}