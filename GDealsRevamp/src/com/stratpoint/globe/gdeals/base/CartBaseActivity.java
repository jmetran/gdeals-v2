package com.stratpoint.globe.gdeals.base;

import com.androidquery.AQuery;
import com.androidquery.util.AQUtility;
import com.nineoldandroids.view.ViewHelper;
import com.stratpoint.globe.gdeals.R;
import com.stratpoint.globe.gdeals.adapters.QuantityAdapter;
import com.stratpoint.globe.gdeals.helpers.AqHelper;
import com.stratpoint.globe.gdeals.models.CartItem;
import com.stratpoint.globe.gdeals.models.Deal;
import com.stratpoint.globe.gdeals.models.Store;
import com.stratpoint.globe.gdeals.models.StoreDetails;
import com.stratpoint.globe.gdeals.utils.HelperFunctions;
import com.stratpoint.globe.gdeals.utils.UiUtil;

import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

public class CartBaseActivity extends BaseActivity{
	
	protected Store store;
	protected StoreDetails storeDetails;
	
	protected Deal deal;
	
	protected HelperFunctions hFunction;
	protected Bundle extras;
	protected UiUtil uiUtil;
	
	protected RadioGroup radio_group_payment;
	
	protected Spinner spinner_qty;
	protected CartItem cartItem;
	protected double totalPrice = 0;
	
	private TextView txt_view_total_price;
	protected AqHelper aqHelper;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cart);
		
		setUpHeader(getString(R.string.cart), null, false, false, true, null);
		
		init();		
		
		spinner_qty = aq.id(R.id.spinner_qty).getSpinner();
		radio_group_payment = (RadioGroup) findViewById(R.id.radio_group_payment);
		txt_view_total_price = aq.id(R.id.txt_view_total_price).getTextView();
		
		aq.id(R.id.img_view).image(deal.getLargeImageUrl(), false, true, 0, 0, null, AQuery.FADE_IN_NETWORK, 0);
		
		uiUtil.showData(deal.name, R.id.txt_view_deal_name);
		
				
		
	}
	
	protected void enableSelectedPaymentMethod(int checkedId){
		for (int i = 0; i < radio_group_payment.getChildCount(); i++) {
			View view = radio_group_payment.getChildAt(i);
			if (view.getId() == checkedId) {
				ViewHelper.setAlpha(view, 1);
			} else {
				ViewHelper.setAlpha(view, (float) 0.5);
			}
		}
	}
	
	protected void setSelectedPaymentMethod(){
		for (int i = 0; i < radio_group_payment.getChildCount(); i++) {
			if (radio_group_payment.getChildAt(i).getVisibility() == View.VISIBLE) {
				((RadioButton) radio_group_payment.getChildAt(i)).setChecked(true);
				break;
			}
		}
	}
	
	protected void updateQty(int minQty, int maxQty){		
		spinner_qty.setAdapter(new QuantityAdapter(this, R.layout.text_view, minQty, maxQty, aq));		
	}
	
	protected void updateTotalPrice(){		
		txt_view_total_price.setText(hFunction.formatPrice(totalPrice));	
	}
	
	private void init(){
		extras = getIntent().getExtras();
		uiUtil = new UiUtil(aq);
		hFunction = new HelperFunctions();
		deal = (Deal) hFunction.stringToObject(extras.getString("deal"), Deal.class);
		aqHelper = new AqHelper(this);	
		
		cartItem = new CartItem();
		cartItem.deal = deal;
		
		if(extras.containsKey("store")){
			store = (Store) hFunction.stringToObject(extras.getString("store"), Store.class);
			AQUtility.debug(extras.getString("store"));
		}
		
		if(extras.containsKey("storeDetails")){
			storeDetails = (StoreDetails) hFunction.stringToObject(extras.getString("storeDetails"), StoreDetails.class);
			AQUtility.debug(extras.getString("storeDetails"));
		}				
	}
		
}
