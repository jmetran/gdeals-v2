package com.stratpoint.globe.gdeals.base;

import java.util.HashMap;
import java.util.Map;

import android.graphics.drawable.Drawable;
import android.os.Bundle;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.BitmapAjaxCallback;
import com.androidquery.util.AQUtility;
import com.devspark.appmsg.AppMsg;
import com.flurry.android.FlurryAgent;
import com.stratpoint.globe.gdeals.R;
import com.stratpoint.globe.gdeals.helpers.ActionBarHelper;
import com.stratpoint.globe.gdeals.utils.ErrorMessageUtil;
import com.stratpoint.globe.gdeals.utils.IntentUtil;

public class BaseActivity extends SherlockFragmentActivity {
	
	protected AQuery aq;
	protected ErrorMessageUtil errMsg;	
	protected IntentUtil iUtil;
	
	private boolean developmentMode;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		aq = new AQuery(this);
		errMsg = new ErrorMessageUtil(this);
		iUtil = new IntentUtil(this);
		BitmapAjaxCallback.setCacheLimit(80);
		AjaxCallback.setNetworkLimit(8);
		
		if (!Constants.PRODUCTION){
			developmentMode = true;
			AQUtility.setDebug(true);
		}		
	}
	
	protected void onDestroy(){
		super.onDestroy();
		if (isTaskRoot()){
			AQUtility.cleanCacheAsync(this);
		}
		AppMsg.cancelAll(this);
	}
	
	protected void setUpHeader(String title, Drawable icon, boolean showIcon, boolean buttonEnable, boolean isBack, Class<?> backTo){
		new ActionBarHelper(this).setHeader(title, icon, showIcon, buttonEnable, isBack, backTo);
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();	
		if(!developmentMode){
			FlurryAgent.setReportLocation(false);
			FlurryAgent.onStartSession(this, Constants.FLURRY_ID);
			FlurryAgent.onPageView();
			AQUtility.debug("Activity", getTitle().toString());
		}
	}
	 
	@Override
	protected void onStop()
	{
		super.onStop();	
		if(!developmentMode){
			FlurryAgent.onEndSession(this);
		}
	}
	
	protected void flurryLogEvent(String value){		
		this.flurryLogEvent(new String[] { Constants.FLURRY_CLICK_EVENT }, new String[] {value});
	}
	
	protected void flurryLogEvent(String name, String value){		
		this.flurryLogEvent(new String[] {name}, new String[] {value});
	}
	
	protected void flurryLogEvent(String[] name, String[] value){		
		Map<String, String> eventParams = new HashMap<String, String>();
		int len = name.length;
		for(int i=0; i < len; i++){
			eventParams.put(name[i], value[i]);
			AQUtility.debug(name[i], value[i]);
		}
		AQUtility.debug(getTitle().toString());
		FlurryAgent.logEvent(getTitle().toString(), eventParams);
	}

	@Override
	protected void onResume ()
	{
	   super.onResume();	   	   	   
	}
	
	protected void alert(String message) {		
		AppMsg.makeText(this, message, AppMsg.STYLE_ALERT).show();
	}

	protected void confirm(String message) {
		AppMsg.makeText(this, message, AppMsg.STYLE_CONFIRM).show();
	}

	protected void info(String message) {
		AppMsg.makeText(this, message, AppMsg.STYLE_INFO).show();
	}

	protected void alert(int resId) {
		alert(getString(resId));
	}

	protected void confirm(int resId) {
		confirm(getString(resId));
	}

	protected void info(int resId) {
		info(getString(resId));
	}
	
	protected void errorMsg(String msg) {
		alert(errMsg.getErrorMessage(msg));
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		this.overridePendingTransition (R.anim.open_main, R.anim.close_next);
	}
}
