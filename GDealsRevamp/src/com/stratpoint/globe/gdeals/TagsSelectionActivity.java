package com.stratpoint.globe.gdeals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stratpoint.globe.gdeals.adapters.TagsGridAdapter;
import com.stratpoint.globe.gdeals.base.BaseActivity;
import com.stratpoint.globe.gdeals.base.Constants;
import com.stratpoint.globe.gdeals.base.GDeals;
import com.stratpoint.globe.gdeals.helpers.AqHelper;
import com.stratpoint.globe.gdeals.helpers.MenuHelper;
import com.stratpoint.globe.gdeals.helpers.RegistrationState;
import com.stratpoint.globe.gdeals.helpers.SharedPrefHelper;
import com.stratpoint.globe.gdeals.models.Tag;
import com.stratpoint.globe.gdeals.utils.IntentUtil;
import com.stratpoint.globe.gdeals.utils.JsonUtil;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.GridView;

public class TagsSelectionActivity extends BaseActivity{

	private AqHelper aqHelper;
	private ProgressDialog progress;
	
	private List<Tag> tags;
	private List<String> myTags;
	private TagsGridAdapter adapter;
	private GridView grid_view;
	private int tagsLen;
	private SharedPrefHelper sharedPrefHelper;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tags_selection);
		
		init();		
		
		if(sharedPrefHelper.getBoolean(SharedPrefHelper.LOGGED_IN)){
			setUpHeader(getString(R.string.menu_preferences), null, false, false, true, null);
			aq.id(R.id.txt_view_header).gone();
		}else{
			setUpHeader(getString(R.string.tags_selection_title), null, false, false, false, null);
		}
		
		getMyTags();		
	}
		
	private List<String> getSelectedTags(){
		List<String> selectedTags = new ArrayList<String>();
		for (int i=0; i < tagsLen; i++){
			if (adapter.getItem(i).isActive){
				selectedTags.add(adapter.getItem(i).name);
			}			
		}
		return selectedTags;
	}
	
	private void submitTags(){
		progress = new ProgressDialog(this);
		progress.setMessage("Please wait...");
		progress.setCancelable(false);
		progress.show();
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("access_token", GDeals.appAccessToken());
	    params.put("tags", TextUtils.join(", ", getSelectedTags()).toLowerCase());
	    
	    AQUtility.debug(TextUtils.join(", ", getSelectedTags()));
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.TAGS_URL , "submitTagsCb");  
		
		cb.params(params);
		cb.method(AQuery.METHOD_PUT);
		aq.progress(progress).ajax(cb);
		
		
	}
	
	public void submitTagsCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		
		if (_status.getCode() == 200){			
			checkWhatsNext();
		}else{
			errorMsg(_status.getMessage());
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}		
	}
	
	private void getMyTags(){
		progress = new ProgressDialog(this);
		progress.setMessage("Please wait...");
		progress.setCancelable(false);
		progress.show();
			    
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.TAGS_URL+ "?access_token=" +GDeals.appAccessToken() , "getMyTagsCb");  
		aq.progress(progress).ajax(cb);
	}
	
	public void getMyTagsCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		
		if (_status.getCode() == 200){	
			
			myTags = new Gson().fromJson(JsonUtil.getJSONArray("result", _jo).toString(), new TypeToken<List<String>>(){}.getType());
			
			String[] tmpTags = getResources().getStringArray(R.array.deals_category_menu_items);
			
			for(String tag: tmpTags){
				
				if(myTags.contains(tag.toLowerCase())){
					tags.add(new Tag(tag, true));
				}else{
					tags.add(new Tag(tag, false));
				}				
			}		
			
			tagsLen = tags.size();
			
			adapter = new TagsGridAdapter(this, R.layout.tag_row, tags);
			grid_view.setAdapter(adapter);
			
		}else{
			errorMsg(_status.getMessage());
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}		
	}
	
	private void checkWhatsNext(){
		if(sharedPrefHelper.getBoolean(SharedPrefHelper.NEW_ACCOUNT)){
			sharedPrefHelper.updateSharedPreferences(SharedPrefHelper.REGISTRATION_STATE, RegistrationState.DONE);
			sharedPrefHelper.updateSharedPreferences(SharedPrefHelper.LOGGED_IN, true);
			sharedPrefHelper.updateSharedPreferences(SharedPrefHelper.NEW_ACCOUNT, false);
			iUtil.startIntent(GenericThankYouPageActivity.class, true);
		}else{
			info(String.format(getString(R.string.successfully_saved), getString(R.string.tags_selection_title)));
		}
	}
	
	private void init(){
		aqHelper = new AqHelper(this);	
		iUtil = new IntentUtil(this);
		sharedPrefHelper = new SharedPrefHelper(this);
		tags = new ArrayList<Tag>();
		grid_view = aq.id(R.id.grid_view).getGridView();
	}
	
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.done_action, menu);
	    
	    MenuItem item = menu.findItem(R.id.btn_done);
	    MenuHelper.setMenuItemLabel(item.getActionView(), getString(R.string.done), R.drawable.img_ic_check, new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				submitTags();
				flurryLogEvent(getString(R.string.done));
			}
		});
	    return super.onCreateOptionsMenu(menu);
    }
	
	

}
