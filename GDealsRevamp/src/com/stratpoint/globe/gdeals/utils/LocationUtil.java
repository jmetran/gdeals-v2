package com.stratpoint.globe.gdeals.utils;

import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.LocationAjaxCallback;

import android.content.Context;
import android.location.Location;
import android.util.Log;

public class LocationUtil {
	
	private Context context;
	private double lon;
	private double lat;
	
	public LocationUtil(Context c){
		this.context = c;
		getLocation();
	}
	
	public void getLocation(){
		LocationAjaxCallback cb = new LocationAjaxCallback();
		cb.weakHandler(this, "locationCb").timeout(40*1000);
		cb.async(this.context);
	}
	
	public void locationCb(String url, Location loc, AjaxStatus status){
		if(loc != null){
			this.setLon(loc.getLongitude());
			this.setLat(loc.getLatitude());
			Log.v("LOC", loc.toString());
		}
	}
	
	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

}
