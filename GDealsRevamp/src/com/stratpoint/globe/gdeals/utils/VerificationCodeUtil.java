package com.stratpoint.globe.gdeals.utils;

import android.content.Context;

import com.stratpoint.globe.gdeals.helpers.SharedPrefHelper;

public class VerificationCodeUtil {

	private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	
	public static String generateVerificationCode(int len, Context context) {
		StringBuilder builder = new StringBuilder();
		while (len-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		
		new SharedPrefHelper(context).updateSharedPreferences(SharedPrefHelper.VERIFICATION_CODE, builder.toString());
		
		return builder.toString();
	}
}
