package com.stratpoint.globe.gdeals.utils;

import java.util.ArrayList;

import com.androidquery.AQuery;
import com.stratpoint.globe.gdeals.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

public class UiUtil {

	private AQuery aq;
	private HelperFunctions helperFunction;
	private Activity activity;

	public UiUtil(AQuery aq) {
		this.aq = aq;
		helperFunction = new HelperFunctions();
	}

	public UiUtil(Activity activity, AQuery aq) {
		this.activity = activity;
		this.aq = aq;
		helperFunction = new HelperFunctions();		
	}

	public void showData(String _str, int _id, int bgColor) {
		if (!isEmpty(_str)) {
			aq.id(_id).text(_str).background(bgColor);
			aq.id(_id).visible();
		} else {
			aq.id(_id).gone();
		}
	}

	public void showData(String _str, int _id, Drawable _drawable) {
		if (!isEmpty(_str)) {
			if (_drawable == null) {
				aq.id(_id).gone();
			} else {
				aq.id(_id)
						.text(_str)
						.visible()
						.getTextView()
						.setCompoundDrawablesWithIntrinsicBounds(_drawable,
								null, null, null);
			}
		} else {
			aq.id(_id).gone();
		}
	}

	public void showLabel(String _str, int _id) {
		showLabel(new String[] { _str }, _id);
	}

	public void showLabel(String[] _str, int _id) {
		boolean show = false;
		if (_str.length != 0) {
			for (String str : _str) {
				if (str != null && str.length() > 0) {
					show = true;
				}
			}
			if (show) {
				aq.id(_id).visible();
			} else {
				aq.id(_id).gone();
			}
		}
	}

	public void showData(String _str, int _id) {
		if (!isEmpty(_str)) {
			aq.id(_id).text(_str);
			aq.id(_id).visible();
		} else {
			aq.id(_id).gone();
		}
	}

	public void showData(String _strToCheck, String _strToShow, View _id) {
		if (!isEmpty(_strToCheck)) {
			aq.id(_id).text(_strToShow);
			aq.id(_id).visible();
		} else {
			aq.id(_id).gone();
		}
	}

	public void showEmptyData(String _str, int _id) {
		if (!isEmpty(_str)) {
			aq.id(_id).text(_str);
			aq.id(_id).visible();
		} else {
			aq.id(_id).text("");
		}
	}

	public boolean isEmpty(String _str) {
		if (_str != null && !_str.equalsIgnoreCase("null") && _str.length() > 0) {
			return false;
		}
		return true;
	}

	public void showData(ArrayList<String> _str, int _id) {
		if (_str != null && _str.size() > 0) {
			aq.id(_id).text(helperFunction.join(_str, ", "));
			aq.id(_id).visible();
		} else {
			aq.id(_id).gone();
		}
	}

	// NEW
	public void hideIfNotEqual(Object objCheck, Object toCompare,
			Object objShow, Object view) {
		this.showData(objCheck, objShow, view, null, toCompare);
	}

	public void showData(Object objShow, Object view, Drawable drawable) {
		this.showData(objShow, objShow, view, drawable, null);
	}

	public void showData(Object objShow, Object view) {
		this.showData(objShow, objShow, view, null, null);
	}

	public void showData(Object objCheck, Object objShow, Object view) {
		this.showData(objCheck, objShow, view, null, null);
	}

	public void showData(Object objCheck, Object objShow, Object view,
			Drawable drawable, Object toCompare) {
		boolean show = false;
		if (objCheck instanceof Integer) {
			if ((Integer) objCheck > 0) {
				show = true;
			} else {
				show = false;
			}
		} else if (objCheck instanceof Double) {
			if ((Double) objCheck > 0) {
				show = true;
			} else {
				show = false;
			}
		} else if (objCheck instanceof String) {
			if (TextUtils.isEmpty((String) objCheck)) {
				show = false;
			} else {
				show = true;
			}

			if (toCompare != null) {
				if (!((String) objCheck).equalsIgnoreCase((String) toCompare)) {
					show = false;
				}
			}

		}

		if (view instanceof Integer) {
			if (show) {

				aq.id((Integer) view).visible();

				if (objShow instanceof Spanned) {
					aq.id((Integer) view)
							.text((Spanned) objShow)
							.getTextView()
							.setMovementMethod(LinkMovementMethod.getInstance());
				} else {
					aq.id((Integer) view).text((String) objShow);
				}

				if (drawable != null) {
					aq.id((Integer) view)
							.getTextView()
							.setCompoundDrawablesWithIntrinsicBounds(drawable,
									null, null, null);
				}

			} else {
				aq.id((Integer) view).gone();
			}
		} else if (view instanceof View) {
			if (show) {

				aq.id((View) view).visible();

				if (objShow instanceof Spanned) {
					aq.id((View) view)
							.text((Spanned) objShow)
							.getTextView()
							.setMovementMethod(LinkMovementMethod.getInstance());

				} else {
					aq.id((View) view).text((String) objShow);
				}

				if (drawable != null) {
					aq.id((View) view)
							.getTextView()
							.setCompoundDrawablesWithIntrinsicBounds(drawable,
									null, null, null);
				}

			} else {
				aq.id((View) view).gone();
			}
		}
	}

	// Refresh with custom message
	/*public void showTapToRefresh(Activity a, LinearLayout l, String msg) {
		this.showTapToRefresh(a, l, msg, 0, null, 0);
	}

	public void showTapToRefresh(Activity a, LinearLayout l, String msg,
			int color) {
		this.showTapToRefresh(a, l, msg, 0, null, color);
	}

	public void showTapToRefresh(Activity a, LinearLayout l, String msg, int layout, Drawable img, int color) {
		View v = null;
		if (layout == 0) {
			v = ((LayoutInflater) a.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.tap_to_refresh, null, false);
		} else {
			v = ((LayoutInflater) a.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(layout, null, false);
		}

		TextView message = (TextView) v.findViewById(R.id.message);
		if (msg != null) {
			if (img != null) {
				message.setCompoundDrawablesWithIntrinsicBounds(null, img, null, null);
			}
			message.setText(msg);
		}

		if (color != 0) {
			message.setTextColor(color);
		}

		v.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		l.addView(v);
	}

	public View showTapToRefresh() {
		View v = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.tap_to_refresh, null, false);
		return v;
	}
	
	public View showLoader() {
		View v = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.loader, null, false);
		return v;
	}*/
	
	public void showView(int inflateTo, View view) {
		LinearLayout parent_view = (LinearLayout) activity.findViewById(inflateTo);
		parent_view.addView(view);
	}
	
	public View showLoader() {
		View v = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.loader, null, false);
		return v;
	}
	
	public View tapToRefresh() {
		View v = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.tap_to_refresh, null, false);
		return v;
	}
	
	public View loader() {
		View v = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.loader, null, false);
		return v;
	}
	
	public View empty() {
		View v = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.empty, null, false);
		return v;
	}
	
}
