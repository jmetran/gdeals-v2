package com.stratpoint.globe.gdeals.utils;

import java.io.Serializable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonUtil  implements Serializable{
	
	public static String getString(String key, JSONObject object) {
		try {
			return "null".equals(object.getString(key)) ? null : object.getString(key);
		} catch (JSONException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	public static JSONObject getJSONObject(String key, JSONObject object) {
		try {
			return object.getJSONObject(key);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static JSONArray getJSONArray(String key, JSONObject object) {
		try {
			return object.getJSONArray(key);
		} catch (JSONException e) {
			return null;
		}
	}
	
}
