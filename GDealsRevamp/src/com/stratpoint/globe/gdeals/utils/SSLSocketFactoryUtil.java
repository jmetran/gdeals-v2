package com.stratpoint.globe.gdeals.utils;

import java.io.InputStream;
import java.security.KeyStore;

import org.apache.http.conn.ssl.SSLSocketFactory;

import android.content.Context;

import com.stratpoint.globe.gdeals.R;

public class SSLSocketFactoryUtil {

	public static SSLSocketFactory getSSLSocketFactory(Context context) {
		try {
			KeyStore trusted = KeyStore.getInstance("BKS");
			InputStream in = context.getResources().openRawResource(R.raw.gdealsstore);
			try {
				trusted.load(in, "ez24get".toCharArray());
			} finally {
				in.close();
			}
			return new SSLSocketFactory(trusted);
		} catch (Exception e) {
			throw new AssertionError(e);
		}
	}
}
