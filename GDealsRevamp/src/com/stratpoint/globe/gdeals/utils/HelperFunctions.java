package com.stratpoint.globe.gdeals.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.StyleSpan;

import com.google.gson.Gson;

public class HelperFunctions {

	private Gson gson = new Gson();
	private DecimalFormatSymbols dfs;
	
	public HelperFunctions(){	
		dfs = new DecimalFormatSymbols();
		dfs.setDecimalSeparator('.');
		dfs.setGroupingSeparator(',');
	}
	
	public String objectToString(Object _obj){
		String retVal = "";
		try{
			retVal = gson.toJson(_obj).toString();			
		}catch (Exception e) {
			// TODO: handle exception
		}
		return retVal;
	}
	
	public Object stringToObject(String _str, Class<?> _obj){
		Object retVal = "";
		try{
			retVal = gson.fromJson(_str, _obj);		
		}catch (Exception e) {
			// TODO: handle exception
		}
		return retVal;
	}
	
	
	public List<String> jsonArrayToArrayList(String _str){
		List<String> retVal = new ArrayList<String>();		
		try {
			JSONArray jsonArray = new JSONArray(_str);
			int len = jsonArray.length();
			   for (int i=0;i<len;i++){ 
				   retVal.add(jsonArray.get(i).toString());
			   } 				
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return retVal;
	}
	
	
	
	public String isGreaterThanAndEqualTo(String _value){
		return urlEncode(">="+_value);
	}
	
	public String urlEncode(String _value){
		String retVal = "";
		try {
			retVal = URLEncoder.encode(_value, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retVal;
	}
	
	
	
	public String join(List<String> list , String replacement, boolean addSqlWrapper) {
        StringBuilder b = new StringBuilder();
        for( String item: list ) { 
        	if (addSqlWrapper){
                b.append( replacement ).append( "'"+item+"'");        		
        	}else{
            	b.append( replacement ).append( item );        		
        	}
        }
        return b.toString().substring(replacement.length());
    }
	
	public String join(List<String> list , String replacement  ) {
        return this.join(list , replacement, false);
    }
	
	public String formatPrice(String price){
		return "Php "+ this.formatPriceNoCurrency(Double.parseDouble(price));
	}
	
	public String formatPrice(Double price){
		return "Php "+ this.formatPriceNoCurrency(price);
	}
	
	public String formatPriceNoCurrency(String price){
		return this.formatPriceNoCurrency(Double.parseDouble(price));
	}
	
	public String formatPriceNoCurrency(Double price){
		DecimalFormat moneyFormat = new DecimalFormat("###,##0.##", dfs);
		return moneyFormat.format(price);
	}
	
	public SpannableString spannableString(String str){
		SpannableString spanString = new SpannableString(str);
		spanString.setSpan(new StyleSpan(Typeface.ITALIC), 0, spanString.length(), 0);
		return spanString;
	}
	
	
}
