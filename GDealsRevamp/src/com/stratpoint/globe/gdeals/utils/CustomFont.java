package com.stratpoint.globe.gdeals.utils;

import android.content.Context;
import android.graphics.Typeface;

public class CustomFont {

	public final static int FONT_BOLD = 0;
	public final static int FONT_REGULAR = 1;
	public final static int FONT_LIGHT = 2;
	
	
	public static final Typeface typeFace(Context context){
    	return typeFace(context, FONT_REGULAR);
    }
	
    public static final Typeface typeFace(Context context, int _type){
    	String fontType = "";
    	switch (_type) {
		case FONT_BOLD:
			fontType = "fonts/FS Elliot Pro-Bold.ttf";
			break;
		case FONT_REGULAR:
			fontType = "fonts/FS Elliot Pro-Regular.ttf";
			break;
		case FONT_LIGHT:
			fontType = "fonts/FS Elliot Pro-Light.ttf";
			break;
		}
    	
    	return Typeface.createFromAsset(context.getAssets(), fontType);
    }

}