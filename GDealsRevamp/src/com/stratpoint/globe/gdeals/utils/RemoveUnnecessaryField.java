package com.stratpoint.globe.gdeals.utils;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.orm.SugarRecord;

public class RemoveUnnecessaryField implements ExclusionStrategy{

	@Override
	public boolean shouldSkipClass(Class<?> arg0) {
		return false;
	}

	@Override
	public boolean shouldSkipField(FieldAttributes f) {
		return (f.getDeclaringClass() == SugarRecord.class && f.getName().equals("tableName")) ||
	            (f.getDeclaringClass() == SugarRecord.class && f.getName().equals("id"));
	}
	
}
