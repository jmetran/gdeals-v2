package com.stratpoint.globe.gdeals.utils;

import android.content.Context;
import android.telephony.TelephonyManager;

public class PhoneUtil {
	
	private Context context;
	
	public PhoneUtil(Context context){
		this.context = context;
 	}
	
	public String getMobileNumber(){
		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		return tm.getLine1Number();
	}

}
