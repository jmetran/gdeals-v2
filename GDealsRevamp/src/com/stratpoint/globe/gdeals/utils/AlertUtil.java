package com.stratpoint.globe.gdeals.utils;

import com.stratpoint.globe.gdeals.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.widget.Toast;

public class AlertUtil {

	private Activity activity;
	
	
	public AlertUtil(Activity _activity){
		this.activity = _activity;
	}
	
	public void showToast(String msg){
		Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
	}
	
	public void alertInfo(String msg) {
		this.alertInfo(null, msg, null);
	}
	
	public void alertInfo(String msg, DialogInterface.OnClickListener positiveListener) {
		this.alertInfo(null, msg, positiveListener);
	}
	
	public void alertInfo(String title, String msg, DialogInterface.OnClickListener positiveListener) {
		AlertDialog.Builder alertbox = new AlertDialog.Builder(activity);
		if (title != null){
			alertbox.setTitle(title);
		}		
		alertbox.setMessage(msg);
		
		if(positiveListener == null){
			alertbox.setNeutralButton(activity.getString(R.string.ok), null);
		}else{
			alertbox.setNeutralButton(activity.getString(R.string.ok), positiveListener);
		}
		
		alertbox.show();
	}
	
//	public void alertConfirmation(String title, String msg, String btnLabelLeft, String btnLabelRight, boolean isGoBack, Class<?> _class){
//		this.alertConfirmation(title, msg, btnLabelLeft, btnLabelRight, isGoBack, _class, null, null, false);
//	}
//	
//	public void alertConfirmation(String title, String msg, String btnLabelLeft, String btnLabelRight, boolean isGoBack, boolean isExit){
//		this.alertConfirmation(title, msg, btnLabelLeft, btnLabelRight, isGoBack, null, null, null, isExit);
//	}
//	
	public void alertConfirmation(String title, String msg, boolean isGoBack, Class<?> _class){
		this.alertConfirmation(title, msg, null, null, null, null, isGoBack, _class, null, null, false);
		
	}
	
	public void alertConfirmation(CharSequence title, CharSequence msg, DialogInterface.OnClickListener positiveListener, DialogInterface.OnClickListener negativeListener){
		this.alertConfirmation(title, msg, null, null, positiveListener, negativeListener, false, null, null, null, false);		
	}
//	
//	public void alertConfirmation(String title, String msg, String btnLabelLeft, String btnLabelRight, boolean isGoBack, Class<?> _class, String[] str, Object[] obj){
//		this.alertConfirmation(title, msg, btnLabelLeft, btnLabelRight, isGoBack, _class, str, obj, false);
//	}
	
	public void alertConfirmation(CharSequence title, CharSequence msg, String btnLabelPositive, String btnLabelNegative, DialogInterface.OnClickListener positiveListener, DialogInterface.OnClickListener negativeListener, final boolean isGoBack, final Class<?> _class, final String[] str, final Object[] obj, final boolean isExit) {
		AlertDialog.Builder alertbox = new AlertDialog.Builder(activity);
		
		String _btnLabelPositive = btnLabelPositive;
		String _btnLabelNegative = btnLabelNegative;
		
		if (btnLabelPositive == null){
			_btnLabelPositive = activity.getString(R.string.ok);
		}
		
		if (btnLabelNegative == null){
			_btnLabelNegative = activity.getString(R.string.cancel);
		}
		
		if (title != null){
			alertbox.setTitle(title);
		}
		
		alertbox.setMessage(msg);
		
		if (positiveListener == null){
			
			alertbox.setPositiveButton(_btnLabelPositive, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					if (isExit){
						activity.finish();
					}else{
						if (str != null && obj != null){
							new IntentUtil(activity).startIntent(_class, str, obj);
						}else{
							new IntentUtil(activity).startIntent(_class);
						}
					}
				}
			});
		}else{
			alertbox.setPositiveButton(_btnLabelPositive, positiveListener);
		}
		
		if (negativeListener == null){
			alertbox.setNegativeButton(_btnLabelNegative, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					if (isGoBack){
						activity.finish();
					}
				}
			});
		}else{
			alertbox.setNegativeButton(_btnLabelNegative, negativeListener);
		}
		
		alertbox.show();
	}
	
	
	public void launchGPSOptions(Activity a) {
		final Intent intent = new Intent(
				Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		a.startActivityForResult(intent, 0);
	}
	
	public void networkProblem(final Activity activity){
		AlertDialog.Builder alertbox = new AlertDialog.Builder(activity);
		alertbox.setTitle("Connection failed");
		alertbox.setMessage("This application requires network access. Enable mobile network or Wi-fi.");
		alertbox.setPositiveButton("Ok",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
//						if(activity.getClass() == MainActivity.class){
//							activity.finish();
//						}						
					}
				});				
		alertbox.show();
	}
	
	public void alertNetworkSetting(final Activity activity) {
		alertNetworkSetting(activity, false);
	}
	//
	public void alertNetworkSetting(final Activity activity, boolean cancel) {
		AlertDialog.Builder alertbox = new AlertDialog.Builder(activity);
		alertbox.setTitle("Connection failed");
		alertbox.setMessage("This application requires network access. Enable mobile network or Wi-fi.");
		alertbox.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						Intent mIntent = new Intent("/");
						ComponentName comp = new ComponentName(
								"com.android.settings",
								"com.android.settings.WirelessSettings");
						mIntent.setComponent(comp);
						mIntent.setAction("android.intent.action.VIEW");
						activity.startActivityForResult(mIntent, 0);
					}
				});
		if (cancel){
			alertbox.setNeutralButton("Cancel", null);
		}
		alertbox.setNegativeButton("Close", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {				
//				if(activity.getClass() != MainActivity.class){
//					activity.finish();
//				}				
			}
		});				
		alertbox.show();
	}
	
	private boolean isGPSSettingShow = false;
	
	public boolean isGPSSettingShow() {
		return isGPSSettingShow;
	}

	public void setGPSSettingShow(boolean isGPSSettingShow) {
		this.isGPSSettingShow = isGPSSettingShow;
	}

	public void showGPSSettings(final Activity a) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(a);
		builder.setMessage("Your GPS Satelites and Wireless Networks seems to be disabled.\n\nDo you want to enable it?")
				.setCancelable(false)
				.setNegativeButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(
									final DialogInterface dialog, final int id) {
								launchGPSOptions(a);
								setGPSSettingShow(true); 
							}
						})
				.setPositiveButton("No", new DialogInterface.OnClickListener() {
					public void onClick(final DialogInterface dialog, final int id) {
						setGPSSettingShow(false);
					}
				});
		
		final AlertDialog alert = builder.create();
		alert.show();
	}
	
}
