package com.stratpoint.globe.gdeals.utils;

import java.util.ArrayList;
import java.util.List;

import android.text.Html;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.util.Pair;

public class HtmlUtil {
	public static Spanned addLinksHtmlAware(String htmlString) {
		// gather links from html
		Spanned spann = Html.fromHtml(htmlString);
		URLSpan[] old = spann.getSpans(0, spann.length(), URLSpan.class);
		List<Pair<Integer, Integer>> htmlLinks = new ArrayList<Pair<Integer, Integer>>();
		for (URLSpan span : old) {
			htmlLinks.add(new Pair<Integer, Integer>(spann.getSpanStart(span), spann.getSpanEnd(span)));
		}
		// linkify spanned, html link will be lost
		Linkify.addLinks((Spannable) spann, Linkify.ALL);
		// add html links back
		for (int i = 0; i < old.length; i++) {
			((Spannable) spann).setSpan(old[i], htmlLinks.get(i).first, htmlLinks.get(i).second, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		}

		return spann;
	}
}
