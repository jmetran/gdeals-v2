package com.stratpoint.globe.gdeals.utils;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class SecurityUtil {

	/**
	 * @param key
	 *            - this will serve as secret key for generating hmac_sha1
	 * @param data
	 *            - to be used for generating hmac_sha1
	 * 
	 * @return hmac_sha1 value of the given key and data
	 * 
	 * @throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException 
	 * */
	public static String generateHmacSHA1(String key, String data) throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException {

		Mac mac = Mac.getInstance("HmacSHA1");

		// get the bytes of the hmac key and data string
		byte[] secretByte = key.getBytes("UTF-8");
		byte[] dataBytes = data.getBytes("UTF-8");

		SecretKey secret = new SecretKeySpec(secretByte, "HmacSHA1");

		mac.init(secret);
		byte[] doFinal = mac.doFinal(dataBytes);
		return byte2hex(doFinal);

	}

	private static String byte2hex(final byte[] b) {
		String hs = "";
		String stmp = "";
		for (int n = 0; n < b.length; n++) {
			stmp = (java.lang.Integer.toHexString(b[n] & 0xFF));
			if (stmp.length() == 1)
				hs = hs + "0" + stmp;
			else
				hs = hs + stmp;
		}
		return hs;
	}
}
