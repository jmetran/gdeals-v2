package com.stratpoint.globe.gdeals;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.stratpoint.globe.gdeals.base.CartBaseActivity;
import com.stratpoint.globe.gdeals.base.Constants;
import com.stratpoint.globe.gdeals.base.GDeals;
import com.stratpoint.globe.gdeals.helpers.DateHelper;
import com.stratpoint.globe.gdeals.helpers.SignatureHelper;
import com.stratpoint.globe.gdeals.models.Payment;
import com.stratpoint.globe.gdeals.models.ResultMessage;
import com.stratpoint.globe.gdeals.utils.JsonUtil;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class CartAdealsActivity extends CartBaseActivity{
	
	private ProgressDialog progress;
	private ResultMessage result;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		init();
		
		aq.id(R.id.layout_holder).gone();
		aq.id(R.id.view_linear_variant).gone();
		
		aq.id(R.id.txt_view_deal_exclusive).text(deal.companyName + " - "+ store.name);

		spinner_qty.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {				
				cartItem.qty = position+1;
				totalPrice = (double) Double.parseDouble(deal.discountedPrice) * cartItem.qty;	
				updateTotalPrice();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});
		
		uiUtil.hideIfNotEqual(storeDetails.allowCreditCard, "yes", getText(R.string.payment_option_via_cc), R.id.rdo_payment_option_via_cc);
		uiUtil.hideIfNotEqual(storeDetails.allowCod, "yes", getText(R.string.payment_option_via_cod), R.id.rdo_payment_option_via_cod);
		uiUtil.hideIfNotEqual(storeDetails.allowLoad, "yes", getText(R.string.payment_option_via_load), R.id.rdo_payment_option_via_load);
		
		radio_group_payment.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rdo_payment_option_via_cc:
					updateQty(storeDetails.minCCQuantity, storeDetails.maxCCQuantity);
					showHideDeliveryNotes(false);
					break;
				case R.id.rdo_payment_option_via_cod:
					updateQty(storeDetails.minCodQuantity, storeDetails.maxCodQuantity);
					showHideDeliveryNotes(true);
					break;
				case R.id.rdo_payment_option_via_load:
					updateQty(storeDetails.minLoadQuantity, storeDetails.maxLoadQuantity);
					showHideDeliveryNotes(false);
					break;
				}
				
				enableSelectedPaymentMethod(checkedId);
				
			}
		});
		
		setSelectedPaymentMethod();
		
	}
	
	private void init(){
		result = new ResultMessage();
	}
	
	private void showHideDeliveryNotes(boolean show){
		if(show){
			aq.id(R.id.txt_view_delivery_label).visible();
			aq.id(R.id.txt_view_delivery_caption).visible();
			aq.id(R.id.devider_bottom).visible();
			aq.id(R.id.txt_view_checkout_terms).visible();
		}else{
			aq.id(R.id.txt_view_delivery_label).gone();
			aq.id(R.id.txt_view_delivery_caption).gone();
			aq.id(R.id.devider_bottom).gone();
			aq.id(R.id.txt_view_checkout_terms).gone();
		}
	}	
	
	private void paymentViaCreditCard(){
		progress = new ProgressDialog(this);
		progress.setMessage("Please wait...");
		progress.setCancelable(false);
		progress.show();
		
		String accessToken = GDeals.appAccessToken();
		String dealId = cartItem.deal.id;
		String merchantId = cartItem.deal.merchantId;
		String paymentType = Payment.TYPE_CREDIT_CARD;
		String qty = String.valueOf(cartItem.qty);
		String storeId = store.id;
		String timeStamp = new DateHelper().getCurrentDate("yyyyMMddhhmmssss");
		
		Map<String, Object> params = new HashMap<String, Object>();
	    params.put("access_token", accessToken);
		params.put("deal_id", dealId);
	    params.put("merchant_id", merchantId);
	    params.put("payment_type", paymentType);
	    params.put("quantity", qty);
	    params.put("store_id", storeId);
	    params.put("ts", timeStamp);
	    
	    params.put("signature", new SignatureHelper().generateSignature(new String[] { accessToken, dealId, merchantId, paymentType, qty, storeId, timeStamp }));
	    
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.PAYMENT_ADEALS_URL, "paymentViaCreditCardCb");  
		cb.params(params);
		cb.method(AQuery.METHOD_POST);
		aq.progress(progress).ajax(cb);
	}
	
	public void paymentViaCreditCardCb(String url, JSONObject jo, AjaxStatus status) throws JSONException{
		AQUtility.debug(jo);
		AQUtility.debug(status.getCode());
		Gson gson = new GsonBuilder().create();
		if (status.getCode() == 200){
			String html = JsonUtil.getString("web_form", JsonUtil.getJSONObject("result", jo));
			iUtil.startIntent(PaymentGatewayAdealsActivity.class, new String[] {"payment_html", "deal"}, new Object[] {html, deal});			
		}else if(status.getCode() == 400){	
			result = gson.fromJson(status.getError(), ResultMessage.class);
			alert(result.message);
		}else{
			errorMsg(status.getMessage());
			AQUtility.debug("Error", status.getCode() + " " + "Status:" + status.getMessage());
		}	
	}
	
	private void paymentViaCOD(){
		cartItem.dealProvider = Constants.ADEALS;
		cartItem.paymentType = Payment.TYPE_COD;
		cartItem.store = store;
		iUtil.startIntent(DeliveryAddressActivity.class, new String[] {"cartItem", "deal"}, new Object[] {cartItem, deal});
	}
	
	private void confirmReserveViaLoad(){
		View view = getLayoutInflater().inflate(R.layout.text_view, null);
		TextView txt_view = (TextView) view.findViewById(R.id.txt_view_item);
		
		String str = "<b><font size=\"30\" color=\"#005ab0\">"+hFunction.formatPrice(store.loadPrice)+"</font></b> will be charged to your prepaid load or postpaid account. Please pay the amount of <b><font size=\"30\" color=\"#005ab0\">"+ hFunction.formatPrice(Double.parseDouble(store.price) - Double.parseDouble(store.loadPrice)) +"</font></b> when you visit the store/branch. Click OK to proceed.";
		txt_view.setText(Html.fromHtml(str));
		
		AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
		alertbox.setTitle(getString(R.string.reserve_via_load));
		alertbox.setView(txt_view);
		alertbox.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				reserveViaLoad();
			}
		});
		
		alertbox.setNegativeButton(getString(R.string.cancel), null);
		alertbox.show();
	}
	
	
	private void reserveViaLoad(){
		progress = new ProgressDialog(this);
		progress.setMessage("Please wait...");
		progress.setCancelable(false);
		progress.show();
		
		String accessToken = GDeals.appAccessToken();
		String dealId = cartItem.deal.id;
		String merchantId = cartItem.deal.merchantId;
		String paymentType = Payment.TYPE_RESERVE_VIA_LOAD;
		String qty = String.valueOf(cartItem.qty);
		String storeId = store.id;
		String timeStamp = new DateHelper().getCurrentDate("yyyyMMddhhmmssss");
		
		Map<String, Object> params = new HashMap<String, Object>();
	    params.put("access_token", accessToken);
		params.put("deal_id", dealId);
	    params.put("merchant_id", merchantId);
	    params.put("payment_type", paymentType);
	    params.put("quantity", qty);
	    params.put("store_id", storeId);
	    params.put("ts", timeStamp);
	    
	    params.put("signature", new SignatureHelper().generateSignature(new String[] { accessToken, dealId, merchantId, paymentType, qty, storeId, timeStamp }));
	    
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.PAYMENT_ADEALS_URL, "reserveViaLoadCb");  
		cb.params(params);
		cb.method(AQuery.METHOD_POST);
		aq.progress(progress).ajax(cb);
	}
	
	public void reserveViaLoadCb(String url, JSONObject jo, AjaxStatus status) throws JSONException{
		AQUtility.debug(jo);
		AQUtility.debug(status.getCode());
		Gson gson = new GsonBuilder().create();
		if (status.getCode() == 200){
			iUtil.startIntent(GenericThankYouPageActivity.class, new String[] { "paymentType", "deal" }, new Object[] { Payment.TYPE_RESERVE_VIA_LOAD, deal });
		}else if(status.getCode() == 400){	
			result = gson.fromJson(status.getError(), ResultMessage.class);
			alert(result.message);
		}else{
			errorMsg(status.getMessage());
			AQUtility.debug("Error", status.getCode() + " " + "Status:" + status.getMessage());
		}	
	}
	
	public void onClick(View v){
		switch (v.getId()) {
		case R.id.btn_cancel:
			flurryLogEvent("Cancelled");
			onBackPressed();
			break;
		case R.id.btn_continue:
			switch (radio_group_payment.getCheckedRadioButtonId()) {
			case R.id.rdo_payment_option_via_cc:
				paymentViaCreditCard();
				flurryLogEvent("Payment - Credit Card");
				break;
			case R.id.rdo_payment_option_via_cod:
				paymentViaCOD();
				flurryLogEvent("Payment - COD");
				break;
			case R.id.rdo_payment_option_via_load:
				confirmReserveViaLoad();
				flurryLogEvent("Payment - Via Load");
				break;
			default:
				alert(getString(R.string.select_payment_option));
				break;
			}
		}
	}
	
}
