package com.stratpoint.globe.gdeals;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;

import com.androidquery.util.AQUtility;
import com.stratpoint.globe.gdeals.base.BaseActivity;
import com.stratpoint.globe.gdeals.models.Deal;
import com.stratpoint.globe.gdeals.models.Payment;
import com.stratpoint.globe.gdeals.utils.HelperFunctions;

import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Environment;
public class PaymentGatewayOkayOkayActivity extends BaseActivity {

	private Bundle extras;

	private File mParentDirectory;

	private static final String SUCCESS_URL = "https://theshop.ph/deals/success/";
	private static final String CANCEL_URL = "theshop.ph/deals/checkout/message?type=customer+cancel+transaction";
	
	private static final String OTHER_SUCCESS_URL = "https://deals.theshop.ph/deals/success/";
	
	private ProgressDialog progress;
	private WebView webview;
	
	private HelperFunctions hFunction;

	private static final String htmlFileName = "_.html";
	private Deal deal;
	
	private boolean isSuccess = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		init();

		setContentView(webview);

		setUpHeader(getString(R.string.back), null, false, false, true, null);

		try {

			// Check parent directory
			if (!mParentDirectory.exists()) {
				System.err.println("It seems like parent directory does not exist...");
				if (!mParentDirectory.mkdirs()) {
					System.err.println("And we cannot create it...");
				}
			}

			// Write it into the file
			File tmp = new File(mParentDirectory.getPath() + "/" + htmlFileName);
			Writer out = null;
			out = new BufferedWriter(new FileWriter(tmp));
			out.write(extras.getString("payment_html"));
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		webview.getSettings().setJavaScriptEnabled(true);
		webview.setInitialScale(1);
		webview.getSettings().setLoadWithOverviewMode(true);
		webview.getSettings().setUseWideViewPort(true);
		webview.getSettings().setSupportZoom(true);
		
		
		CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(this);
		CookieManager cookieManager = CookieManager.getInstance();
		cookieManager.removeAllCookie();

		webview.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progressLoad) {
				try {
					progress.setMessage("Please wait...");
					progress.setCancelable(false);
					progress.show();
					if (progressLoad == 100) {
						progress.dismiss();
					}
				} catch (Exception e) {
				}
			}
			
			@Override
			public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
				new AlertDialog.Builder(PaymentGatewayOkayOkayActivity.this).setTitle(getString(R.string.app_name)).setMessage(message).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						result.confirm();
					}
				}).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						result.cancel();
					}
				}).create().show();

				return true;
			}
		});

		webview.setWebViewClient(new WebViewClient() {

			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				view.clearCache(true);
			}

			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				AQUtility.debug(description);
			}

			@Override
			public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
				handler.proceed();
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				
				AQUtility.debug(url);
				
				if (url.contains(SUCCESS_URL) || url.contains(OTHER_SUCCESS_URL)) {
					
					if(!isSuccess){
						isSuccess = true;
						iUtil.startIntent(GenericThankYouPageActivity.class, new String[] {"paymentType", "deal"}, new Object[] {Payment.TYPE_CREDIT_CARD, deal});	
					}
					
				} else if (url.contains(CANCEL_URL)) {
					finish();
				}

				view.loadUrl(url);
				return true;
			}
		});

		webview.loadUrl("file://" + Environment.getExternalStorageDirectory() + "/" + htmlFileName);

	}

	private void init() {
		extras = getIntent().getExtras();
		mParentDirectory = new File(Environment.getExternalStorageDirectory().toString());
		webview = new WebView(this);
		progress = new ProgressDialog(this);
		hFunction = new HelperFunctions();
		deal = (Deal) hFunction.stringToObject(extras.getString("deal"), Deal.class);
		AQUtility.debug("HTML", extras.getString("payment_html"));
		AQUtility.debug("Deal", extras.getString("deal"));
	}
	
	@Override
	public void onBackPressed (){
	    if (webview.isFocused() && webview.canGoBack()) {
	    	webview.goBack();  	    	     
	    }else {
			super.onBackPressed();
			finish();
	    }
	} 

}
