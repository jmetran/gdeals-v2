package com.stratpoint.globe.gdeals;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import com.androidquery.util.AQUtility;
import com.stratpoint.globe.gdeals.base.BaseActivity;
import com.stratpoint.globe.gdeals.models.Deal;
import com.stratpoint.globe.gdeals.utils.HelperFunctions;
import com.stratpoint.globe.gdeals.views.CameraPreview;

import android.view.View;
import android.widget.FrameLayout;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Handler;

public class QrCodeScannerActivity extends BaseActivity {

	private Bundle extras;

	private double available_points;

	private Deal deal;
	private HelperFunctions hFunction;

	ImageScanner scanner;

	private Camera mCamera;
	private CameraPreview mPreview;

	private Handler autoFocusHandler;
	
	private boolean previewing = true;
	private static String cameraError = "";
	private FrameLayout preview;
	
	static {
		System.loadLibrary("iconv");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_qr_code_scanner);

		setUpHeader(getString(R.string.back), null, false, false, true, null);

		init();

		autoFocusHandler = new Handler();
		mCamera = getCameraInstance();

		if(isCameraAvailable()){
			/* Instance barcode scanner */
			scanner = new ImageScanner();
			scanner.setConfig(0, Config.X_DENSITY, 3);
			scanner.setConfig(0, Config.Y_DENSITY, 3);

			mPreview = new CameraPreview(this, mCamera, previewCb, autoFocusCB);
			preview = (FrameLayout) findViewById(R.id.cameraPreview);
			preview.addView(mPreview);
		}else{
			alert(cameraError);
		}
	}

	private void init() {
		hFunction = new HelperFunctions();
		extras = getIntent().getExtras();
		available_points = Double.parseDouble(extras.getString("available_points"));
		deal = (Deal) hFunction.stringToObject(extras.getString("deal"), Deal.class);
	}
	
	PreviewCallback previewCb = new PreviewCallback() {
		public void onPreviewFrame(byte[] data, Camera camera) {
			Camera.Parameters parameters = camera.getParameters();
			Size size = parameters.getPreviewSize();

			Image barcode = new Image(size.width, size.height, "Y800");
			barcode.setData(data);

			int result = scanner.scanImage(barcode);

			if (result != 0) {

				SymbolSet syms = scanner.getResults();
				for (Symbol sym : syms) {
					if (isQrCodeValid(sym.getData())) {
						previewing = false;
						mCamera.setPreviewCallback(null);
						mCamera.stopPreview();
						
						AQUtility.debug(sym.getData());
						iUtil.startIntent(CheckOutGlobeRewardsActivity.class, new String[] {"deal", "available_points", "qr_code"} , new Object[] { deal, available_points, sym.getData() });
						finish();
					} else {
						previewing = false;
						mCamera.setPreviewCallback(null);
						mCamera.stopPreview();
						AQUtility.debug("Opppss");
						alert(getString(R.string.scanner_error_message));
					}
				}
			}
		}
	};
	

	public void onPause() {
		super.onPause();
		releaseCamera();
	}
	
	public void onResume() {
		super.onPause();
		if(mCamera==null){
			init();
			// cameraPreview

			autoFocusHandler = new Handler();
			mCamera = getCameraInstance();

			if(isCameraAvailable()){
				/* Instance barcode scanner */
				scanner = new ImageScanner();
				scanner.setConfig(0, Config.X_DENSITY, 3);
				scanner.setConfig(0, Config.Y_DENSITY, 3);
				preview.removeAllViews();
				mPreview = new CameraPreview(this, mCamera, previewCb, autoFocusCB);
				preview = (FrameLayout) findViewById(R.id.cameraPreview);
				preview.addView(mPreview);
			}else{
				alert(cameraError);
			}
		}
		
	}

	/** A safe way to get an instance of the Camera object. */
	public static Camera getCameraInstance() {
		Camera c = null;
		try {
			c = Camera.open();
		} catch (Exception e) {
			cameraError = e.getMessage();
		}
		return c;
	}
	
	private boolean isCameraAvailable(){
		return mCamera != null ? true : false;
	}

	private void releaseCamera() {
		if (mCamera != null) {
			previewing = false;
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mCamera = null;
		}
	}

	private Runnable doAutoFocus = new Runnable() {
		public void run() {
			if (previewing)
				mCamera.autoFocus(autoFocusCB);
		}
	};

	// Mimic continuous auto-focusing
	AutoFocusCallback autoFocusCB = new AutoFocusCallback() {
		public void onAutoFocus(boolean success, Camera camera) {
			autoFocusHandler.postDelayed(doAutoFocus, 1000);
		}
	};

	private boolean isQrCodeValid(String qrCode) {
		boolean isValid = false;
		if (qrCode.length() == 32) {
			isValid = true;
		}
		return isValid;
	}

	public void onClick(View v) {
		onBackPressed();
		flurryLogEvent("Cancelled");
	}

}
