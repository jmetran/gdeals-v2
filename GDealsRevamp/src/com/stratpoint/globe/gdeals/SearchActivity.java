package com.stratpoint.globe.gdeals;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import com.actionbarsherlock.app.SherlockFragment;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.devspark.appmsg.AppMsg;
import com.nhaarman.listviewanimations.swinginadapters.prepared.SwingBottomInAnimationAdapter;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;
import com.stratpoint.globe.gdeals.adapters.DealsAdapter;
import com.stratpoint.globe.gdeals.base.Constants;
import com.stratpoint.globe.gdeals.base.GDeals;
import com.stratpoint.globe.gdeals.events.SearchDealsEvent;
import com.stratpoint.globe.gdeals.helpers.AqHelper;
import com.stratpoint.globe.gdeals.models.Deal;
import com.stratpoint.globe.gdeals.utils.ErrorMessageUtil;

import de.greenrobot.event.EventBus;

import android.text.Html;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

public class SearchActivity extends SherlockFragment{

	private AqHelper aqHelper;	
	private AQuery aq;
	private ErrorMessageUtil errMsg;
	private List<Deal> deals;
	private EditText edit_txt_search;
	private ListView list_view;
	private DealsAdapter adapter;
	private SwingBottomInAnimationAdapter swingBottomInAnimationAdapter;
	private RelativeLayout search_main_container;
	private RelativeLayout search_deal_main_container;
	boolean isFirstSearch = true;
	private ProgressBar search_progress_bar;
	private TextView txt_view_empty;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.activity_search, null);
	}

	public void onActivityCreated(Bundle savedInstanceState) {
		
		super.onActivityCreated(savedInstanceState);
		EventBus.getDefault().register(this);
		InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
		
		init();
				
		this.getSherlockActivity().getSupportActionBar().hide();
		
		aq.id(R.id.btn_back).clicked(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				hideKeyboard();
				getActivity().onBackPressed();
			}
		});
		
		txt_view_empty.setVisibility(View.GONE);
		search_progress_bar.setVisibility(View.GONE);
		edit_txt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					hideKeyboard();
				    search_progress_bar.setVisibility(View.VISIBLE);
				    txt_view_empty.setVisibility(View.GONE);
				    
				    if(isFirstSearch){
				    	search_deal_main_container.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
					    
					    ObjectAnimator fadeIn = ObjectAnimator.ofFloat(search_deal_main_container, "alpha", 0f, 1f);
						      fadeIn.setDuration(1000);
						      AnimatorSet animatorSet = new AnimatorSet();
						      animatorSet.play(fadeIn);
						      animatorSet.start();
						      
						      animatorSet.addListener(new AnimatorListener() {
								
								@Override
								public void onAnimationStart(Animator arg0) {}
								
								@Override
								public void onAnimationRepeat(Animator arg0) {}
								
								@Override
								public void onAnimationEnd(Animator arg0) {
									search_main_container.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
								}
								
								@Override
								public void onAnimationCancel(Animator arg0) {}
							});
						      
						isFirstSearch = false;
				    }				    
				    
				    searchDeals();
					return true;
				}
				
				return false;
			}
		});			
	}
	
	private void searchDeals(){
		adapter.clear();
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.SEARCH_DEALS_URL+ "?search_text="+edit_txt_search.getText().toString()+"&access_token="+GDeals.appAccessToken(), "searchDealsCb", Constants.CACHE_DEFAULT_EXPIRE);  
		aq.ajax(cb);
	}
	
	public void showDeals(String msg){
		search_progress_bar.setVisibility(View.GONE);
		list_view.setVisibility(View.VISIBLE);
	    
		for(Deal deal: deals){
			adapter.add(deal);
		}
		
	    
	   
		if(!TextUtils.isEmpty(msg)) return;
		if(deals.size() == 0){
			txt_view_empty.setText(Html.fromHtml("No results found for \"<b>"+edit_txt_search.getText().toString()+"</b>\""));
			txt_view_empty.setVisibility(View.VISIBLE);
		}
	}
	
	private void hideKeyboard(){
		InputMethodManager inputMethodManager = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
	    inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
	}
	
	private void init(){
		aq = new AQuery(getActivity());
		aqHelper = new AqHelper(getActivity());
		errMsg = new ErrorMessageUtil(getActivity());
		deals = new ArrayList<Deal>();
		
		list_view = aq.id(R.id.list_view_search).getListView();
		search_main_container = (RelativeLayout) aq.id(R.id.search_main_container).getView();
		search_deal_main_container = (RelativeLayout) aq.id(R.id.search_deal_main_container).getView();
		edit_txt_search = aq.id(R.id.edit_txt_search).getEditText();
		search_progress_bar = aq.id(R.id.search_progress_bar).getProgressBar();
		txt_view_empty = aq.id(R.id.txt_view_empty).getTextView();
		
		adapter = new DealsAdapter(getActivity(), R.layout.deals_row, deals, aq);
		
		
		
		swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(adapter);
		swingBottomInAnimationAdapter.setInitialDelayMillis(300);
		swingBottomInAnimationAdapter.setAbsListView(list_view);
		list_view.setAdapter(swingBottomInAnimationAdapter);
	}		
	
	protected void errorMsg(String msg) {
		AppMsg.makeText(getActivity(), errMsg.getErrorMessage(msg), AppMsg.STYLE_ALERT).show();
	}
	
	public void onEvent(SearchDealsEvent event) {
		this.deals = event.deals;
		
		showDeals(event.msg);
	}
	
	@Override
	public void onPause() {
		hideKeyboard();
		super.onPause();
	}
	
	@Override
	public void onDestroy() {
		EventBus.getDefault().unregister(this);
		super.onDestroy();
	}

}
