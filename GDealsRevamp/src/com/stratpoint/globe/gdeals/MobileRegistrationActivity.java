package com.stratpoint.globe.gdeals;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Required;
import com.mobsandgeeks.saripaar.annotation.TextRule;
import com.stratpoint.globe.gdeals.R.id;
import com.stratpoint.globe.gdeals.base.BaseActivity;
import com.stratpoint.globe.gdeals.base.Constants;
import com.stratpoint.globe.gdeals.helpers.AqHelper;
import com.stratpoint.globe.gdeals.helpers.MenuHelper;
import com.stratpoint.globe.gdeals.helpers.PhoneHelper;
import com.stratpoint.globe.gdeals.helpers.RegistrationState;
import com.stratpoint.globe.gdeals.helpers.SharedPrefHelper;
import com.stratpoint.globe.gdeals.helpers.SignatureHelper;
import com.stratpoint.globe.gdeals.utils.IntentUtil;
import com.stratpoint.globe.gdeals.utils.PhoneUtil;
import com.stratpoint.globe.gdeals.utils.VerificationCodeUtil;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

public class MobileRegistrationActivity extends BaseActivity implements ValidationListener{
	
	@Required(order = 1)
	@TextRule(order = 2, minLength = 10, message = "Invalid mobile number.")
	private EditText edit_txt_mobile_no;
	private ProgressDialog progress;
	
	private Validator validator;
	
	private AqHelper aqHelper;
	private SharedPrefHelper sharedPrefHelper;
	
	private String verificationCode;
	private String mobileNumber;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mobile_registration);
		
		setUpHeader(getString(R.string.mobile_registration_title), null, false, false, false, null);
		
		init();
		setState();
		
		verificationCode = VerificationCodeUtil.generateVerificationCode(Constants.VERIFICATION_CODE_LENGTH, this);
		
		validator.setValidationListener(this);
		
		TextView txt_view_caption = aq.id(id.txt_view_caption).getTextView();
		txt_view_caption.setText(Html.fromHtml(getString(R.string.mobile_registration_instruction)));
		txt_view_caption.setMovementMethod(LinkMovementMethod.getInstance());
		
		edit_txt_mobile_no = aq.id(R.id.edit_txt_mobile_number).getEditText();
		edit_txt_mobile_no.setText(new PhoneUtil(this).getMobileNumber());
	}
	
	private void requestVerificationCode(){
		progress = new ProgressDialog(this);
		progress.setMessage("Please wait...");
		progress.setCancelable(false);
		progress.show();
		
		mobileNumber = new PhoneHelper().formatMobtel(edit_txt_mobile_no.getText().toString());
		
		Map<String, Object> params = new HashMap<String, Object>();
	    params.put("mobile_number", mobileNumber);
	    params.put("verification_code", verificationCode);
	    params.put("signature", new SignatureHelper().generateSignature(new String[] { mobileNumber, verificationCode }));
	    
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.REQUEST_VERIFICATION_CODE_URL, "requestVerificationCodeCb");  
		cb.params(params);
		cb.method(AQuery.METHOD_POST);
		aq.progress(progress).ajax(cb);
	}
	
	public void requestVerificationCodeCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		
		if (_status.getCode() == 200){			
			sharedPrefHelper.updateSharedPreferences(SharedPrefHelper.MOBILE_NO, mobileNumber);
			iUtil.startIntent(VerificationCodeActivity.class);
		}else{
			errorMsg(_status.getMessage());
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}		
	}
	
	private void init(){
		validator = new Validator(this);
		aqHelper = new AqHelper(this);	
		sharedPrefHelper = new SharedPrefHelper(this);
		iUtil = new IntentUtil(this);
	}
	
	private void setState(){
		sharedPrefHelper.updateSharedPreferences(SharedPrefHelper.REGISTRATION_STATE, RegistrationState.MOBILE_REGISTRATION);
	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.done_action, menu);
	    
	    MenuItem item = menu.findItem(R.id.btn_done);
	    MenuHelper.setMenuItemLabel(item.getActionView(), getString(R.string.done), R.drawable.img_ic_check, new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				validator.validate();
			}
		});
	    return super.onCreateOptionsMenu(menu);
    }
	
	@Override
	public void onResume ()
	{
	   super.onResume();	
	   setState();
	}

	@Override
	public void preValidation() {}

	@Override
	public void onSuccess() {
		requestVerificationCode();
		flurryLogEvent(getString(R.string.done));
	}
	
	@Override
	public void onFailure(View failedView, Rule<?> failedRule) {
		String message = failedRule.getFailureMessage();
        if (failedView instanceof EditText) {
            failedView.requestFocus();
            ((EditText) failedView).setError(message);
        }
	}

	@Override
	public void onValidationCancelled() {}

}
