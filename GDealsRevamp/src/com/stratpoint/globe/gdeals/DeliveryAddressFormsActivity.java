package com.stratpoint.globe.gdeals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Required;
import com.stratpoint.globe.gdeals.base.BaseActivity;
import com.stratpoint.globe.gdeals.base.Constants;
import com.stratpoint.globe.gdeals.base.GDeals;
import com.stratpoint.globe.gdeals.events.ItemSelectedEvent;
import com.stratpoint.globe.gdeals.fragments.LocationsDialog;
import com.stratpoint.globe.gdeals.helpers.AqHelper;
import com.stratpoint.globe.gdeals.helpers.MenuHelper;
import com.stratpoint.globe.gdeals.models.DeliveryAddress;
import com.stratpoint.globe.gdeals.utils.JsonUtil;
import com.stratpoint.globe.gdeals.utils.RemoveUnnecessaryField;

import de.greenrobot.event.EventBus;

import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.app.ProgressDialog;
import android.os.Bundle;

public class DeliveryAddressFormsActivity extends BaseActivity implements ValidationListener{

	private AqHelper aqHelper;
	
	private ProgressDialog progress;
	private CheckBox check_box;
	
	@Required(order = 1)
	private EditText edit_txt_street;
	
	@Required(order = 2)
	private EditText edit_txt_city;
	
	private EditText edit_txt_zip_code;
	
	
	private Validator validator;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_delivery_address_forms);
		
		setUpHeader(getString(R.string.delivery_address_tile), null, false, false, true, null);
		
		EventBus.getDefault().register(this);
		
		init();	
		
		validator.setValidationListener(this);
		
		edit_txt_street = aq.id(R.id.edit_txt_street).getEditText();
		edit_txt_city = aq.id(R.id.edit_txt_city).getEditText();
		edit_txt_zip_code  = aq.id(R.id.edit_txt_zip_code).getEditText();
		check_box = aq.id(R.id.check_box).getCheckBox();
		
		edit_txt_city.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(hasFocus){
					showLocations();
				}
			}
		});
		
		aq.id(R.id.main_container_holder).clicked(new View.OnClickListener() {
			public void onClick(View v) {
				if(check_box.isChecked()){
					check_box.setChecked(false);
				}else{
					check_box.setChecked(true);
				}				
			}
		});
		
    }
	
	public void onEvent(String event) {
		edit_txt_city.setText(event);
    }
	
	private void showLocations(){
		FragmentManager fr = getSupportFragmentManager();
		LocationsDialog locationsDialog = new LocationsDialog();
		locationsDialog.setRetainInstance(true);
		locationsDialog.show(fr, "locations");
	}
	
	public void onClick(View v){
		showLocations();
	}
		
	private void init(){
		aqHelper = new AqHelper(this);	
		validator = new Validator(this);
	}
	
	private void saveAddress(){
		progress = new ProgressDialog(this);
		progress.setMessage("Please wait...");
		progress.setCancelable(false);
		progress.show();
				
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("building_name", aq.id(R.id.edit_txt_bldg_name).getText().toString());
	    params.put("street", edit_txt_street.getText().toString());
	    params.put("village", aq.id(R.id.edit_txt_village).getText().toString());
	    params.put("postal", edit_txt_zip_code.getText().toString());
	    params.put("city", edit_txt_city.getText().toString());
	    params.put("country", "Philippines");
	    params.put("company_name", null);
	    params.put("state", null);
	    params.put("is_default", check_box.isChecked() ? "1" : "0");
	    params.put("user_id", GDeals.userId());
	    params.put("access_token", GDeals.appAccessToken());
	    
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.DELIVERY_ADDRESS_URL, "saveAddressCb");  
		cb.params(params);
		cb.method(AQuery.METHOD_POST);
		aq.progress(progress).ajax(cb);
		
	}
	
	public void saveAddressCb(String url, JSONObject jo, AjaxStatus status) throws JSONException{
		AQUtility.debug(jo);
		AQUtility.debug(status.getCode());
		
		if (status.getCode() == 200){
			
			Gson gson = new GsonBuilder().setExclusionStrategies(new RemoveUnnecessaryField()).create();
			
			DeliveryAddress address = gson.fromJson(JsonUtil.getJSONObject("result", jo).toString(), DeliveryAddress.class);
			
			List<DeliveryAddress>  addresses = DeliveryAddress.find(DeliveryAddress.class, "address_id=?", new String[] { address.addressId }, "", "", "1");
			
			/*
			 * Check if added address is set as default 
			 * if default update delivery address is_default=0 to reset data
			 */
			if(address.isDefault.equalsIgnoreCase("1")){
				DeliveryAddress.executeQuery("UPDATE DELIVERY_ADDRESS SET is_default='0'");
				EventBus.getDefault().post(new ItemSelectedEvent(address));
			}
			
			if(addresses.size() == 0){
				address.save();
				EventBus.getDefault().post(new ItemSelectedEvent(address, String.format(getString(R.string.successfully_saved), getString(R.string.address))));				
			}else{ 
				DeliveryAddress tmpAddress = addresses.get(0);				
				tmpAddress = address.updateDeliveryAddress(tmpAddress.getId(), address);				
				tmpAddress.save();
			}

			finish();
		}else{
			errorMsg(status.getMessage());
			AQUtility.debug("Error", status.getCode() + " " + "Status:" + status.getMessage());
		}	
	}	
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.done_action, menu);
	    
	    MenuItem item = menu.findItem(R.id.btn_done);
	    MenuHelper.setMenuItemLabel(item.getActionView(), getString(R.string.save), R.drawable.img_ic_check, new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				validator.validate();
			}
		});
	    return super.onCreateOptionsMenu(menu);
    }

	@Override
	public void preValidation() {}

	@Override
	public void onSuccess() {
		saveAddress();
		flurryLogEvent(getString(R.string.save));
	}

	@Override
	public void onFailure(View failedView, Rule<?> failedRule) {
		String message = failedRule.getFailureMessage();
        if (failedView instanceof EditText) {
            failedView.requestFocus();
            ((EditText) failedView).setError(message);
        }
	}

	@Override
	public void onValidationCancelled() {}
	
}
