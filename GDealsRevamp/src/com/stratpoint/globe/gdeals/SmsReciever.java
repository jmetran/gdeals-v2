package com.stratpoint.globe.gdeals;

import com.androidquery.util.AQUtility;
import com.stratpoint.globe.gdeals.events.SMSEvent;
import com.stratpoint.globe.gdeals.helpers.SharedPrefHelper;

import de.greenrobot.event.EventBus;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class SmsReciever extends BroadcastReceiver{

	private SharedPrefHelper sharedPrefHelper;
	
	@Override
	public void onReceive(Context context, Intent intent) {

		sharedPrefHelper = new SharedPrefHelper(context);
		
		if(sharedPrefHelper.getBoolean(SharedPrefHelper.LOGGED_IN)) return;
		
		final String DEFAULT_MESSAGE = "GDEALS: Here is your verification code: ";
		final String SENDER = "GDEALS";
		final int VERIFICATION_CHARACTERS_COUNT = 4;
		String code = "";
		
		Bundle extras = intent.getExtras();       
		if (extras != null){
			Object[] messages=(Object[])extras.get("pdus");
			SmsMessage[] sms=new SmsMessage[messages.length];
			
			for(int n=0;n<messages.length;n++){
				sms[n]=SmsMessage.createFromPdu((byte[]) messages[n]);
			}
			
			for (SmsMessage msg : sms) {
				AQUtility.debug("sender", msg.getDisplayOriginatingAddress());
				if(SENDER.equalsIgnoreCase(msg.getDisplayOriginatingAddress())){
					if(msg.getMessageBody().contains(DEFAULT_MESSAGE)){						
						try {
							code = msg.getMessageBody().substring(DEFAULT_MESSAGE.length(), DEFAULT_MESSAGE.length() + VERIFICATION_CHARACTERS_COUNT);
							EventBus.getDefault().post(new SMSEvent(code));
						} catch (Exception e) {
						}						
					}
				}
	        }
			
		}
	}

}
