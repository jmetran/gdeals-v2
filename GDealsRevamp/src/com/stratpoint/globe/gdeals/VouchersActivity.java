package com.stratpoint.globe.gdeals;

import java.util.ArrayList;
import java.util.List;

import com.androidquery.util.AQUtility;
import com.stratpoint.globe.gdeals.adapters.VoucherAdapter;
import com.stratpoint.globe.gdeals.base.BaseActivity;
import com.stratpoint.globe.gdeals.models.Voucher;
import com.stratpoint.globe.gdeals.models.VoucherType;
import com.stratpoint.globe.gdeals.utils.HelperFunctions;

import android.widget.ListView;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

public class VouchersActivity extends BaseActivity{
	
	private VoucherType voucherType;
	private List<Voucher> vouchers;
	
	private ListView list_view;
	
	private VoucherAdapter adapter;
	
	private Bundle extras;
	private HelperFunctions hFunction;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		init();
		
		setUpHeader(voucherType.name, null, false, false, true, null);
		
		if(vouchers.size() == 0){
			setContentView(R.layout.empty_voucher);
			return;
		}
		
		setContentView(R.layout.list_view);
		
		adapter = new VoucherAdapter(this, R.layout.voucher_row, vouchers, voucherType.type, aq);
		
		float scale = getResources().getDisplayMetrics().density;
		int dpAsPixels = (int) (10*scale + 0.5f);
		
		list_view = aq.id(R.id.list_view).getListView(); 
		list_view.setDivider(new ColorDrawable(getResources().getColor(R.color.parent_bg_color)));
		list_view.setDividerHeight(dpAsPixels);
		list_view.setAdapter(adapter);
    }	
	
	private void init(){
		extras = getIntent().getExtras();
		hFunction = new HelperFunctions();
		voucherType = (VoucherType) hFunction.stringToObject(extras.getString("voucherType"), VoucherType.class);
		vouchers = new ArrayList<Voucher>();
		AQUtility.debug(extras.getString("voucherType"));
		
		vouchers = voucherType.vouchers;
	}	
	
}
