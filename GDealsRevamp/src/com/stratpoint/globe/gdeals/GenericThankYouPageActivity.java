package com.stratpoint.globe.gdeals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.stratpoint.globe.gdeals.base.BaseActivity;
import com.stratpoint.globe.gdeals.base.Constants;
import com.stratpoint.globe.gdeals.base.GDeals;
import com.stratpoint.globe.gdeals.helpers.AqHelper;
import com.stratpoint.globe.gdeals.models.Deal;
import com.stratpoint.globe.gdeals.models.Payment;
import com.stratpoint.globe.gdeals.utils.HelperFunctions;
import com.stratpoint.globe.gdeals.utils.IntentUtil;
import com.stratpoint.globe.gdeals.utils.JsonUtil;
import com.stratpoint.globe.gdeals.utils.RemoveUnnecessaryField;
import com.stratpoint.globe.gdeals.utils.UiUtil;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class GenericThankYouPageActivity extends BaseActivity{

	private Bundle extras;
	
	private String paymentType = "";
	private Deal deal;
	private List<Deal> recommendedDeals;
	
	private UiUtil uiUtil;
	private AqHelper aqHelper;
	private HelperFunctions hFunction;
	
	private LinearLayout carousel_container;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_generic_thank_you_page);
		
		setUpHeader(getString(R.string.app_name), null, false, false, false, null);
		
		init();
		
		carousel_container = (LinearLayout) aq.id(R.id.carousel_container).getView();
		
		String message = getString(R.string.registration_thank_you_page_header_caption) + "\n\n" + getString(R.string.registration_thank_you_page_note);
		
		if(paymentType.equalsIgnoreCase(Payment.TYPE_RESERVE_VIA_LOAD)){
			message = getString(R.string.reserve_via_load_payment_thank_you_message);
		}else if(paymentType.equalsIgnoreCase(Payment.TYPE_COD) || paymentType.equalsIgnoreCase(Payment.TYPE_CREDIT_CARD)){
			message = getString(R.string.payment_thank_you_message);
			
			CharSequence[] instructions = getResources().getTextArray(R.array.payment_creditcard_message);
			TypedArray instructionIcons = getResources().obtainTypedArray(R.array.payment_creditcard_icons);
			LinearLayout instructions_container = (LinearLayout) findViewById(R.id.layout_holder);
			int i =0;
			
			for(CharSequence instruction: instructions){
				View v = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.unordered_list, null, false);
				TextView txt_view = (TextView) v.findViewById(R.id.txt_view_name);
				ImageView img_icon = (ImageView) v.findViewById(R.id.imageView1);
				img_icon.setImageResource(instructionIcons.getResourceId(i, -1));
				
				txt_view.setText(instruction);
				instructions_container.addView(v);
				i++;
			}
			
			aq.id(R.id.txt_view_caption).gone();
		
		}else if(paymentType.equalsIgnoreCase(Payment.TYPE_REWARD_POINTS)){
			message = getString(R.string.globe_rewards_thank_you_message);
		}
		
		aq.id(R.id.txt_view_caption).text(message);
				
		aq.id(R.id.btn_ok).clicked(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				flurryLogEvent(getString(R.string.registration_thank_you_page_next));
				new IntentUtil(GenericThankYouPageActivity.this).startIntent(MainActivity.class, true);
			}
		});
		
		if(!TextUtils.isEmpty(paymentType)){
			getRecommendation();
		}else{
			aq.id(R.id.recommend_layout_holder).gone();
		}
		
	}
	
	private void getRecommendation(){
		
		String category = "";
		
		if(!TextUtils.isEmpty(deal.categoryId)){
			category = "&category_id="+deal.categoryId;
		}
		
		if(!TextUtils.isEmpty(deal.categoryIdNew)){
			category+="&category_id_new="+deal.categoryIdNew;
		}
		
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.DEALS_FEATURED_URL+"?action="+Constants.RECO_ENGINE_ACTION_BUY+"&deal_id="+deal.id+"&page_legend="+Constants.RECO_ENGINE_PAGE_LEGEND_MOBILE_VOUCHER+"&source_page="+Constants.RECO_ENGINE_PAGE_LEGEND_DEAL_DETAILS+"&access_token="+GDeals.appAccessToken() + category +"&client_version=1.13.003&strip_html_tags=1" , "getRecommendationCb", Constants.CACHE_DEFAULT_EXPIRE);  
		aq.progress(R.id.reco_progress_bar).ajax(cb);
	}
	
	public void getRecommendationCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		
		Gson gson = new GsonBuilder().setExclusionStrategies(new RemoveUnnecessaryField()).create();
		if (_status.getCode() == 200){	
			recommendedDeals = new ArrayList<Deal>();
			
			if(JsonUtil.getJSONObject("result", _jo) !=null){
				recommendedDeals = Arrays.asList(gson.fromJson(JsonUtil.getJSONArray("deals", JsonUtil.getJSONObject("result", _jo)).toString(), Deal[].class));
			}
			
			for(final Deal deal: recommendedDeals){
				View v = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.recommended_deals, null, false);
				
				ImageView img_item = (ImageView) v.findViewById(R.id.img_item);
				TextView txt_view_header = (TextView) v.findViewById(R.id.txt_view_header);
				TextView txt_view_price = (TextView) v.findViewById(R.id.txt_view_price);
				TextView txt_view_discounted_price = (TextView) v.findViewById(R.id.txt_view_discounted_price);
				
				aq.id(img_item).image(deal.getLargeImageUrl(), false, true, 0, 0, null, AQuery.FADE_IN_NETWORK, 0);

				uiUtil.showData(deal.name, txt_view_header);
				
				uiUtil.showData(Double.parseDouble(deal.regularPrice), hFunction.formatPrice(deal.regularPrice), txt_view_price);
				uiUtil.showData(Double.parseDouble(deal.discountedPrice), hFunction.formatPrice(deal.discountedPrice), txt_view_discounted_price);
				
				if(deal.source.equalsIgnoreCase(Constants.GLOBE_REWARDS)){
					aq.id(txt_view_price).text("Min. Globe Rewards").visible();
					aq.id(txt_view_price).getTextView().setPaintFlags(aq.id(txt_view_price).getTextView().getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
					aq.id(txt_view_discounted_price).text(deal.details.dealMinRewardsPoints+ " Points").visible();
				}else{
					aq.id(txt_view_price).getTextView().setPaintFlags(aq.id(txt_view_price).getTextView().getPaintFlags()  | Paint.STRIKE_THRU_TEXT_FLAG);
				}
				
				v.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						AQUtility.debug(deal.id);					
						iUtil.startIntent(MainActivity.class, new String[] { "deal_id", "source_page" }, new Object[] { deal.id, Constants.RECO_ENGINE_PAGE_LEGEND_MOBILE_VOUCHER }, true);						
					}
				});
				
				carousel_container.addView(v);
			}
			
			if(recommendedDeals.size() == 0){
				aq.id(R.id.recommend_layout_holder).gone();
			}
						
		}else{
			getRecommendation();
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}		
	}
	
	private void init(){
		extras = getIntent().getExtras();
		aqHelper = new AqHelper(this);			
		uiUtil = new UiUtil(this, aq);
		hFunction = new HelperFunctions();		
		
		if(extras !=null && extras.containsKey("paymentType")){
			paymentType = extras.getString("paymentType");
			deal = (Deal) hFunction.stringToObject(extras.getString("deal"), Deal.class);
		}
	}
	
	@Override
	public void onBackPressed (){
	    
	}

}
