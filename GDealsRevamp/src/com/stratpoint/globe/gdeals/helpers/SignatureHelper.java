package com.stratpoint.globe.gdeals.helpers;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import com.stratpoint.globe.gdeals.base.Constants;
import com.stratpoint.globe.gdeals.utils.SecurityUtil;

public class SignatureHelper {

	StringBuffer signature;
	
	public SignatureHelper(){
		
	}
	
	public String generateSignature(String[] signatureList){
		signature = new StringBuffer();
		
		for(String str: signatureList){
			signature.append(str);
		}
		
		try {
			return SecurityUtil.generateHmacSHA1(Constants.HMAC_SHA1_KEY, signature.toString());
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return "";
	}

}
