package com.stratpoint.globe.gdeals.helpers;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefHelper {
	
	public static final String APP_NAME = "gDeals";
	public static final String MOBILE_NO = "mobileNo";
	public static final String VERIFICATION_CODE = "verificationCode";
	public static final String LOGGED_IN = "isLoggedIn";
	public static final String REGISTRATION_STATE = "registrationState";
	public static final String USER_ID = "userId";
	public static final String NEW_ACCOUNT = "newAccount";
	public static final String ACCESS_TOKEN = "accessToken";
	public static final String REQUEST_TOKEN = "requestToken";
	public static final String ADDRESS_SYNC = "addressSync";
	
	private Context context;
	private SharedPreferences sharedPrefs;
	
	public SharedPrefHelper(Context _context){
		this.context = _context;
		this.sharedPrefs = getPref();
	}
	
	public SharedPreferences getPref(){
		return context.getSharedPreferences(APP_NAME, Context.MODE_PRIVATE);
	}
	
	public SharedPreferences.Editor getPrefEditor(){
		return getPref().edit();
	}
	
	public String getString(String strKey){
		return sharedPrefs.getString(strKey, null);
	}
	
	public int getInteger(String strKey){
		return sharedPrefs.getInt(strKey, 0);
	}
	
	public boolean getBoolean(String strKey){
		return sharedPrefs.getBoolean(strKey, false);
	}
		
	public void updateSharedPreferences(String _key, Object _value){
		this.updateSharedPreferences(new String[] { _key }, new Object[] { _value });	
	}
	
	public void updateSharedPreferences(String _key[], Object _value[]){
		SharedPreferences.Editor editor = getPrefEditor();
		int keyLength = _key.length;
		for (int i=0; i < keyLength; i++) {
			
			if(_value[i] instanceof String){
				editor.putString(_key[i], (String) _value[i]);
			}else if(_value[i] instanceof Integer){
				editor.putInt(_key[i], (Integer) _value[i]);
			}else if(_value[i] instanceof Boolean){
				editor.putBoolean(_key[i], (Boolean) _value[i]);
			}						
		}
		editor.commit();
	}
	
	public void remove(){
		SharedPreferences.Editor editor = getPrefEditor();
		editor.clear();
		editor.commit();
	}

}
