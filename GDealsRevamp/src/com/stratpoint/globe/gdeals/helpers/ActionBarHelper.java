package com.stratpoint.globe.gdeals.helpers;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.stratpoint.globe.gdeals.R;
import com.stratpoint.globe.gdeals.utils.IntentUtil;

public class ActionBarHelper {
	
	private SherlockFragmentActivity activity;
	private IntentUtil iUtil;
	
	public ActionBarHelper(SherlockFragmentActivity _activity){
		this.activity = _activity;	
		this.iUtil = new IntentUtil(activity);		
	}
		
	public void setHeader(String _title, Drawable _icon, boolean showIcon, boolean buttonEnable, boolean isBack, final Class<?> backTo){
		
		if(buttonEnable){
			activity.getSupportActionBar().setHomeButtonEnabled(true);
		}		
		
		if(_icon == null){
			_icon = activity.getResources().getDrawable(R.drawable.img_ic_list);
		}
		
		if(showIcon){
			activity.getSupportActionBar().setIcon(_icon);
		}
		
		activity.getSupportActionBar().setDisplayShowCustomEnabled(true);
		activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
		
		LayoutInflater inflater = LayoutInflater.from(activity);
		View v = inflater.inflate(R.layout.action_bar_title, null);
		TextView txt_view_title = (TextView) v.findViewById(R.id.txt_view_title);
		txt_view_title.setText(_title);
		
		if(isBack){
			activity.getSupportActionBar().setDisplayShowHomeEnabled(false);
			txt_view_title.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimension(R.dimen.text_medium));
			txt_view_title.setClickable(true);
			txt_view_title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.img_ic_back, 0, 0, 0);
			txt_view_title.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if(backTo != null){
						activity.finish();
						iUtil.startIntent(backTo);
						activity.overridePendingTransition (R.anim.open_main, R.anim.close_next);
					}else{
						activity.finish();
						activity.overridePendingTransition (R.anim.open_main, R.anim.close_next);
					}										
				}
			});
		}
		
		activity.getSupportActionBar().setBackgroundDrawable(new ColorDrawable(activity.getResources().getColor(R.color.header_bg_color)));
		activity.getSupportActionBar().setCustomView(v);
	}	
	
}
