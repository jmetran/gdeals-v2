package com.stratpoint.globe.gdeals.helpers;

public class RegistrationState {

	public static final int MOBILE_REGISTRATION = 0;
	public static final int VERIFICATION_CODE = 1;
	public static final int USER_REGISTRATION = 2;
	public static final int TAGS_SELECTION = 3;
	public static final int WELCOME = 4;
	public static final int DONE = 5;
	
}
