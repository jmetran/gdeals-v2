package com.stratpoint.globe.gdeals.helpers;

public class PhoneHelper {
	
	
	/*Format mobile number to 10 digits
	 * Ex: +639054969255 will become 9054969255
	 * Ex: 09054969255 will become 9054969255 
	 */
	public String formatMobtel(String mobtel) {
		int length = mobtel.length();
		if (length > 10) {
			mobtel = mobtel.substring(length - 10);
		}
		return mobtel;
	}
	
	public String displayMobTel(String mobTel){
		return "0"+mobTel;
	}

}
