package com.stratpoint.globe.gdeals.helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.text.TextUtils;

public class DateHelper {

	/* Convert String to Date */
	public Date stringToDate(String date, String format) {
		SimpleDateFormat df = new SimpleDateFormat(format, Locale.US);
		Date retVal = null;
		try {
			Date today = df.parse(date);
			retVal = today;			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return retVal;
	}
	
	public Date convertUnixDateToDate(String strDate) {
		long dateInSecs = Long.parseLong(strDate);
		long dateInMillis = dateInSecs * 1000;
		Date date = new Date(dateInMillis);
		return date;
	}
	
	public String formatDate(String strDate, String format) {
		if(TextUtils.isEmpty(strDate)) return "";
		
		if (format == null) {
			format = "MMMM dd, yyyy";
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.US);
		return dateFormat.format(convertUnixDateToDate(strDate));
	}
	
	
	public String formatDate(Date date, String format) {
		if (format == null) {
			format = "MMMM dd, yyyy";
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.US);
		return dateFormat.format(date);
	}
	
	public String getCurrentDate(String format){
		return formatDate(new Date(), format);
	}
	
	public long dayDiff(Date _start, Date _end){
		long diffTime = _start.getTime() - _end.getTime();
		long diffDays = diffTime / (1000 * 60 * 60 * 24);
		return diffDays;
	}

}
