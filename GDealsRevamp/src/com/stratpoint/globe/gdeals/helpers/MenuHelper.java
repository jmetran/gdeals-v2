package com.stratpoint.globe.gdeals.helpers;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.stratpoint.globe.gdeals.R;

public class MenuHelper {
	
	public static void setMenuItemLabel(View view, String title, int icon, OnClickListener listener){
		TextView txt_view_title = (TextView) view.findViewById(R.id.txt_view_title);
		txt_view_title.setCompoundDrawablesWithIntrinsicBounds(icon, 0, 0, 0);
	    txt_view_title.setText(title);
	    txt_view_title.setOnClickListener(listener);
	}

}
