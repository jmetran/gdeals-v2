package com.stratpoint.globe.gdeals.helpers;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.app.Activity;

import com.androidquery.callback.AjaxCallback;
import com.stratpoint.globe.gdeals.base.Constants;
import com.stratpoint.globe.gdeals.utils.SSLSocketFactoryUtil;

public class AqHelper {
	
	private Activity activity;
	private Map<String, String> headers;
	
	public AqHelper(Activity _activity){
		this.activity = _activity;
		headers = new HashMap<String, String>();
		headers.put("User-Agent", Constants.MOBILE_AGENT);
	}
	
	public AjaxCallback<JSONObject> ajaxCallback(String _url, String _callback, long expire){
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(_url).type(JSONObject.class).fileCache(true).expire(expire);
        cb.setTimeout(Constants.NETWORK_TIMEOUT);
        cb.setSSF(SSLSocketFactoryUtil.getSSLSocketFactory(activity));
        
        if (_callback != null){
        	cb.weakHandler(activity, _callback);
        }
        
        for(String name: headers.keySet()){
        	cb.header(name, headers.get(name));
    	}
        return cb;
	}
	
	public AjaxCallback<Object> ajax(String _url, Class<?> type, long expire, AjaxCallback<Object> cb){
		for(String name: headers.keySet()){
        	cb.header(name, headers.get(name));
    	}
		return cb.type((Class<Object>) type).url(_url).expire(expire);
	}
	
	public AjaxCallback<Object> ajaxObjectCallback(String _url, String _callback, long expire, Class<?> _class){
		AjaxCallback<Object> cb = new AjaxCallback<Object>();
        		
		cb.url(_url).type((Class<Object>) _class).fileCache(true).expire(expire);
        
        if (_callback != null){
        	cb.weakHandler(activity, _callback);
        }
        
        for(String name: headers.keySet()){
        	cb.header(name, headers.get(name));
    	}
        return cb;
	}
	
	
	public AjaxCallback<String> ajaxStringCallback(String _url, String _callback, long expire){
		AjaxCallback<String> cb = new AjaxCallback<String>();
        cb.url(_url).type(String.class).fileCache(true).expire(expire);
        if (_callback != null){
        	cb.weakHandler(activity, _callback);
        }
        for(String name: headers.keySet()){
        	cb.header(name, headers.get(name));
    	}
        return cb;
	}
	
	public AjaxCallback<String> ajaxStringCallback(String _url, String _callback){
		return ajaxStringCallback(_url, _callback, Constants.CACHE_RESET);
	}
	
	public AjaxCallback<JSONObject> ajaxCallback(String _url, String _callback){
		return ajaxCallback(_url, _callback, Constants.CACHE_RESET);
	}
	
	public AjaxCallback<JSONObject> ajaxCallback(String _url){
		return ajaxCallback(_url, null, Constants.CACHE_RESET);
	}

}
