package com.stratpoint.globe.gdeals.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONObject;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.stratpoint.globe.gdeals.models.Category;
import com.stratpoint.globe.gdeals.models.User;
import com.stratpoint.globe.gdeals.utils.JsonUtil;
import com.stratpoint.globe.gdeals.utils.RemoveUnnecessaryField;

public class UserRegistrationHelper {
	
	private SharedPrefHelper sharedPrefHelper;
	
	public UserRegistrationHelper(Context context){
		sharedPrefHelper = new SharedPrefHelper(context);
	}
	
	public User parseAndSaveUserDetails(JSONObject _jo){
		//Parse and Save User
//		Gson gson = new GsonBuilder().create();
		Gson gson = new GsonBuilder().setExclusionStrategies(new RemoveUnnecessaryField()).create();
		User user = gson.fromJson(JsonUtil.getString("user", JsonUtil.getJSONObject("result", _jo)), User.class);	
		
		sharedPrefHelper.updateSharedPreferences(SharedPrefHelper.USER_ID, user.userId);
		
		List<User>  users = User.find(User.class, "user_id=?", new String[] { user.userId }, "", "", "1");
		
		if(users.size() == 0){ //Create new
			user.save();
		}else{ //Update user
			User tmpUser = users.get(0);				
			tmpUser = user.updateUser(tmpUser.getId(), user);				
			tmpUser.save();
		}
		
		//Parse and Save Categories
		List<Category> categories = new ArrayList<Category>();
		categories = Arrays.asList(gson.fromJson(JsonUtil.getString("categories", JsonUtil.getJSONObject("result", _jo)), Category[].class));
		
		for(Category category: categories){
			
			List<Category>  foundCategories = Category.find(Category.class, "category_id=?", new String[] { category.categoryId }, "", "", "1");
			
			if(foundCategories.size() == 0){
				category.save();
			}else{
				Category updateTag = foundCategories.get(0);
				updateTag = category.updateTag(updateTag.getId(), category);
				updateTag.save();
			}

		}
		
		return user;
	}
	
	
	
	

}
