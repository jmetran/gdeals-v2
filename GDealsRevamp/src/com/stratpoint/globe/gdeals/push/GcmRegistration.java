package com.stratpoint.globe.gdeals.push;

import android.os.AsyncTask;
import android.os.Bundle;

import com.androidquery.util.AQUtility;
import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.stratpoint.globe.gdeals.base.BaseActivity;
import com.stratpoint.globe.gdeals.base.Constants;
import com.stratpoint.globe.gdeals.utils.GCMServerUtilities;

public class GcmRegistration extends BaseActivity{

	protected AsyncTask<Void, Void, Void> mRegisterTask;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Make sure the device has the proper dependencies.
        GCMRegistrar.checkDevice(this);
        // Make sure the manifest was properly set - comment out this line
        // while developing the app, then uncomment it when it's ready.
        GCMRegistrar.checkManifest(this);
	}
	
	public void registerPushNotification(){
		if (checkPlayServices()){
			final String regId = GCMRegistrar.getRegistrationId(this);
			if (regId.equals("")) {
	            // Automatically registers application on startup.
	            GCMRegistrar.register(this, Constants.SENDER_ID);
	        } else {
	            // Device is already registered on GCM, check server.
	            if (GCMRegistrar.isRegisteredOnServer(this)) {
	                // Skips registration.
	            	AQUtility.debug("Registered", regId);
	            } else {
	            	AQUtility.debug("server REGISTER");
	            	
                    mRegisterTask = new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected Void doInBackground(Void... params) {
                        	boolean registered = GCMServerUtilities.register(GcmRegistration.this, regId);
                            if (!registered) {
                                GCMRegistrar.unregister(GcmRegistration.this);
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void result) {
                            mRegisterTask = null;
                        }

                    };
                    mRegisterTask.execute(null, null, null);
                    
	            }
	        }
			
			
        }else{
        	AQUtility.debug("Google play service is unavailable");
        }
	}
	
	/**
	 * Check the device to make sure it has the Google Play Services APK. If
	 * it doesn't, display a dialog that allows users to download the APK from
	 * the Google Play Store or enable it in the device's system settings.
	 */
	private boolean checkPlayServices() {
	    int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
	    if (resultCode != ConnectionResult.SUCCESS) {
	        return false;
	    }
	    return true;
	}	
    
    @Override
	protected void onDestroy() {
		if (mRegisterTask != null) {
            mRegisterTask.cancel(true);
        }
        GCMRegistrar.onDestroy(getApplicationContext());
		super.onDestroy();
	}
	
}
