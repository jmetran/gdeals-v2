package com.stratpoint.globe.gdeals;

import com.stratpoint.globe.gdeals.adapters.StoreAdapter;
import com.stratpoint.globe.gdeals.base.BaseActivity;
import com.stratpoint.globe.gdeals.models.Deal;
import com.stratpoint.globe.gdeals.models.StoreDetails;
import com.stratpoint.globe.gdeals.utils.HelperFunctions;
import com.stratpoint.globe.gdeals.utils.IntentUtil;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class StoreSelectionActivity extends BaseActivity{
	
	private StoreDetails storeDetails;
	private Deal deal;
	
	private StoreAdapter adapter;
	private ListView list_view;
	
	private HelperFunctions hFunction;
	private IntentUtil iUtil;
	private Bundle extras;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_store_selection);
		
		setUpHeader(getString(R.string.back), null, false, false, true, null);
		
		init();	
		
		adapter = new StoreAdapter(this, R.layout.store_row, storeDetails.stores, aq);
		list_view.setAdapter(adapter);
		
		list_view.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				iUtil.startIntent(CartAdealsActivity.class, new String[] { "deal", "store", "storeDetails" }, new Object[] { deal, storeDetails.stores.get(position), storeDetails });
				flurryLogEvent(storeDetails.stores.get(position).name);
			}
		});
		
	}

	private void init(){
		extras = getIntent().getExtras();
		iUtil = new IntentUtil(this);
		list_view = aq.id(R.id.list_view).getListView();
		hFunction = new HelperFunctions();
		deal = (Deal) hFunction.stringToObject(extras.getString("deal"), Deal.class);
		storeDetails = (StoreDetails) hFunction.stringToObject(extras.getString("storeDetails"), StoreDetails.class);
		iUtil = new IntentUtil(this);
	}	

}
