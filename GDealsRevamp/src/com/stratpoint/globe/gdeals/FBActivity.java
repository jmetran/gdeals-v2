package com.stratpoint.globe.gdeals;

import java.util.Collection;
import java.util.List;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.stratpoint.globe.gdeals.base.BaseActivity;
import com.stratpoint.globe.gdeals.base.Constants;

import android.content.Intent;
import android.os.Bundle;

public class FBActivity extends BaseActivity{

	private UiLifecycleHelper uiHelper;
	
	private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		uiHelper = new UiLifecycleHelper(this, callback);
		uiHelper.onCreate(savedInstanceState);
	}	
	
	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
		switch (state) {
		case CLOSED_LOGIN_FAILED:
			alert("Login failed.");
			break;
		case CLOSED:
			info("Logged out of facebook.");
			break;
		case OPENED:
			onLoggedInToFacebook(session);
			break;
		case OPENED_TOKEN_UPDATED:
			onLoggedInToFacebook(session);
			break;
		}
	}
	
	protected void onLoggedInToFacebook(Session session) {
		info("Logged in to Facebook");
	}
	
	@Override
    public void onResume() {
        super.onResume();
        uiHelper.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }
	
	
	protected void requestPublishPermission(Session session){
		if (session != null) {
			List<String> permissions = session.getPermissions();
	        if (!isSubsetOf(Constants.FB_PUBLISH_PERMISSIONS, permissions)) {
	            Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(this, Constants.FB_PUBLISH_PERMISSIONS);
	            session.requestNewPublishPermissions(newPermissionsRequest);
	            return;
	        }
		}
	}	
	
	protected boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}
	
	
}
