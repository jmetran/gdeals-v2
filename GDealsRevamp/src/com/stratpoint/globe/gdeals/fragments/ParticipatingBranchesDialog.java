package com.stratpoint.globe.gdeals.fragments;

import java.util.Arrays;
import java.util.List;

import com.androidquery.AQuery;
import com.androidquery.util.AQUtility;
import com.google.gson.Gson;
import com.stratpoint.globe.gdeals.R;
import com.stratpoint.globe.gdeals.adapters.MerchantsAdapter;
import com.stratpoint.globe.gdeals.models.Merchant;

import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.os.Bundle;

public class ParticipatingBranchesDialog extends DialogFragment{
	
	private List<Merchant> merchants;
	
	private Bundle extras;
	
	private Gson gson = new Gson();
	
	private MerchantsAdapter adapter;
	
	private AQuery aq;
	private ListView list_view;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.list_view, null);
		
		list_view = (ListView) view.findViewById(R.id.list_view);
		
		return view;
	}
	
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	
		extras = getArguments();
		
		getDialog().setTitle(R.string.participating_branches);
		
		init();	
		
		adapter = new MerchantsAdapter(getActivity(), R.layout.merchant_row, merchants, aq);
		
		list_view.setAdapter(adapter);
		
		AQUtility.debug(extras.getString("merchants"));
	}
	
	private void init(){
		aq = new AQuery(getActivity());
		merchants = Arrays.asList(gson.fromJson(extras.getString("merchants"), Merchant[].class));
	}
	
}
