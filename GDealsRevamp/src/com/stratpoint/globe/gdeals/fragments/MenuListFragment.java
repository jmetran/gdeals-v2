package com.stratpoint.globe.gdeals.fragments;

import java.util.ArrayList;
import java.util.List;

import com.actionbarsherlock.app.SherlockFragment;
import com.androidquery.AQuery;
import com.stratpoint.globe.gdeals.ProfileActivity;
import com.stratpoint.globe.gdeals.R;
import com.stratpoint.globe.gdeals.SplashScreenActivity;
import com.stratpoint.globe.gdeals.TagsSelectionActivity;
import com.stratpoint.globe.gdeals.VoucherTypeActivity;
import com.stratpoint.globe.gdeals.WebViewActivity;
import com.stratpoint.globe.gdeals.adapters.MenuAdapter;
import com.stratpoint.globe.gdeals.base.Constants;
import com.stratpoint.globe.gdeals.events.CategorySelectedEvent;
import com.stratpoint.globe.gdeals.events.ItemSelectedEvent;
import com.stratpoint.globe.gdeals.helpers.SharedPrefHelper;
import com.stratpoint.globe.gdeals.models.Category;
import com.stratpoint.globe.gdeals.models.DealMenuChildItem;
import com.stratpoint.globe.gdeals.models.DealMenuItem;
import com.stratpoint.globe.gdeals.models.DeliveryAddress;
import com.stratpoint.globe.gdeals.models.User;
import com.stratpoint.globe.gdeals.utils.AlertUtil;
import com.stratpoint.globe.gdeals.utils.IntentUtil;

import de.greenrobot.event.EventBus;

import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;

public class MenuListFragment extends SherlockFragment {
	
	private AQuery aq;
	private MenuAdapter adapter;
	private SparseArray<DealMenuItem> dealMenuItems;
	private IntentUtil iUtil;
	private AlertUtil alertUtil;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.expandable_list_view, null);
	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		init();
				
		String[] mnuLabels = getResources().getStringArray(R.array.menu_items);
		TypedArray mnuIcons = getResources().obtainTypedArray(R.array.menu_icons);
		
		
		List<Category> categories = new ArrayList<Category>();
		categories.addAll(Category.find(Category.class, "is_active=?", "2"));
		categories.addAll(Category.find(Category.class, "is_active=? AND name NOT IN('Food','Shopping','Leisure','Services','Health','Sights','Entertainment')", "1"));
		
		int len = mnuLabels.length;
		for (int i = 0; i < len; i++) {
			DealMenuItem dealMenuItem = new DealMenuItem(mnuLabels[i], mnuIcons.getResourceId(i, -1), 0);
			
			if(mnuLabels[i].equalsIgnoreCase("categories")){
				for(Category category: categories){
					dealMenuItem.childDealMenu.add(new DealMenuChildItem(category.name, category.categoryIconInt(category.name, getActivity().getApplicationContext()), 0, category.categoryId));
				}
			}
			dealMenuItems.append(i, dealMenuItem);
		}
		
		ExpandableListView expandable_list_view = (ExpandableListView) getActivity().findViewById(R.id.expandable_list_view);
		
		expandable_list_view.setGroupIndicator(null);
		expandable_list_view.setDivider(new ColorDrawable(getActivity().getResources().getColor(R.color.slider_menu_devider_bg)));
		expandable_list_view.setChildDivider(new ColorDrawable(getActivity().getResources().getColor(R.color.slider_menu_devider_bg)));
		expandable_list_view.setDividerHeight(1);
		
		adapter = new MenuAdapter(getActivity(), dealMenuItems, aq);
		expandable_list_view.setAdapter(adapter);
		
		expandable_list_view.setOnGroupClickListener(new OnGroupClickListener() {
			
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
				
				DealMenuItem dealMenuItem = dealMenuItems.get(groupPosition);
				
				if(!dealMenuItem.name.equalsIgnoreCase(getString(R.string.menu_categories))){
					
					EventBus.getDefault().post(new ItemSelectedEvent(dealMenuItem.name));
					
					if(dealMenuItem.name.equalsIgnoreCase(getString(R.string.menu_home))){
						EventBus.getDefault().post(new CategorySelectedEvent(new DealMenuChildItem("", 0, 0, "")));
					}else{
						if(dealMenuItem.name.equalsIgnoreCase(getString(R.string.menu_vouchers))){
							iUtil.startIntent(VoucherTypeActivity.class);
						}else if(dealMenuItem.name.equalsIgnoreCase(getString(R.string.menu_preferences))){
							iUtil.startIntent(TagsSelectionActivity.class);
						}else if(dealMenuItem.name.equalsIgnoreCase(getString(R.string.menu_settings))){
							
						}else if(dealMenuItem.name.equalsIgnoreCase(getString(R.string.menu_profile))){
							iUtil.startIntent(ProfileActivity.class);
						}else if(dealMenuItem.name.equalsIgnoreCase(getString(R.string.menu_faq))){
							iUtil.startIntent(WebViewActivity.class, "url", Constants.GDEAL_FAQ_URL);
						}else if(dealMenuItem.name.equalsIgnoreCase(getString(R.string.menu_contact_us))){
							iUtil.sendEmail(getString(R.string.contact_us_email_support), getString(R.string.contact_us_email_subject), "");
						}else if(dealMenuItem.name.equalsIgnoreCase(getString(R.string.menu_logout))){
							
							alertUtil.alertConfirmation(
											getString(R.string.logout_title),
											getString(R.string.logout_message),
											new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface arg0, int arg1) {
													new SharedPrefHelper(getActivity()).remove();
													User.deleteAll(User.class);
													DeliveryAddress.deleteAll(DeliveryAddress.class);
													iUtil.startIntent(SplashScreenActivity.class, true);
												}
											}, new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface arg0, int arg1) {
													
												}
											});						
						}
						EventBus.getDefault().post(new CategorySelectedEvent(null));
					}					
				}
				return false;
			}
		});
		
		
	}
	
	private void init(){
		aq = new AQuery(getActivity());
		dealMenuItems = new SparseArray<DealMenuItem>();
		iUtil = new IntentUtil(getActivity());
		alertUtil = new AlertUtil(getActivity());
	}

}
