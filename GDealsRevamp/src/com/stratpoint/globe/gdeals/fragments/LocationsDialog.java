package com.stratpoint.globe.gdeals.fragments;

import java.util.ArrayList;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

import com.orm.query.Select;
import com.stratpoint.globe.gdeals.R;
import com.stratpoint.globe.gdeals.adapters.LocationsAdapter;
import com.stratpoint.globe.gdeals.models.Location;

import de.greenrobot.event.EventBus;

import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.os.Bundle;

public class LocationsDialog extends DialogFragment{
	
	private LocationsAdapter adapter;
	
	private StickyListHeadersListView list_view;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.stickylist_header_view, null);
		
		list_view = (StickyListHeadersListView) view.findViewById(R.id.list_view);
		
		return view;
	}
	
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	
		getDialog().setTitle(R.string.select_location);
		
		List<Location> metroLocations = Select.from(Location.class).where("parent=?", new String[] { "Metro Manila" }).orderBy("name").list();
		List<Location> otherLocations = Select.from(Location.class).where("parent!=?", new String[] { "Metro Manila" }).orderBy("name").list();
		List<Location> allLocations = new ArrayList<Location>();
		
		allLocations.addAll(metroLocations);
		
		for(Location loc: otherLocations){
			loc.parent = "Outside Metro Manila";
			allLocations.add(loc);
		}
		
		adapter = new LocationsAdapter(getActivity(), R.layout.merchant_row, allLocations);
		
		list_view.setAdapter(adapter);
		list_view.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				EventBus.getDefault().post(adapter.getItem(position).name);
				getDialog().dismiss();
			}
		});
		
	}
	
	
}
