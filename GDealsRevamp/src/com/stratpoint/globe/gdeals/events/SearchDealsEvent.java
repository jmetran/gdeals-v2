package com.stratpoint.globe.gdeals.events;

import java.util.List;

import com.stratpoint.globe.gdeals.models.Deal;

public class SearchDealsEvent {

	public List<Deal> deals;
	public String msg;
	
	public SearchDealsEvent(List<Deal> deals, String msg){
		this.deals = deals;
		this.msg = msg;
	}
	
}
