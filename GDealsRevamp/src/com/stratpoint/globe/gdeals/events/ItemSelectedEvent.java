package com.stratpoint.globe.gdeals.events;

public class ItemSelectedEvent {

	public Object object;
	public String msg;
	
	public ItemSelectedEvent(Object object){
		this.object = object;
	}	
	
	public ItemSelectedEvent(Object object, String msg){
		this.object = object;
		this.msg = msg;
	}
	
}
