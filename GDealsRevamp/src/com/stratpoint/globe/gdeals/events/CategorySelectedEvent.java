package com.stratpoint.globe.gdeals.events;

public class CategorySelectedEvent {

	public Object object;
	public String msg;
	
	public CategorySelectedEvent(Object object){
		this.object = object;
	}	
	
	public CategorySelectedEvent(Object object, String msg){
		this.object = object;
		this.msg = msg;
	}
	
}
