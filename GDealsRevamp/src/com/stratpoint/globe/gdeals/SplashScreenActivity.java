package com.stratpoint.globe.gdeals;

import com.stratpoint.globe.gdeals.base.BaseActivity;
import com.stratpoint.globe.gdeals.helpers.RegistrationState;
import com.stratpoint.globe.gdeals.helpers.SharedPrefHelper;
import com.stratpoint.globe.gdeals.utils.IntentUtil;

import android.os.Bundle;

public class SplashScreenActivity extends BaseActivity {

	private SharedPrefHelper sharedPrefHelper;
	private IntentUtil iUtil;
	protected int splashTime = 3000; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getSupportActionBar().hide();
		setContentView(R.layout.activity_splash_screen);
		
		init();
		validateAccount();
	}
	
	private void init(){
		sharedPrefHelper = new SharedPrefHelper(this);
		iUtil = new IntentUtil(this);
	}
	
	private void validateAccount(){
		
		Thread splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    synchronized(this){
                    	wait(splashTime);
                    }
                } catch(InterruptedException e) {}
                finally {
                	if(!sharedPrefHelper.getBoolean(SharedPrefHelper.LOGGED_IN)){
            			switch (sharedPrefHelper.getInteger(SharedPrefHelper.REGISTRATION_STATE)) {
            			case RegistrationState.MOBILE_REGISTRATION:
            				iUtil.startIntent(MobileRegistrationActivity.class, true);
            				break;
            			case RegistrationState.VERIFICATION_CODE:
            				iUtil.startIntent(VerificationCodeActivity.class, true);
            				break;
            			case RegistrationState.USER_REGISTRATION:
            				iUtil.startIntent(UserRegistrationActivity.class, true);
            				break;
            			case RegistrationState.TAGS_SELECTION:
            				iUtil.startIntent(TagsSelectionActivity.class, true);
            				break;
            			}			            			
            		}else{
            			sharedPrefHelper.updateSharedPreferences(SharedPrefHelper.NEW_ACCOUNT, false);
            			iUtil.startIntent(MainActivity.class, true);
            		}
                }
            }
        };
        splashTread.start();
		
	}

}
