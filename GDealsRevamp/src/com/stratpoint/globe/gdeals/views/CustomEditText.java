package com.stratpoint.globe.gdeals.views;

import com.stratpoint.globe.gdeals.R;
import com.stratpoint.globe.gdeals.utils.CustomFont;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class CustomEditText extends EditText{

	public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		setFontStyle(context, attrs);
	}

	public CustomEditText(Context context) {
		super(context);
				
		Typeface typeface = CustomFont.typeFace(context, 1);
		this.setTypeface(typeface);
	}

	public CustomEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		setFontStyle(context, attrs);
	}
	
	private void setFontStyle(Context context, AttributeSet attrs){
		if (attrs != null) {
			TypedArray styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.TypefacedTextView);
			int typefaceCode = styledAttrs.getInt(R.styleable.TypefacedTextView_fontStyle, -1);
			styledAttrs.recycle();
	
			// Typeface.createFromAsset doesn't work in the layout editor.
			// Skipping...
			if (isInEditMode()) {
				return;
			}
			
			Typeface typeface = CustomFont.typeFace(context, (typefaceCode == -1) ? 1 : typefaceCode);
			this.setTypeface(typeface);
		}
	}

}
