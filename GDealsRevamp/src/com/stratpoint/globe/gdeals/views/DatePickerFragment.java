package com.stratpoint.globe.gdeals.views;

import java.util.Calendar;
import java.util.Date;

import com.stratpoint.globe.gdeals.helpers.DateHelper;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{

	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
		Bundle extras = getArguments();
		
		final Calendar c = Calendar.getInstance();
		
		if(extras != null && extras.containsKey("date")){
			DateHelper dateHelper = new DateHelper();
			Date date = dateHelper.stringToDate(extras.getString("date"), "yyyy-MM-dd");
			c.setTime(date);			
		}
		
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
    	
    }
	
}

