package com.stratpoint.globe.gdeals;

import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.stratpoint.globe.gdeals.base.BaseActivity;

public class WebViewActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		WebView webview = new WebView(this);
		setContentView(webview);

		setUpHeader(getString(R.string.back), null, false, false, true, null);

		Bundle extras = getIntent().getExtras();
		webview.getSettings().setJavaScriptEnabled(true);
		
		webview.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
			}
		});
		webview.setWebViewClient(new WebViewClient() {
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				alert(description);
			}
		});
		webview.loadUrl(extras.containsKey("url") ? extras.getString("url") : "");

	}

}
