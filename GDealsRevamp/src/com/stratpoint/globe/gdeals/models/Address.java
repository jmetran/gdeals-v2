package com.stratpoint.globe.gdeals.models;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class Address {

	@SerializedName("street_address")
	public String streetAddress;
	
	public String city;
	public String state;
	public String postcode;
	public String country;		
	
	
	public String getCompleteAddress() {
		StringBuilder addressBuilder = new StringBuilder();
		addressBuilder.append(TextUtils.isEmpty(streetAddress) ? "" : streetAddress + ", ");
		addressBuilder.append(TextUtils.isEmpty(city) ? "" : city + ", ");
		addressBuilder.append(TextUtils.isEmpty(state) ? "" : state + ", ");
		addressBuilder.append(country);		
		return addressBuilder.toString();
	}
	
}
