package com.stratpoint.globe.gdeals.models;

import com.google.gson.annotations.SerializedName;

public class Store {

	@SerializedName("store_id")
	public String id;
	
	@SerializedName("store_name")
	public String name;
	
	@SerializedName("store_price")
	public String price;
	
	@SerializedName("load_price")
	public String loadPrice;
	
}
