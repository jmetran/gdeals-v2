package com.stratpoint.globe.gdeals.models;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

public class DeliveryAddress  extends SugarRecord<DeliveryAddress>{

	@SerializedName("building_name")
	public String building_name;
	
	@SerializedName("id")
	public String addressId;
	
	public String village;
	public String postal;	
	
	@SerializedName("company_name")
	public String companyName;
	public String street;
	public String state;	
	
	@SerializedName("is_default")
	public String isDefault;
	
	public String city;
	public String country;	
	
	@Ignore
	public boolean isSelected;
	
	public DeliveryAddress(){		
	}
	
	public DeliveryAddress updateDeliveryAddress(long id, DeliveryAddress deliveryAddress){
		deliveryAddress.setId(id);
		return deliveryAddress;		
	}
	
	public String getCompleteAddress() {
		StringBuilder addressBuilder = new StringBuilder();
		addressBuilder.append(TextUtils.isEmpty(companyName) ? "" : companyName + ", ");
		addressBuilder.append(TextUtils.isEmpty(building_name) ? "" : building_name + ", ");
		addressBuilder.append(TextUtils.isEmpty(street) ? "" : street + ", ");
		addressBuilder.append(TextUtils.isEmpty(village) ? "" : village + ", ");
		addressBuilder.append(TextUtils.isEmpty(city) ? "" : city + ", ");
		addressBuilder.append(TextUtils.isEmpty(postal) ? "" : postal + ", ");
		addressBuilder.append(TextUtils.isEmpty(state) ? "" : state + ", ");
		addressBuilder.append(country);		
		return addressBuilder.toString();
	}
	
}
