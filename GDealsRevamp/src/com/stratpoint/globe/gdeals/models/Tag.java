package com.stratpoint.globe.gdeals.models;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;

public class Tag {

	public String name;
	public boolean isActive;
	
	public Tag(String name, boolean isActive){
		this.name = name;
		this.isActive = isActive;
	}
	
	public Drawable tagBackground(String name, Context context) {
		try{
			Resources resources = context.getResources();
			final int resourceId = context.getResources().getIdentifier("selector_category_" + name.toLowerCase(), "drawable", context.getPackageName());
			return resources.getDrawable(resourceId);
		}catch(Exception e){
			return null;
		}
	}
	
}
