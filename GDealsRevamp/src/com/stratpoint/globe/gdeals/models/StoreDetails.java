package com.stratpoint.globe.gdeals.models;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class StoreDetails {

	public List<Store> stores;
	
	@SerializedName("tariff_code")
	public String tariffCode;
	
	@SerializedName("merchant_name")
	public String merchantName;
	
	@SerializedName("available_coupons")
	public String availableCoupons;
	
	@SerializedName("allow_credit_card")
	public String allowCreditCard;
	
	@SerializedName("allow_cod")
	public String allowCod;
	
	@SerializedName("allow_load")
	public String allowLoad;
	
	@SerializedName("min_quantity_per_transaction")
	public int minCCQuantity;
	
	@SerializedName("max_quantity_per_transaction")
	public int maxCCQuantity;
	
	@SerializedName("min_cod_quantity")
	public int minCodQuantity;
	
	@SerializedName("max_cod_quantity")
	public int maxCodQuantity;
	
	@SerializedName("min_load_quantity")
	public int minLoadQuantity;
	
	@SerializedName("max_load_quantity")
	public int maxLoadQuantity;
	
	public StoreDetails(){
		stores = new ArrayList<Store>();
	}
	
}
