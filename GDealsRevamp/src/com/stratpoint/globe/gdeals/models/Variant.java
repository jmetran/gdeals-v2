package com.stratpoint.globe.gdeals.models;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Variant {

	public static final String LABEL_COLOR = "Color";
	public static final String LABEL_SIZE = "Size";
	public static final String LABEL_PAX = "Pax";
	public static final String LABEL_DESIGN = "Design";
	public static final String LABEL_HOTEL = "Hotel";
	public static final String LABEL_LENGTH = "Length";
	
	public String id;
	public String name;
	public String qty;
	public String price;
	
	@SerializedName("discount")
	public String discountedPrice;
	
	@SerializedName("color")
	public List<VariantItem> colors;
	
	@SerializedName("size")
	public List<VariantItem> sizes;
	
	@SerializedName("hotel")
	public List<VariantItem> hotels;
	
	@SerializedName("pax")
	public List<VariantItem> paxes;
	
	@SerializedName("design")
	public List<VariantItem> designs;
	
	@SerializedName("length")
	public List<VariantItem> length;
	
	public Variant() {
		colors = new ArrayList<VariantItem>();
		sizes = new ArrayList<VariantItem>();
		paxes = new ArrayList<VariantItem>();
		designs = new ArrayList<VariantItem>();
		hotels = new ArrayList<VariantItem>();
		length = new ArrayList<VariantItem>();
	}
	
}
