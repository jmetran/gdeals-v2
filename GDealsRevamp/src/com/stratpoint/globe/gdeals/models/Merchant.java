package com.stratpoint.globe.gdeals.models;

import com.google.gson.annotations.SerializedName;

public class Merchant{
	
	public String id;
	public String name;
	public String image;
	public String latitude;
	public String longitude;
	public String distance;
	public String relevance;
	
	@SerializedName("branch_name")
	public String branchName;
	
	@SerializedName("category_id")
	public String categoryId;
	
	@SerializedName("is_twoanyone")
	public String isTwoAnyone;
	
	public Address address;
	
	public Merchant(){		
	}
	
	public String getBranchNameOnly() {
		return branchName.replace(name + " - ", "");
	}
	
}