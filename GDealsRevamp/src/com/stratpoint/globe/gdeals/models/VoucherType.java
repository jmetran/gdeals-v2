package com.stratpoint.globe.gdeals.models;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.google.gson.annotations.SerializedName;

public class VoucherType {

	public String name;
	public String type;
	public int count;
	
	@SerializedName("nodes")
	public List<Voucher> vouchers;
	
	public VoucherType(){
		vouchers = new ArrayList<Voucher>();
	}
	
	public int voucherIconInt(String name, Context context) {
		try{
			return context.getResources().getIdentifier("img_ic_" + name.replace(" ", "_").toLowerCase(), "drawable", context.getPackageName());
		}catch(Exception e){
			return 0;
		}
	}
	
}
