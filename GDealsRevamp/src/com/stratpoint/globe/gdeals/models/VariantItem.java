package com.stratpoint.globe.gdeals.models;

import com.google.gson.annotations.SerializedName;

public class VariantItem {

	public String id;
	public String name;
	public String qty;
	public String price;
	
	@SerializedName("discount")
	public String discountedPrice;
	
	public VariantItem() {}
	
	public VariantItem(String id, String name, String qty, String price, String discountedPrice) {
		this.id = id;
		this.name = name;
		this.qty = qty;
		this.price = price;
		this.discountedPrice = discountedPrice;
	}
	
}
