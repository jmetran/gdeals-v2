package com.stratpoint.globe.gdeals.models;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class Voucher {

	@SerializedName("redemption_id")
	public String redemptionId;
	
	@SerializedName("category_id")
	public String categoryId;
	
	@SerializedName("name")
	public String dealName;
	
	public String image;
	public String price;
	
	@SerializedName("regular_price")
	public String regularPrice;
	
	@SerializedName("discounted_price")
	public String discountedPrice;
	
	@SerializedName("discount_percentage")
	public String discountPercentage;
	
	@SerializedName("data")
	public DealDetails details;
	
	public class DealDetails{
		
		@SerializedName("deal_source_text")
		public String deal_source_text;
		
		public String description;
		public String terms;
	}
	
	public String source;
	
	@SerializedName("redemption_start")
	public String redemptionStart;
	
	@SerializedName("redemption_end")
	public String redemptionEnd;
	
	@SerializedName("days_before_redemption")
	public String daysBeforeRedemption;
	
	@SerializedName("store_name")
	public String storeName;
	
	@SerializedName("payment_type")
	public String paymentType;
	
	@SerializedName("coupon_code")
	public String couponCode;
	
	@SerializedName("paid_amount")
	public String paidAmount;
	
	@SerializedName("remaining_balance")
	public String remainingBalance;
	
	@SerializedName("redemption_code")
	public String redemptionCode;
	
	@SerializedName("date_redeemed")
	public String dateRedeemed;
	
	@SerializedName("redemption_color_code")
	public String redemptionColorCode;
	
	@SerializedName("deal_display_color_code")
	public String dealDisplayColorCode;
	
	public String note;
	
	@SerializedName("purchased_date")
	public String datePurchased;
	
	@SerializedName("delivery_note")
	public String deliveryNote;
	
	@SerializedName("merchant_hotline_info")
	public String merchantHotlineInfo;
	
	
	
	public String getSmallImageUrl() {
		if (TextUtils.isEmpty(image)) {
			return "";
		} else {
			return image.replace("https:", "http:").replace("_thumb", "_small");
		}
	}
	
}
