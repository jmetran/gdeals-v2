package com.stratpoint.globe.gdeals.models;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class Deal {

	public String id;
	
	@SerializedName("merchant_id")
	public String merchantId;
	
	@SerializedName("category_id")
	public String categoryId;
	
	@SerializedName("category_id_new")
	public String categoryIdNew;
	
	@SerializedName("company_name")
	public String companyName;
	
	public String name;
	
	@SerializedName("branch_name")
	public String branchName;
	
	@SerializedName("is_exclusive")
	public String isExclusive;
	
	public String image;
	
	@SerializedName("image_carousel")
	public String imageCarousel;
	
	public Address address;
	
	@SerializedName("data")
	public Details details;
	
	public String latitude;
	public String longitude;
	
	@SerializedName("deal_start_date")
	public String dealStartDate;
	
	@SerializedName("deal_end_date")
	public String dealEndDate;
	
	public class Details{
		@SerializedName("points_expiry")
		public String pointsExpiry;
		
		@SerializedName("perk_points")
		public String perkPoints;
		
		public String description;
		public String terms;
		
		@SerializedName("dti_number")
		public String dtiNumber;	
		
		@SerializedName("url_link")
		public String urlLink;
		
		@SerializedName("vendor_url")
		public String vendorUrl;
		
		@SerializedName("groupon_link")
		public String grouponLink;	
		
		@SerializedName("rating_avg")
		public String ratingAvg;
		
		@SerializedName("rating_count")
		public String ratingCount;	
		
		@SerializedName("claim_count")
		public String claimCount;
		
		@SerializedName("purchase_count")
		public String purchaseCount;
		
		@SerializedName("deal_min_rewards_points")
		public String dealMinRewardsPoints;
		
		@SerializedName("deal_source_text")
		public String dealSourceText;
	}
	
	@SerializedName("regular_price")
	public String regularPrice;
	
	@SerializedName("discounted_price")
	public String discountedPrice;
	
	@SerializedName("discount_percentage")
	public String discountPercentage;
	
	@SerializedName("redemption_start")
	public String redemptionStart;
	
	@SerializedName("redemption_end")
	public String redemptionEnd;
	
	public String source;
	
	@SerializedName("sancus_menu_flag")
	public String sancusMenuFlag;
	
	public String relevance;
	
	@SerializedName("redemption_color_code")
	public String redemptionColorCode;
	
	@SerializedName("deal_display_color_code")
	public String dealDisplayColorCode;
	
	@SerializedName("variants")
	public Variant variant;
	
	public Deal(){
		categoryIdNew = "";
	}
	
	public String getAddress(){
		return address.streetAddress + " " + address.city  + ", " + address.state;
	}
	
	public String getLargeImageUrl() {
		if (TextUtils.isEmpty(image)) {
			return "";
		} else {
			return image.replace("https:", "http:").replace("_thumb", "_large");
		}
	}
}