package com.stratpoint.globe.gdeals.models;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class GlobeRewardsMerchant{
	
	public String name;
	
	public Address address;
	
	public GlobeRewardsMerchant(){		
	}
	
	@SerializedName("data")
	public Details details;
	
	public class Details{
		@SerializedName("delivery_no")
		public List<String> deliveryNo;
		
		public Details(){
			deliveryNo = new ArrayList<String>();
		}
	}
	
}