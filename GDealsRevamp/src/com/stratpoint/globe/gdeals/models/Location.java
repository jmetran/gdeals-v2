package com.stratpoint.globe.gdeals.models;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

public class Location extends SugarRecord<Location>{
	
	
	@SerializedName("id")
	public String locationId;
	
	public String name;
	public String parent;
	
	public Location(){
	}
	
	public Location updateLocation(long id, Location location){
		location.setId(id);
		return location;		
	}
	
}
