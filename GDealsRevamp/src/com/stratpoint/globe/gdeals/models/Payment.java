package com.stratpoint.globe.gdeals.models;

public interface Payment {
	public static final String TYPE_CREDIT_CARD = "credit";
	public static final String TYPE_COD = "cod";
	public static final String TYPE_RESERVE_VIA_LOAD = "load";
	public static final String TYPE_REWARD_POINTS = "globe_rewards";

}