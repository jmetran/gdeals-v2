package com.stratpoint.globe.gdeals.models;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

public class User extends SugarRecord<User>{
	
	@SerializedName("fname")
	public String firstName;
	
	@SerializedName("lname")
	public String lastName;
	
	@SerializedName("birth_date")
	public String birthDate;
	
	@SerializedName("gdeals_mobile_number")
	public String mobileNumber;
	
	@SerializedName("delivery_address")
	public String deliveryAddress;
	
	@SerializedName("id")
	public String userId;
	
	public String nickname;
	public String email;
	public String username;
	public String image;
	public String address;
	public String gender;
	
	@SerializedName("facebook_id")
	public String facebookId;
	
	@SerializedName("qr_code")
	public String qrCode;
	
	public User(){
	}
	
	public String getFullName(){
		return firstName + " " + lastName;
	}	
	
	public User updateUser(long id, User user){
		user.setId(id);
		return user;		
	}
	
}
