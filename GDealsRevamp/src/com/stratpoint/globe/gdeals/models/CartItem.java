package com.stratpoint.globe.gdeals.models;

public class CartItem {
	
	public Deal deal;
	public int qty;
	public String paymentType;
	public String variantName;
	public String dealProvider;
	
	public Store store;
	
	public VariantItem variantItem;
}
