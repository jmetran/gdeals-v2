package com.stratpoint.globe.gdeals.models;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Banner {

	@SerializedName("banner")
	public List<BannerItem> banners;
	
	@SerializedName("hero_deal")
	public List<Deal> deals;
	
	public Banner(){
		banners = new ArrayList<Banner.BannerItem>();
		deals = new ArrayList<Deal>();
	}
	
	public class BannerItem{
		public String id;
		public String name;
		public String image;
	}
	
}
