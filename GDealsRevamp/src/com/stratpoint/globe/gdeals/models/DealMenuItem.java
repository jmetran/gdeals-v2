package com.stratpoint.globe.gdeals.models;

import java.util.ArrayList;
import java.util.List;

public class DealMenuItem {
	
	public String name;
	public int iconRes;
	public int color;
	
	public final List<DealMenuChildItem> childDealMenu = new ArrayList<DealMenuChildItem>();
		
	public DealMenuItem(String name, int iconRes, int color) {
		this.name = name; 
		this.iconRes = iconRes;
		this.color = color;
	}
}
