package com.stratpoint.globe.gdeals.models;

import com.google.gson.annotations.SerializedName;

public class ResultMessage {

	public String message;
	public String status;
	
	@SerializedName("server_date")
	public String serverDate;
	
	public ResultMessage(){
		
	}
}
