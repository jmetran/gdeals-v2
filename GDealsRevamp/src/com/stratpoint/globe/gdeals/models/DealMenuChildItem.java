package com.stratpoint.globe.gdeals.models;

public class DealMenuChildItem {
	
	public String name;
	public int iconRes;
	public int color;
	public String categoryId;
		
	public DealMenuChildItem(String name, int iconRes, int color, String categoryId) {
		this.name = name; 
		this.iconRes = iconRes;
		this.color = color;
		this.categoryId = categoryId;
	}
}
