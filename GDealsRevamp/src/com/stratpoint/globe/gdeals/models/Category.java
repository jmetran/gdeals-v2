package com.stratpoint.globe.gdeals.models;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

public class Category extends SugarRecord<Category>{
	
	@SerializedName("deal_icon")
	public String icon;
	
	@SerializedName("id")
	public String categoryId;
	
	@SerializedName("merchant_icon")
	public String merchantIcon;
	
	@SerializedName("is_active")
	public String isActive;
	
	@SerializedName("map_pin")
	public String mapPin;
	
	public String name;
	public String ordinality;
	
	public Category(){		
	}
	
	public Category(String name, String categoryId){
		this.name = name;
		this.categoryId = categoryId;
	}

	public Category updateTag(long id, Category tag){
		tag.setId(id);
		return tag;		
	}
	
	public Drawable categoryIcon(String name, Context context) {
		try{
			Resources resources = context.getResources();
			final int resourceId = context.getResources().getIdentifier("img_ic_category_badge_" + name.replace(" ", "_").toLowerCase(), "drawable", context.getPackageName());
			return resources.getDrawable(resourceId);
		}catch(Exception e){
			return null;
		}
	}
	
	public int categoryIconInt(String name, Context context) {
		try{
			final int resourceId = context.getResources().getIdentifier("img_ic_category_badge_" + name.replace(" ", "_").toLowerCase(), "drawable", context.getPackageName());
			return resourceId;
		}catch(Exception e){
			return 0;
		}
	}
	
}
