package com.stratpoint.globe.gdeals;

import com.stratpoint.globe.gdeals.base.BaseActivity;
import com.stratpoint.globe.gdeals.helpers.DateHelper;
import com.stratpoint.globe.gdeals.models.Deal;
import com.stratpoint.globe.gdeals.utils.HelperFunctions;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.content.Context;
import android.os.Bundle;

public class RewardInstructionsActivity extends BaseActivity{
	
	private Bundle extras;
	
	private String available_points;
	
	private Deal deal;
	private HelperFunctions hFunction;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reward_instructions);
		
		setUpHeader(getString(R.string.reward_instruction_title), null, false, false, true, null);
		
		init();	
		aq.id(R.id.txt_view_reward_date).text(String.format(getString(R.string.reward_instruction_as_xdate), new DateHelper().getCurrentDate("MMMM dd, yyyy hh:mm a")));		
		aq.id(R.id.txt_view_rewards).text(available_points+" Points");
		
		CharSequence[] instructions = getResources().getTextArray(R.array.reward_mechanics);
		LinearLayout instructions_container = (LinearLayout) findViewById(R.id.rewards_instructions_container);
		
		for(CharSequence instruction: instructions){
			View v = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.unordered_list, null, false);
			TextView txt_view = (TextView) v.findViewById(R.id.txt_view_name);
			txt_view.setText(instruction);
			instructions_container.addView(v);
		}
    }
	
	private void init(){
		hFunction = new HelperFunctions();
		extras = getIntent().getExtras();
		available_points = extras.getString("available_points");
		deal = (Deal) hFunction.stringToObject(extras.getString("deal"), Deal.class);
	}
	
	public void onClick(View v){
		switch (v.getId()) {
		case R.id.btn_ok:
			iUtil.startIntent(QrCodeScannerActivity.class, new String[] {"deal", "available_points"} , new Object[] { deal, available_points });
			flurryLogEvent(getString(R.string.confirm));
			break;
		case R.id.btn_cancel:
			onBackPressed();
			break;
		}
	}
	
}
