package com.stratpoint.globe.gdeals;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import com.facebook.Session;
import com.facebook.widget.LoginButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Required;
import com.stratpoint.globe.gdeals.base.Constants;
import com.stratpoint.globe.gdeals.helpers.AqHelper;
import com.stratpoint.globe.gdeals.helpers.DateHelper;
import com.stratpoint.globe.gdeals.helpers.MenuHelper;
import com.stratpoint.globe.gdeals.helpers.RegistrationState;
import com.stratpoint.globe.gdeals.helpers.SharedPrefHelper;
import com.stratpoint.globe.gdeals.helpers.SignatureHelper;
import com.stratpoint.globe.gdeals.helpers.UserRegistrationHelper;
import com.stratpoint.globe.gdeals.models.ResultMessage;
import com.stratpoint.globe.gdeals.models.User;
import com.stratpoint.globe.gdeals.utils.JsonUtil;
import com.stratpoint.globe.gdeals.views.DatePickerFragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class UserRegistrationActivity extends FBActivity implements ValidationListener{

	@Required(order = 1)
	private EditText edit_txt_first_name;
	
	@Required(order = 2)
	private EditText edit_txt_last_name;
	
	@Required(order = 3)
	private EditText edit_txt_nickname;

	private EditText edit_txt_birthday;
	
	private RadioGroup rdo_group_gender;
	
	private LoginButton btn_fb_login;
	
	private Validator validator;
	
	private ProgressDialog progress;
	
	private SharedPrefHelper sharedPrefHelper;
	private AqHelper aqHelper;
	private DateHelper dateHelper;
	
	private Date currDate;
	private Date selectedDate;
	
	private String gender;
	private User user;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_registration);
		
		setUpHeader(getString(R.string.user_registration_title), null, false, false, false, null);
		
		init();
		setState();
		
		validator.setValidationListener(this);
		
		edit_txt_birthday = aq.id(R.id.edit_txt_birthday).getEditText();
		
		edit_txt_first_name = aq.id(R.id.edit_txt_first_name).getEditText();
		edit_txt_last_name = aq.id(R.id.edit_txt_last_name).getEditText();
		edit_txt_nickname = aq.id(R.id.edit_txt_nickname).getEditText();
		
		btn_fb_login = (LoginButton) findViewById(R.id.btn_fb_login);
		
		btn_fb_login.setReadPermissions(Constants.FB_READ_PERMISSIONS);
		
		rdo_group_gender = (RadioGroup) findViewById(R.id.rdo_group);
		rdo_group_gender.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rdo_btn_female:
					gender = getString(R.string.female);
					break;
				case R.id.rdo_btn_male:
					gender = getString(R.string.male);	
					break;						
				}
			}
		});
		
		edit_txt_birthday.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(hasFocus){
					showDatePicker();
				}
			}
		});
		
	}
	
	@Override
	protected void onLoggedInToFacebook(Session session) {
		super.onLoggedInToFacebook(session);

		registerViaFacebook(session);
		flurryLogEvent("FB Login");
	}
	
	//1. REGISTER VIA MOBILE
		private void processRegistration(){
			progress = new ProgressDialog(this);
			progress.setMessage("Please wait...");
			progress.setCancelable(false);
			progress.show();
			
			String mobileNo = sharedPrefHelper.getString(SharedPrefHelper.MOBILE_NO);
			String fName = edit_txt_first_name.getText().toString();
			String lName = edit_txt_last_name.getText().toString();
			String nickname = edit_txt_nickname.getText().toString();
			String birthday = edit_txt_birthday.getText().toString();
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("fname", fName);
		    params.put("lname", lName);
		    params.put("nickname", nickname);
		    params.put("mobile_number", mobileNo);
		    params.put("birth_date", birthday);
		    params.put("gender", gender.toLowerCase());
		    
		    params.put("signature", new SignatureHelper().generateSignature(new String[] { birthday, fName, gender.toLowerCase(), lName, mobileNo, nickname }));
		    
			AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.USER_MOBILE_REGISTRATION_URL, "processRegistrationCb");  
			cb.params(params);
			cb.method(AQuery.METHOD_POST);
			aq.progress(progress).ajax(cb);
		}
		
		public void processRegistrationCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
			AQUtility.debug(_jo);
			AQUtility.debug(_status.getCode());
			Gson gson = new GsonBuilder().create();
			if (_status.getCode() == 200){			
				parseAndSaveUserDetails(_jo);
			}else if(_status.getCode() == 400){	
				ResultMessage result = gson.fromJson(_status.getError(), ResultMessage.class);
				if(result.message.contains("already existing")){
					login();
				}				
			}else{
				
				if(user.userId != null){
					getOAuthRequestToken();
				}
				
				errorMsg(_status.getMessage());
				AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
			}		
		}
		
	//This will handle error registration due to internet connections lost
		private void login(){
			progress = new ProgressDialog(this);
			progress.setMessage("Please wait...");
			progress.setCancelable(false);
			progress.show();
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("mobile_number", sharedPrefHelper.getString(SharedPrefHelper.MOBILE_NO));
		    
			AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.MOBILE_LOGIN_URL, "loginCb");  
			cb.params(params);
			cb.method(AQuery.METHOD_POST);
			aq.progress(progress).ajax(cb);
			
		}
		
		public void loginCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
			AQUtility.debug(_jo);
			AQUtility.debug(_status.getCode());
			
			if (_status.getCode() == 200){	
				
				parseAndSaveUserDetails(_jo);
			}else{
				errorMsg(_status.getMessage());
				AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
			}		
		}
		
		
		
	private void parseAndSaveUserDetails(JSONObject _jo){
		user = new UserRegistrationHelper(this).parseAndSaveUserDetails(_jo);
		getOAuthRequestToken();
	}
		
	
	//1. REGISTER VIA FB
	private void registerViaFacebook(Session session){
		progress = new ProgressDialog(this);
		progress.setMessage("Please wait...");
		progress.setCancelable(false);
		progress.show();
		
		String mobileNo = sharedPrefHelper.getString(SharedPrefHelper.MOBILE_NO);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("fb_access_token", session.getAccessToken());
		params.put("fb_access_token_expiry_date", session.getExpirationDate());
		params.put("mobile_number", mobileNo);
	    params.put("signature", new SignatureHelper().generateSignature(new String[] { session.getAccessToken(), mobileNo }));
	    
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.FB_OAUTH_REGISTRATION_URL, "registerViaFacebookCb");  
		cb.params(params);
		cb.method(AQuery.METHOD_POST);
		aq.progress(progress).ajax(cb);
	}
	
	public void registerViaFacebookCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		Gson gson = new GsonBuilder().create();
		if (_status.getCode() == 200){			
			
			parseAndSaveUserDetails(_jo);				
			
		}else if(_status.getCode() == 400){	
			ResultMessage result = gson.fromJson(_status.getError(), ResultMessage.class);
			if(result.message.contains("already existing")){
				login();
			}				
		}else{
			if(user.userId != null){
				getOAuthRequestToken();
			}
			
			errorMsg(_status.getMessage());
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}		
	}
	
	//2. GET REQUEST TOKEN
	private void getOAuthRequestToken(){
		progress = new ProgressDialog(this);
		progress.setMessage("Please wait...");
		progress.setCancelable(false);
		progress.show();
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("client_id", Constants.GDEALS_APP_CLIENT_ID);
	    params.put("client_secret", Constants.GDEALS_APP_CLIENT_SECRET);
	    params.put("user_id", user.userId);
	    params.put("response_type", "request_token");
	    params.put("scope", "app.mobile");
	    
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.REQUEST_ACCESS_TOKEN_URL, "getOAuthRequestTokenCb");  
		cb.params(params);
		cb.method(AQuery.METHOD_POST);
		aq.progress(progress).ajax(cb);
	}
	
	public void getOAuthRequestTokenCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		
		if (_status.getCode() == 200){			
			sharedPrefHelper.updateSharedPreferences(SharedPrefHelper.REQUEST_TOKEN, JsonUtil.getString("request_token", JsonUtil.getJSONObject("result", _jo)));			
			getOAuthAccessToken();
		}else{
			errorMsg(_status.getMessage());
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}		
	}
	
	//3. GET ACCESS TOKEN
	private void getOAuthAccessToken(){
		progress = new ProgressDialog(this);
		progress.setMessage("Please wait...");
		progress.setCancelable(false);
		progress.show();
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("client_id", Constants.GDEALS_APP_CLIENT_ID);
	    params.put("client_secret", Constants.GDEALS_APP_CLIENT_SECRET);
	    params.put("grant_type", "authorization_code");
	    params.put("request_token", sharedPrefHelper.getString(SharedPrefHelper.REQUEST_TOKEN));
	    
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.ACCESS_TOKEN_URL, "getOAuthAccessTokenCb");  
		cb.params(params);
		cb.method(AQuery.METHOD_POST);
		aq.progress(progress).ajax(cb);
	}
	
	public void getOAuthAccessTokenCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		
		if (_status.getCode() == 200){		
			AQUtility.debug(JsonUtil.getString("access_token", JsonUtil.getJSONObject("result", _jo)));
			sharedPrefHelper.updateSharedPreferences(SharedPrefHelper.ACCESS_TOKEN, JsonUtil.getString("access_token", JsonUtil.getJSONObject("result", _jo)));			
			gotoTagsSelection();
		}else{
			errorMsg(_status.getMessage());
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}		
	}
	
	private void gotoTagsSelection(){
		sharedPrefHelper.updateSharedPreferences(SharedPrefHelper.NEW_ACCOUNT, true);
		sharedPrefHelper.updateSharedPreferences(SharedPrefHelper.REGISTRATION_STATE, RegistrationState.TAGS_SELECTION);
		iUtil.startIntent(TagsSelectionActivity.class, true);
	}
	
	private void init(){
		validator = new Validator(this);
		aqHelper = new AqHelper(this);	
		sharedPrefHelper = new SharedPrefHelper(this);
		dateHelper = new DateHelper();
	}
	
	private void setState(){
		sharedPrefHelper.updateSharedPreferences(SharedPrefHelper.REGISTRATION_STATE, RegistrationState.USER_REGISTRATION);
	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.done_action, menu);
	    
	    MenuItem item = menu.findItem(R.id.btn_done);
	    MenuHelper.setMenuItemLabel(item.getActionView(), getString(R.string.done), R.drawable.img_ic_check, new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				validator.validate();
			}
		});
	    return super.onCreateOptionsMenu(menu);
    }
	
	public void onClick(View v){
		showDatePicker();
	}
	
	private void showDatePicker(){
		DialogFragment datePicker = new DatePickerFragment(){
			@Override
			public void onDateSet(DatePicker view, int year, int month, int day) {

				Calendar c = Calendar.getInstance();
				currDate = dateHelper.stringToDate((c.get(Calendar.MONTH)+1)+"/"+c.get(Calendar.DAY_OF_MONTH)+"/"+c.get(Calendar.YEAR), "MM/dd/yyyy");
				selectedDate = dateHelper.stringToDate((month+1)+"/"+day+"/"+year, "MM/dd/yyyy");
				
				if(currDate.compareTo(selectedDate) < 0){
					alert(getString(R.string.future_date_not_allowed));
				}else{
					edit_txt_birthday.setText(dateHelper.formatDate(selectedDate, "yyyy-MM-dd"));
				}
				
			}
			
		};
		datePicker.show(getSupportFragmentManager(), "datePicker");
	}

	@Override
	public void preValidation() {}

	@Override
	public void onSuccess() {
		
		if(TextUtils.isEmpty(edit_txt_birthday.getText().toString())){
			alert(String.format(getString(R.string.required), getString(R.string.birthday)));
		}else if(TextUtils.isEmpty(gender)){
			alert(String.format(getString(R.string.required), getString(R.string.gender)));
		}else{
			processRegistration();
			flurryLogEvent(getString(R.string.done));
		}
		
	}
	
	@Override
	public void onFailure(View failedView, Rule<?> failedRule) {
		String message = failedRule.getFailureMessage();
        if (failedView instanceof EditText) {
            failedView.requestFocus();
            ((EditText) failedView).setError(message);
        }
	}

	@Override
	public void onValidationCancelled() {}
	
	@Override
	public void onResume ()
	{
	   super.onResume();	
	   setState();
	}

}
