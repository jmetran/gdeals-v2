package com.stratpoint.globe.gdeals;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.androidquery.util.AQUtility;
import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;
import com.stratpoint.globe.gdeals.base.Constants;
import com.stratpoint.globe.gdeals.utils.GCMServerUtilities;

public class GCMIntentService extends GCMBaseIntentService{

	public GCMIntentService() {
        super(Constants.SENDER_ID);
    }
	
	@Override
    protected void onRegistered(Context context, String registrationId) {
		AQUtility.debug("Device registered: regId = " + registrationId);
		GCMServerUtilities.register(context, registrationId);
    }

    @Override
    protected void onUnregistered(Context context, String registrationId) {
    	AQUtility.debug("Device unRegistered: regId = " + registrationId);
        if (GCMRegistrar.isRegisteredOnServer(context)) {
        	GCMRegistrar.setRegisteredOnServer(this, false);
        } else {
            Log.i(TAG, "Ignoring unregister callback");
        }
    }

    @Override
    protected void onMessage(Context context, Intent intent) {
    	AQUtility.debug(intent.toString());
    	Bundle extras = intent.getExtras();
    	AQUtility.debug(extras.get("id"));
    	AQUtility.debug(extras.get("message"));
        // notifies user
        generateNotification(context, intent);
    }

    @Override
    protected void onDeletedMessages(Context context, int total) {}

    @Override
    public void onError(Context context, String errorId) {
    	AQUtility.debug("Registration Error");
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {        
        return super.onRecoverableError(context, errorId);
    }

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private static void generateNotification(Context context, Intent _intent) {
    	Bundle extras = _intent.getExtras();
        
    	int id = 1;
    	int icon = R.drawable.ic_launcher;
    	String title = context.getString(R.string.app_name);
    	String msg = extras.getString("message");
    	
    	PendingIntent resultPendingIntent;
    	Intent resultIntent;
    	
    	if(_intent.hasExtra("id")){
    		resultIntent = new Intent(context, DealDetailsActivity.class);
    		resultIntent.putExtra("deal_id", extras.getString("id"));
    		
    		TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        	// Adds the back stack
        	stackBuilder.addParentStack(DealDetailsActivity.class);
        	// Adds the Intent to the top of the stack
        	stackBuilder.addNextIntent(resultIntent);
        	// Gets a PendingIntent containing the entire back stack
        	resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
    		
        }else{
        	resultIntent = new Intent(context, MainActivity.class);
        	resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        	resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent, 0);
        }
    	
    	NotificationCompat.Builder builder = new NotificationCompat.Builder(context).setSmallIcon(icon).setContentTitle(title).setAutoCancel(true).setStyle(new NotificationCompat.BigTextStyle().bigText(msg)).setContentText(msg);
    	builder.setContentIntent(resultPendingIntent);
    	NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    	mNotificationManager.notify(id, builder.build());
    	    	
    }
	

}
