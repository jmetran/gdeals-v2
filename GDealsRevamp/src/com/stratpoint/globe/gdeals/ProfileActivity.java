package com.stratpoint.globe.gdeals;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Required;
import com.stratpoint.globe.gdeals.base.BaseActivity;
import com.stratpoint.globe.gdeals.base.Constants;
import com.stratpoint.globe.gdeals.base.GDeals;
import com.stratpoint.globe.gdeals.helpers.AqHelper;
import com.stratpoint.globe.gdeals.helpers.DateHelper;
import com.stratpoint.globe.gdeals.helpers.MenuHelper;
import com.stratpoint.globe.gdeals.helpers.PhoneHelper;
import com.stratpoint.globe.gdeals.helpers.SharedPrefHelper;
import com.stratpoint.globe.gdeals.helpers.SignatureHelper;
import com.stratpoint.globe.gdeals.models.User;
import com.stratpoint.globe.gdeals.utils.IntentUtil;
import com.stratpoint.globe.gdeals.views.DatePickerFragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class ProfileActivity extends BaseActivity implements ValidationListener{

	private AqHelper aqHelper;
	private SharedPrefHelper sharedPrefHelper;
	private DateHelper dateHelper;
	
	@Required(order = 1)
	private EditText edit_txt_first_name;
	
	@Required(order = 2)
	private EditText edit_txt_last_name;
	
	@Required(order = 3)
	private EditText edit_txt_nickname;

	private EditText edit_txt_birthday;
	
	private RadioGroup rdo_group_gender;
	private RadioButton rdo_btn_male;
	private RadioButton rdo_btn_female;
	
	private ProgressDialog progress;
	
	private Validator validator;
	
	private Date currDate;
	private Date selectedDate;
	private String gender;
	
	private User user;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_profile);
		
		setUpHeader(getString(R.string.menu_profile), null, false, false, true, null);
		
		init();	
		
		validator.setValidationListener(this);
		
		edit_txt_birthday = aq.id(R.id.edit_txt_birthday).getEditText();
		edit_txt_first_name = aq.id(R.id.edit_txt_first_name).getEditText();
		edit_txt_last_name = aq.id(R.id.edit_txt_last_name).getEditText();
		edit_txt_nickname = aq.id(R.id.edit_txt_nickname).getEditText();
		rdo_btn_male = (RadioButton) findViewById(R.id.rdo_btn_male);
		rdo_btn_female = (RadioButton) findViewById(R.id.rdo_btn_female);
		
		rdo_group_gender = (RadioGroup) findViewById(R.id.rdo_group);
		rdo_group_gender.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rdo_btn_female:
					gender = getString(R.string.female);
					break;
				case R.id.rdo_btn_male:
					gender = getString(R.string.male);	
					break;						
				}
			}
		});
		
		edit_txt_birthday.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(hasFocus){
					showDatePicker();
				}
			}
		});	
		user = User.find(User.class, "user_id=?", new String[] { sharedPrefHelper.getString(SharedPrefHelper.USER_ID) }, "", "", "1").get(0);
		
		aq.id(R.id.edit_txt_birthday).text(user.birthDate);
		aq.id(R.id.edit_txt_first_name).text(user.firstName);
		aq.id(R.id.edit_txt_last_name).text(user.lastName);
		aq.id(R.id.edit_txt_nickname).text(user.nickname);
		
		if(user.gender.equalsIgnoreCase("male")){
			rdo_btn_male.setChecked(true);
		}else{
			rdo_btn_female.setChecked(true);
		}
		
		aq.id(R.id.txt_view_header).text(new PhoneHelper().displayMobTel(GDeals.mobileNo()));
		
	}
	
	private void save(){
		progress = new ProgressDialog(this);
		progress.setMessage("Please wait...");
		progress.setCancelable(false);
		progress.show();
		
		String accessToken = GDeals.appAccessToken();
		String fName = edit_txt_first_name.getText().toString();
		String lName = edit_txt_last_name.getText().toString();
		String nickname = edit_txt_nickname.getText().toString();
		String birthday = edit_txt_birthday.getText().toString();
		String userId = sharedPrefHelper.getString(SharedPrefHelper.USER_ID);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("fname", fName);
	    params.put("lname", lName);
	    params.put("nickname", nickname);
	    params.put("birth_date", birthday);
	    params.put("gender", gender.toLowerCase());
	    params.put("user_id", userId);
	    params.put("access_token", accessToken);
	    params.put("signature", new SignatureHelper().generateSignature(new String[] { accessToken, birthday, fName, gender.toLowerCase(), lName, nickname, userId }));
	    
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.PROFILE_UPDATE_URL, "saveCb");  
		cb.params(params);
		cb.method(AQuery.METHOD_PUT);
		aq.progress(progress).ajax(cb);
	}
	
	public void saveCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		
		if (_status.getCode() == 200){			
			user.firstName = edit_txt_first_name.getText().toString();
			user.lastName = edit_txt_last_name.getText().toString();
			user.nickname = edit_txt_nickname.getText().toString();
			user.gender = gender.toLowerCase();
			user.birthDate = edit_txt_birthday.getText().toString();
			user.save();
			info(String.format(getString(R.string.successfully_saved), getString(R.string.menu_profile)));
		}else{
			errorMsg(_status.getMessage());
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}		
	}
	
	private void init(){
		aqHelper = new AqHelper(this);	
		iUtil = new IntentUtil(this);
		sharedPrefHelper = new SharedPrefHelper(this);
		dateHelper = new DateHelper();
		validator = new Validator(this);
	}
	
	public void onClick(View v){
		showDatePicker();
	}
	
	private void showDatePicker(){
		DialogFragment datePicker = new DatePickerFragment(){
			@Override
			public void onDateSet(DatePicker view, int year, int month, int day) {

				Calendar c = Calendar.getInstance();
				currDate = dateHelper.stringToDate((c.get(Calendar.MONTH)+1)+"/"+c.get(Calendar.DAY_OF_MONTH)+"/"+c.get(Calendar.YEAR), "MM/dd/yyyy");
				selectedDate = dateHelper.stringToDate((month+1)+"/"+day+"/"+year, "MM/dd/yyyy");
				
				if(currDate.compareTo(selectedDate) < 0){
					alert(getString(R.string.future_date_not_allowed));
				}else{
					edit_txt_birthday.setText(dateHelper.formatDate(selectedDate, "yyyy-MM-dd"));
				}
				
			}
			
		};
		Bundle bundle = new Bundle();
		bundle.putString("date", edit_txt_birthday.getText().toString());
		datePicker.setArguments(bundle);
		datePicker.show(getSupportFragmentManager(), "datePicker");
	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.done_action, menu);
	    
	    MenuItem item = menu.findItem(R.id.btn_done);
	    MenuHelper.setMenuItemLabel(item.getActionView(), getString(R.string.save), R.drawable.img_ic_check, new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				validator.validate();
			}
		});
	    return super.onCreateOptionsMenu(menu);
    }

	@Override
	public void preValidation() {}

	@Override
	public void onSuccess() {
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(rdo_group_gender.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
        
		if(TextUtils.isEmpty(edit_txt_birthday.getText().toString())){
			alert(String.format(getString(R.string.required), getString(R.string.birthday)));
		}else if(TextUtils.isEmpty(gender)){
			alert(String.format(getString(R.string.required), getString(R.string.gender)));
		}else{
			save();
			flurryLogEvent(getString(R.string.save));
		}
		
	}

	@Override
	public void onFailure(View failedView, Rule<?> failedRule) {
		String message = failedRule.getFailureMessage();
        if (failedView instanceof EditText) {
            failedView.requestFocus();
            ((EditText) failedView).setError(message);
        }
	}

	@Override
	public void onValidationCancelled() {}

}
