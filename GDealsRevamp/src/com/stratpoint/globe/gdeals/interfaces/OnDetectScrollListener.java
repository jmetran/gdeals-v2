package com.stratpoint.globe.gdeals.interfaces;

public interface OnDetectScrollListener {
	
	void onUpScrolling();
    void onDownScrolling();

}
