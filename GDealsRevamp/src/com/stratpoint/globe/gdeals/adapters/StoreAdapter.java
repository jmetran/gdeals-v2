package com.stratpoint.globe.gdeals.adapters;

import java.util.List;

import com.androidquery.AQuery;
import com.stratpoint.globe.gdeals.R;
import com.stratpoint.globe.gdeals.models.Store;
import com.stratpoint.globe.gdeals.utils.HelperFunctions;
import com.stratpoint.globe.gdeals.utils.UiUtil;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class StoreAdapter extends ArrayAdapter<Store> {

	private Activity activity;
	private int view;
	private List<Store> stores;
	private AQuery aq;
	private UiUtil uiUtil;
	private HelperFunctions hFunction;

	public StoreAdapter(Activity activity, int view, List<Store> stores, AQuery aq) {
		super(activity, view, stores);
		this.activity = activity;
		this.view = view;
		this.stores = stores;
		this.aq = aq;	
		hFunction = new HelperFunctions();
	}

	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			convertView = activity.getLayoutInflater().inflate(view, null);			
		}

		Store store = stores.get(position);

		AQuery aqView = aq.recycle(convertView);
		
		uiUtil = new UiUtil(aqView);
		uiUtil.showData(store.name, R.id.txt_view_name);
		uiUtil.showData(Double.parseDouble(store.price), hFunction.formatPrice(store.price), R.id.txt_view_price);
		
		return convertView;
	}

}
