package com.stratpoint.globe.gdeals.adapters;

import java.util.List;

import com.androidquery.AQuery;
import com.stratpoint.globe.gdeals.R;
import com.stratpoint.globe.gdeals.helpers.DateHelper;
import com.stratpoint.globe.gdeals.models.Payment;
import com.stratpoint.globe.gdeals.models.Voucher;
import com.stratpoint.globe.gdeals.utils.HelperFunctions;
import com.stratpoint.globe.gdeals.utils.UiUtil;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class VoucherAdapter extends ArrayAdapter<Voucher> {

	private Activity activity;
	private int view;
	private AQuery aq;
	private List<Voucher> vouchers;
	private DateHelper dateHelper;
	private HelperFunctions hFunction;
	private String voucherType;
	private UiUtil uiUtil;

	public VoucherAdapter(Activity activity, int view, List<Voucher> vouchers, String voucherType, AQuery aq) {
		super(activity, view, vouchers);
		this.activity = activity;
		this.view = view;
		this.aq = aq;
		this.vouchers = vouchers;
		dateHelper = new DateHelper();
		hFunction = new HelperFunctions();
		this.voucherType = voucherType;
	}
	
	private static class ViewHolder {
		
		private ImageView img_view;
		private TextView txt_view_deal_name;
		private TextView txt_view_deal_exclusive;
		
		private TextView txt_view_deal_validity;
		private TextView txt_view_load_amount_reserved;
		private TextView txt_view_load_balance;
		private TextView txt_view_voucher_code;
		private TextView txt_view_voucher_note;
		
		private TextView txt_view_status_label;
		private TextView txt_view_date_delivered;
		
		private TextView txt_view_date_purchased;
		
		public ViewHolder() {}

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null) {
			convertView = activity.getLayoutInflater().inflate(view, null);
			holder = new ViewHolder();
			holder.img_view = (ImageView) convertView.findViewById(R.id.img_view);
			holder.txt_view_deal_name = (TextView) convertView.findViewById(R.id.txt_view_deal_name);
			holder.txt_view_deal_exclusive = (TextView) convertView.findViewById(R.id.txt_view_deal_exclusive);
			holder.txt_view_deal_validity = (TextView) convertView.findViewById(R.id.txt_view_deal_validity);
			holder.txt_view_load_amount_reserved = (TextView) convertView.findViewById(R.id.txt_view_load_amount_reserved);
			holder.txt_view_load_balance = (TextView) convertView.findViewById(R.id.txt_view_load_balance);
			holder.txt_view_voucher_code = (TextView) convertView.findViewById(R.id.txt_view_voucher_code);
			holder.txt_view_voucher_note = (TextView) convertView.findViewById(R.id.txt_view_voucher_note);
			
			holder.txt_view_status_label = (TextView) convertView.findViewById(R.id.txt_view_status_label);
			holder.txt_view_date_delivered = (TextView) convertView.findViewById(R.id.txt_view_date_delivered);
			
			holder.txt_view_date_purchased = (TextView) convertView.findViewById(R.id.txt_view_date_purchased);
			
//			holder.txt_view_price = (TextView) convertView.findViewById(R.id.txt_view_price);
//			holder.txt_view_discounted_price = (TextView) convertView.findViewById(R.id.txt_view_discounted_price);
//			holder.txt_view_deal_discounted_percent_price = (TextView) convertView.findViewById(R.id.txt_view_deal_discounted_percent_price);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		Voucher voucher = vouchers.get(position);

		AQuery aqView = aq.recycle(convertView);
		
		uiUtil = new UiUtil(aqView);
		
		aqView.id(holder.img_view).image(voucher.getSmallImageUrl(), false, true, 0, 0, null, AQuery.FADE_IN_NETWORK, 0);
		
		aqView.id(holder.txt_view_deal_name).text(voucher.dealName);
		aqView.id(holder.txt_view_deal_exclusive).text(voucher.storeName);

		aqView.id(holder.txt_view_voucher_code).text(voucher.redemptionCode);
		
		aqView.id(R.id.reservered_via_load_container).text(voucher.note, true);
		
		if(!TextUtils.isEmpty(voucher.redemptionStart) && !TextUtils.isEmpty(voucher.redemptionEnd) ){
			aqView.id(holder.txt_view_deal_validity).text(dateHelper.formatDate(voucher.redemptionStart, "MMM dd, yyyy") + " - " + dateHelper.formatDate(voucher.redemptionEnd, "MMM dd, yyyy"));
		}
		
		uiUtil.showData(voucher.datePurchased, dateHelper.formatDate(voucher.datePurchased, "MMM dd, yyyy"), holder.txt_view_date_purchased);
		
//		uiUtil.showData(deal.details.description, TextUtils.isEmpty(deal.details.description) ? "": Html.fromHtml(deal.details.description), R.id.txt_view_deal_descriptions);
		
		
		
		
		uiUtil.showData(voucher.deliveryNote + voucher.merchantHotlineInfo, TextUtils.isEmpty(voucher.deliveryNote) ? voucher.merchantHotlineInfo :voucher.deliveryNote + "\n\n" + voucher.merchantHotlineInfo , holder.txt_view_voucher_note);
		
		
//		aqView.id(holder.txt_view_voucher_note).text(voucher.deliveryNote);
		
		
		
		aqView.id(holder.txt_view_status_label).gone();
		aqView.id(holder.txt_view_date_delivered).gone();
		aqView.id(R.id.delivery_status_devider).gone();
		aqView.id(R.id.validity_container).gone();
		
		if(voucherType.equalsIgnoreCase("for_redemption")){
			aqView.id(R.id.validity_container).visible();
			aqView.id(holder.txt_view_load_amount_reserved).text(hFunction.formatPrice(voucher.paidAmount));
			aqView.id(holder.txt_view_load_balance).text(hFunction.formatPrice(voucher.remainingBalance));
			
			if(voucher.paymentType.equalsIgnoreCase(Payment.TYPE_RESERVE_VIA_LOAD)){
				aqView.id(R.id.reservered_via_load_container).visible();
			}
			
		}else if(voucherType.equalsIgnoreCase("redeemed")){
			
			aqView.id(holder.txt_view_status_label).visible();
			aqView.id(holder.txt_view_date_delivered).visible();
			aqView.id(R.id.delivery_status_devider).visible();
			
			
			aqView.id(holder.txt_view_date_delivered).text(dateHelper.formatDate(voucher.dateRedeemed, "MMM dd, yyyy"));
			
			if(voucherType.equalsIgnoreCase("redeemed")){
				aqView.id(holder.txt_view_status_label).text(activity.getString(R.string.date_redeemed));
			}
			
		}
		
		return convertView;
	}

}
