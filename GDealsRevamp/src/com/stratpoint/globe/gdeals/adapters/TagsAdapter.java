package com.stratpoint.globe.gdeals.adapters;

import java.util.List;

import com.stratpoint.globe.gdeals.R;
import com.stratpoint.globe.gdeals.models.Tag;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

public class TagsAdapter extends ArrayAdapter<Tag> {

	private Activity activity;
	private int view;
	private List<Tag> tags;

	public TagsAdapter(Activity activity, int view, List<Tag> tags) {
		super(activity, view, tags);
		this.activity = activity;
		this.view = view;
		this.tags = tags;
	}

	private static class ViewHolder {
		private CheckBox checkBox;

		public CheckBox getCheckBox() {
			return checkBox;
		}

		public TextView getRowTitle() {
			return rowTitle;
		}
		
		private TextView rowTitle;

		public ViewHolder() {}

		public ViewHolder(TextView rowTitle, CheckBox checkBox) {
			this.checkBox = checkBox;
			this.rowTitle = rowTitle;
		}

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final CheckBox check_box;
		TextView row_title;

		if (convertView == null) {
			convertView = activity.getLayoutInflater().inflate(view, null);
			row_title = (TextView) convertView.findViewById(R.id.row_title);
			check_box = (CheckBox) convertView.findViewById(R.id.check_box);
			convertView.setTag(new ViewHolder(row_title, check_box));
		} else {
			ViewHolder holder = (ViewHolder) convertView.getTag();
			check_box = holder.getCheckBox();
			row_title = holder.getRowTitle();
		}

		Tag tag = tags.get(position);

		row_title.setText(tag.name);

		check_box.setTag(tag);
		check_box.setChecked(tag.isActive);

		check_box.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				CheckBox cb = (CheckBox) v;
				tags.get(position).isActive = cb.isChecked();
			}
		});
		
		convertView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(check_box.isChecked()){
					check_box.setChecked(false);
				}else{
					check_box.setChecked(true);
				}
				
				tags.get(position).isActive = check_box.isChecked();
			}
		});
		
		return convertView;
	}

}
