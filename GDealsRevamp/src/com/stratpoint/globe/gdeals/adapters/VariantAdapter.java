package com.stratpoint.globe.gdeals.adapters;

import java.util.List;

import com.androidquery.AQuery;
import com.stratpoint.globe.gdeals.R;
import com.stratpoint.globe.gdeals.models.VariantItem;

import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class VariantAdapter extends ArrayAdapter<VariantItem> {

	private AQuery aq;
	private Activity activity;
	private int view;

	public VariantAdapter(Activity activity, int view, List<VariantItem> variants, AQuery aq) {
		super(activity, view, variants);
		this.aq = aq;
		this.activity = activity;
		this.view = view;
	}

	@Override
	public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
		return initView(position, cnvtView, prnt);
	}

	@Override
	
	public View getView(int pos, View cnvtView, ViewGroup prnt) {
		return initView(pos, cnvtView, prnt);
	}
	
	private View initView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = activity.getLayoutInflater().inflate(view, null);
		}
		
		AQuery aqView = aq.recycle(convertView);
		
		aqView.id(R.id.txt_view_item).text(getItem(position).name).getTextView().setGravity(Gravity.RIGHT);
		return convertView;
	}
}
