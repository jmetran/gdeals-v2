package com.stratpoint.globe.gdeals.adapters;

import java.util.List;

import com.stratpoint.globe.gdeals.R;
import com.stratpoint.globe.gdeals.models.Tag;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

public class TagsGridAdapter extends ArrayAdapter<Tag> {

	private Activity activity;
	private int view;
	private List<Tag> tags;

	public TagsGridAdapter(Activity activity, int view, List<Tag> tags) {
		super(activity, view, tags);
		this.activity = activity;
		this.view = view;
		this.tags = tags;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final CheckBox check_box;

		if (convertView == null) {
			convertView = activity.getLayoutInflater().inflate(view, null);
			check_box = (CheckBox) convertView.findViewById(R.id.check_box);
		} else {
			check_box = (CheckBox) convertView;
		}

		Tag tag = tags.get(position);

		check_box.setTag(tag);
		check_box.setChecked(tag.isActive);

		check_box.setButtonDrawable(tag.tagBackground(tag.name, activity.getApplicationContext()));
		
		check_box.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				CheckBox cb = (CheckBox) v;
				tags.get(position).isActive = cb.isChecked();
			}
		});
		
		return convertView;
	}

}
