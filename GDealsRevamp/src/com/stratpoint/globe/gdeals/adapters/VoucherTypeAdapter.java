package com.stratpoint.globe.gdeals.adapters;

import java.util.List;

import com.androidquery.AQuery;
import com.stratpoint.globe.gdeals.R;
import com.stratpoint.globe.gdeals.VouchersActivity;
import com.stratpoint.globe.gdeals.models.VoucherType;
import com.stratpoint.globe.gdeals.utils.IntentUtil;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class VoucherTypeAdapter extends ArrayAdapter<VoucherType> {

	private Activity activity;
	private int view;
	private AQuery aq;
	private List<VoucherType> voucherTypes;
	private IntentUtil iUtil;

	public VoucherTypeAdapter(Activity activity, int view, List<VoucherType> voucherTypes, AQuery aq) {
		super(activity, view, voucherTypes);
		this.activity = activity;
		this.view = view;
		this.aq = aq;
		this.voucherTypes = voucherTypes;
		iUtil = new IntentUtil(activity);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = activity.getLayoutInflater().inflate(view, null);			
		}

		final VoucherType voucherType = voucherTypes.get(position);

		AQuery aqView = aq.recycle(convertView);
		
		aqView.id(R.id.txt_view_name).text(voucherType.name).getTextView().setCompoundDrawablesWithIntrinsicBounds(voucherType.voucherIconInt(voucherType.name, activity.getApplicationContext()), 0, 0, 0);;
		aqView.id(R.id.txt_view_count).text(String.valueOf(voucherType.count));
		
		convertView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				iUtil.startIntent(VouchersActivity.class, "voucherType", voucherType);
			}
		});
		
		return convertView;
	}

}
