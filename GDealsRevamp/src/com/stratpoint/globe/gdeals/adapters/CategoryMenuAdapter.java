package com.stratpoint.globe.gdeals.adapters;

import com.androidquery.AQuery;
import com.stratpoint.globe.gdeals.R;
import com.stratpoint.globe.gdeals.models.Category;
import com.stratpoint.globe.gdeals.utils.UiUtil;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;

public class CategoryMenuAdapter extends ArrayAdapter<Category> implements SpinnerAdapter{

	private Activity activity;
	private AQuery aq;
	private int view;
	private UiUtil uiUtil;
	private Drawable icon;
	
	public CategoryMenuAdapter(Activity activity, int view, AQuery aq) {
		super(activity, view);
		
		this.activity = activity;
		this.view = view;
		this.aq = aq;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		if (convertView == null) {
			convertView = activity.getLayoutInflater().inflate(view, null);
		}
		
		Category item = getItem(position);
		
		AQuery aqView = aq.recycle(convertView);
		
		uiUtil = new UiUtil(aqView);
		
		uiUtil.showData(item.name, R.id.row_title);
		aqView.id(R.id.row_icon).gone();
		
		return convertView;
	}
	
	@Override
    public View getDropDownView(int position, View convertView, ViewGroup p_parent) {
            
		if (convertView == null) {
			convertView = activity.getLayoutInflater().inflate(view, null);
		}

		Category item = getItem(position);

		AQuery aqView = aq.recycle(convertView);

		uiUtil = new UiUtil(aqView);
		
		aqView.id(R.id.row_title).getTextView().setTextColor(activity.getResources().getColor(R.color.default_text_color));
		
		uiUtil.showData(item.name, R.id.row_title);

		icon = item.categoryIcon(item.name, activity.getApplicationContext());
		
		if (icon != null) {
			aqView.id(R.id.row_icon).image(icon);
		} else {
			aqView.id(R.id.row_icon).gone();
		}
		
		return convertView;
	}
	
}
