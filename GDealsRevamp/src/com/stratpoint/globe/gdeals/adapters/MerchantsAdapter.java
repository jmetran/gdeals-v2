package com.stratpoint.globe.gdeals.adapters;

import java.util.List;

import com.androidquery.AQuery;
import com.stratpoint.globe.gdeals.R;
import com.stratpoint.globe.gdeals.models.Merchant;
import com.stratpoint.globe.gdeals.utils.UiUtil;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class MerchantsAdapter extends ArrayAdapter<Merchant> {

	private Activity activity;
	private int view;
	private List<Merchant> merchants;
	private AQuery aq;
	private UiUtil uiUtil;

	public MerchantsAdapter(Activity activity, int view, List<Merchant> merchants, AQuery aq) {
		super(activity, view, merchants);
		this.activity = activity;
		this.view = view;
		this.merchants = merchants;
		this.aq = aq;		
	}

	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			convertView = activity.getLayoutInflater().inflate(view, null);			
		}

		Merchant merchant = merchants.get(position);

		AQuery aqView = aq.recycle(convertView);
		
		uiUtil = new UiUtil(aqView);
		uiUtil.showData(merchant.name + " " + merchant.getBranchNameOnly(), R.id.row_title);
		uiUtil.showData(TextUtils.isEmpty(merchant.distance) ? "" : merchant.distance + "km", R.id.row_sub_title);
				
		return convertView;
	}

}
