package com.stratpoint.globe.gdeals.adapters;

import java.util.List;

import com.androidquery.AQuery;
import com.stratpoint.globe.gdeals.R;
import com.stratpoint.globe.gdeals.base.GDeals;
import com.stratpoint.globe.gdeals.helpers.PhoneHelper;
import com.stratpoint.globe.gdeals.models.DeliveryAddress;
import com.stratpoint.globe.gdeals.models.User;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class DeliveryAddressAdapter extends ArrayAdapter<DeliveryAddress> {

	private Activity activity;
	private int view;
	private List<DeliveryAddress> addresses;
	private AQuery aq;
	private String phoneNo;
	private User user;

	public DeliveryAddressAdapter(Activity activity, int view, List<DeliveryAddress> addresses, AQuery aq) {
		super(activity, view, addresses);
		this.activity = activity;
		this.view = view;
		this.addresses = addresses;
		this.aq = aq;
		phoneNo =  new PhoneHelper().displayMobTel(GDeals.mobileNo());
		user = User.find(User.class, "user_id=?", new String[] { GDeals.userId() }, "", "", "1").get(0);
	}

	

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			convertView = activity.getLayoutInflater().inflate(view, null);
		}
		
		DeliveryAddress address = addresses.get(position);
		
		AQuery aqView = aq.recycle(convertView);
		aqView.id(R.id.txt_view_address).text(address.getCompleteAddress());
		aqView.id(R.id.txt_view_name).text(user.getFullName());
		aqView.id(R.id.txt_view_contact_no).text(phoneNo);
		
		if(addresses.get(position).isSelected){
			convertView.setBackgroundColor(activity.getResources().getColor(R.color.parent_bg_color));
		}else{
			convertView.setBackgroundColor(activity.getResources().getColor(R.color.white));
		}
		
		
		
		
		return convertView;
	}

}
