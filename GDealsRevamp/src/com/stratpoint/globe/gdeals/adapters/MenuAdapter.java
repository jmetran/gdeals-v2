package com.stratpoint.globe.gdeals.adapters;

import com.androidquery.AQuery;
import com.stratpoint.globe.gdeals.R;
import com.stratpoint.globe.gdeals.events.CategorySelectedEvent;
import com.stratpoint.globe.gdeals.models.DealMenuChildItem;
import com.stratpoint.globe.gdeals.models.DealMenuItem;

import de.greenrobot.event.EventBus;

import android.app.Activity;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

public class MenuAdapter extends BaseExpandableListAdapter {

	private AQuery aq;

	private final SparseArray<DealMenuItem> groups;
	public LayoutInflater inflater;

	public MenuAdapter(Activity act, SparseArray<DealMenuItem> groups, AQuery aq) {
		this.groups = groups;
		inflater = act.getLayoutInflater();
		this.aq = aq;
	}

	@Override
	  public Object getChild(int groupPosition, int childPosition) {
	    return groups.get(groupPosition).childDealMenu.get(childPosition);
	  }

	  @Override
	  public long getChildId(int groupPosition, int childPosition) {
	    return 0;
	  }

	  @Override
	  public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
	    
	    if (convertView == null) {
		      convertView = inflater.inflate(R.layout.menu_child_row, null);
		    }
		    
		    final DealMenuChildItem item = (DealMenuChildItem) getChild(groupPosition, childPosition);
		    
		    AQuery aqView = aq.recycle(convertView);
			aqView.id(R.id.row_title).text(item.name).getTextView().setCompoundDrawablesWithIntrinsicBounds(item.iconRes, 0, 0, 0);
	    
		    convertView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					EventBus.getDefault().post(new CategorySelectedEvent(item));
				}
			});

	    return convertView;
	  }

	  @Override
	  public int getChildrenCount(int groupPosition) {
	    return groups.get(groupPosition).childDealMenu.size();
	  }

	  @Override
	  public Object getGroup(int groupPosition) {
	    return groups.get(groupPosition);
	  }

	  @Override
	  public int getGroupCount() {
	    return groups.size();
	  }

	  @Override
	  public void onGroupCollapsed(int groupPosition) {
	    super.onGroupCollapsed(groupPosition);
	  }

	  @Override
	  public void onGroupExpanded(int groupPosition) {
	    super.onGroupExpanded(groupPosition);
	  }

	  @Override
	  public long getGroupId(int groupPosition) {
	    return 0;
	  }

	  @Override
	  public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
	    if (convertView == null) {
	      convertView = inflater.inflate(R.layout.menu_row, null);
	    }
	    
	    final DealMenuItem item = (DealMenuItem) getGroup(groupPosition);
	    
	    AQuery aqView = aq.recycle(convertView);
		aqView.id(R.id.row_title).text(item.name).getTextView().setCompoundDrawablesWithIntrinsicBounds(item.iconRes, 0, 0, 0);
	    
	    return convertView;
	  }

	  @Override
	  public boolean hasStableIds() {
	    return false;
	  }

	  @Override
	  public boolean isChildSelectable(int groupPosition, int childPosition) {
	    return true;
	  }


}
