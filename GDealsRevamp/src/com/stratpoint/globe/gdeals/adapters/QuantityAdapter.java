package com.stratpoint.globe.gdeals.adapters;

import com.androidquery.AQuery;
import com.stratpoint.globe.gdeals.R;

import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

/**
 * an adapter used in quantity in payment option page
 * */
public class QuantityAdapter extends ArrayAdapter<String> {

	private AQuery aq;
	private Activity activity;
	private int view;
	private int mMinQty;
	private int mMaxQty;

	public QuantityAdapter(Activity activity, int view, int minQty, int maxQty, AQuery aq) {
		super(activity, view);
		mMinQty = minQty;
		mMaxQty = maxQty;
		this.aq = aq;
		this.activity = activity;
		this.view = view;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getCount() {
		return (mMaxQty - mMinQty) + 1;
	}

	@Override
	public String getItem(int position) {
		return String.valueOf(position += mMinQty);
	}

	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return initView(convertView, position);
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		return initView(convertView, position);
	}

	private View initView(View convertView, int position) {
		if (convertView == null) {
			convertView = activity.getLayoutInflater().inflate(view, null);
		}
		
		AQuery aqView = aq.recycle(convertView);
		aqView.id(R.id.txt_view_item).text(String.valueOf(position += mMinQty)).getTextView().setGravity(Gravity.RIGHT);
		return convertView;		
	}
}
