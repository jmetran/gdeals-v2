package com.stratpoint.globe.gdeals.adapters;

import java.util.List;

import com.androidquery.AQuery;
import com.stratpoint.globe.gdeals.DealDetailsActivity;
import com.stratpoint.globe.gdeals.R;
import com.stratpoint.globe.gdeals.base.Constants;
import com.stratpoint.globe.gdeals.events.ItemSelectedEvent;
import com.stratpoint.globe.gdeals.models.Deal;
import com.stratpoint.globe.gdeals.utils.HelperFunctions;
import com.stratpoint.globe.gdeals.utils.IntentUtil;
import com.stratpoint.globe.gdeals.utils.UiUtil;

import de.greenrobot.event.EventBus;

import android.app.Activity;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class DealsAdapter extends ArrayAdapter<Deal> {

	private Activity activity;
	private int view;
	private AQuery aq;
	private List<Deal> deals;
	private UiUtil uiUtil;
	private HelperFunctions hFunction;
	private IntentUtil iUtil;

	public DealsAdapter(Activity activity, int view, List<Deal> deals, AQuery aq) {
		super(activity, view, deals);
		this.activity = activity;
		this.view = view;
		this.aq = aq;
		this.deals = deals;
		hFunction = new HelperFunctions();
		iUtil = new IntentUtil(activity);
	}

	private static class ViewHolder {
		
		private ImageView img_item;
		private TextView txt_view_header;
		private TextView txt_view_item;
		
		private TextView txt_view_price;
		private TextView txt_view_discounted_price;
		private TextView txt_view_deal_discounted_percent_price;
		
		public ViewHolder() {}

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null) {
			convertView = activity.getLayoutInflater().inflate(view, null);
			holder = new ViewHolder();
			holder.img_item = (ImageView) convertView.findViewById(R.id.img_item);
			holder.txt_view_header = (TextView) convertView.findViewById(R.id.txt_view_header);
			holder.txt_view_item = (TextView) convertView.findViewById(R.id.txt_view_item);
			
			holder.txt_view_price = (TextView) convertView.findViewById(R.id.txt_view_price);
			holder.txt_view_discounted_price = (TextView) convertView.findViewById(R.id.txt_view_discounted_price);
			holder.txt_view_deal_discounted_percent_price = (TextView) convertView.findViewById(R.id.txt_view_deal_discounted_percent_price);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		final Deal deal = deals.get(position);

		AQuery aqView = aq.recycle(convertView);
		
		aqView.id(holder.img_item).image(deal.getLargeImageUrl(), false, true, 0, 0, null, AQuery.FADE_IN_NETWORK, 0);
		
		uiUtil = new UiUtil(aqView);
		//0418
		uiUtil.showData(deal.name, holder.txt_view_header);
		
		uiUtil.showData(deal.details.dealSourceText, holder.txt_view_item);
		
		uiUtil.showData(Double.parseDouble(deal.regularPrice), hFunction.formatPrice(deal.regularPrice), holder.txt_view_price);
		uiUtil.showData(Double.parseDouble(deal.discountedPrice), hFunction.formatPrice(deal.discountedPrice), holder.txt_view_discounted_price);
		
//		String.format(activity.getString(R.string.successfully_saved), activity.getString(R.string.address))
		
		uiUtil.showData(TextUtils.isEmpty(deal.discountPercentage) ? "" : Integer.parseInt(deal.discountPercentage), String.format(activity.getString(R.string.percent_save), deal.discountPercentage), holder.txt_view_deal_discounted_percent_price);
		
		if(deal.source.equalsIgnoreCase(Constants.GLOBE_REWARDS)){
			aqView.id(holder.txt_view_item).text("").visible();
			aqView.id(holder.txt_view_price).text("Min. Globe Rewards").visible();
			aqView.id(holder.txt_view_price).getTextView().setPaintFlags(aqView.id(holder.txt_view_price).getTextView().getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
			aqView.id(holder.txt_view_discounted_price).text(deal.details.dealMinRewardsPoints+ " Points").visible();
		}else{
			aqView.id(holder.txt_view_price).getTextView().setPaintFlags(aqView.id(holder.txt_view_price).getTextView().getPaintFlags()  | Paint.STRIKE_THRU_TEXT_FLAG);
		}
		
		convertView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				iUtil.startIntent(DealDetailsActivity.class, new String[] { "deal", "source_page" }, new Object[] { deal, Constants.RECO_ENGINE_PAGE_LEGEND_LANDING_PAGE });
				EventBus.getDefault().post(new ItemSelectedEvent(deal.name));
			}
		});
		
		return convertView;
	}

}
