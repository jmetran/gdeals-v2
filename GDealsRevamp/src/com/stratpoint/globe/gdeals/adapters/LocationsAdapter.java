package com.stratpoint.globe.gdeals.adapters;

import java.util.ArrayList;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

import com.stratpoint.globe.gdeals.R;
import com.stratpoint.globe.gdeals.models.Location;
import com.stratpoint.globe.gdeals.utils.CustomFont;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class LocationsAdapter extends ArrayAdapter<Location> implements StickyListHeadersAdapter {

	private Activity activity;
	private int view;
	private List<Location> locations;
	private List<String> parentLocation;
	private int idxId = 0;
	Typeface typeface ;

	public LocationsAdapter(Activity activity, int view, List<Location> locations) {
		super(activity, view, locations);
		this.activity = activity;
		this.view = view;
		this.locations = locations;	
		typeface = CustomFont.typeFace(activity.getApplicationContext(), 0);
		parentLocation = new ArrayList<String>();
	}

	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = activity.getLayoutInflater().inflate(view, null);	
            holder.text = (TextView) convertView.findViewById(R.id.row_title);
            convertView.findViewById(R.id.row_sub_title).setVisibility(View.GONE);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        
        Location location = locations.get(position);
        holder.text.setText(location.name);
				
		return convertView;
	}
	
	@Override
    public int getCount() {
        return locations.size();
    }

    @Override
    public Location getItem(int position) {
        return locations.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


	@Override
	public View getHeaderView(int position, View convertView, ViewGroup parent) {
		HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = activity.getLayoutInflater().inflate(view, null);	
            holder.text = (TextView) convertView.findViewById(R.id.row_title);
            holder.text.setTypeface(typeface);
            convertView.findViewById(R.id.row_sub_title).setVisibility(View.GONE);
            convertView.setBackgroundColor(activity.getResources().getColor(R.color.parent_bg_color));
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }
        Location location = locations.get(position);
        holder.text.setText(location.parent);
        return convertView;
	}

	@Override
	public long getHeaderId(int position) {
		Location location = locations.get(position);
        int idx = 0;
        if (!parentLocation.contains(location.parent)){
        	parentLocation.add(location.parent);
        	idx = idxId;
        	idxId++;
        }else{
        	int len = parentLocation.size();
        	for (int i=0; i < len; i++){
        		if (parentLocation.get(i).equals(location.parent)){
        			idx = i;
        			break;
        		}
        	}        	
        }
    	return idx;
		
	}
	
	class HeaderViewHolder {
        TextView text;
    }

    class ViewHolder {
        TextView text;
    }

}
