package com.stratpoint.globe.gdeals;

import com.androidquery.util.AQUtility;
import com.stratpoint.globe.gdeals.base.BaseActivity;
import com.stratpoint.globe.gdeals.models.Deal;
import com.stratpoint.globe.gdeals.models.Payment;
import com.stratpoint.globe.gdeals.utils.HelperFunctions;

import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.net.http.SslError;
import android.os.Bundle;

public class PaymentGatewayAdealsActivity extends BaseActivity {

	private Bundle extras;

	private static final String SUCCESS_URL = "http://www.adeals.ph/globe-deals/approved";
	private static final String CANCEL_URL = "http://www.adeals.ph/globe-deals/disapproved";

	private static final String OTHER_SUCCESS_URL = "https://www.myregalo.com/gdeals/success.asp";
	private static final String OTHER_CANCEL_URL = "https://www.myregalo.com/gdeals/fail.asp";
	
	private ProgressDialog progress;
	private WebView webview;
	
	private HelperFunctions hFunction;
	
	private Deal deal;
	
	private boolean isSuccess = false;
	private boolean isCancelled = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		init();

		setContentView(webview);

		setUpHeader(getString(R.string.back), null, false, false, true, null);

		webview.getSettings().setJavaScriptEnabled(true);
		webview.setInitialScale(1);
		webview.getSettings().setLoadWithOverviewMode(true);
		webview.getSettings().setUseWideViewPort(true);
		webview.getSettings().setSupportZoom(true);

		webview.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progressLoad) {
				try {
					progress.setMessage("Please wait...");
					progress.setCancelable(false);
					progress.show();
					if (progressLoad == 100) {
						progress.dismiss();
					}
				} catch (Exception e) {
				}
			}
			
			@Override
			public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
				new AlertDialog.Builder(PaymentGatewayAdealsActivity.this).setTitle(getString(R.string.app_name)).setMessage(message).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						result.confirm();
					}
				}).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						result.cancel();
					}
				}).create().show();

				return true;
			}
		});

		webview.setWebViewClient(new WebViewClient() {

			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
			}

			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				AQUtility.debug(description);
			}

			@Override
			public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
				handler.proceed();
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				
				AQUtility.debug(url);
				if (url.contains(SUCCESS_URL) || url.contains(OTHER_SUCCESS_URL)) {
					AQUtility.debug("####SUCCESS#####",url);
					
					//Just added this to prevent multiple call. This is to prevent issue from android L
					if(!isSuccess){
						isSuccess = true;
						iUtil.startIntent(GenericThankYouPageActivity.class, new String[] {"paymentType", "deal"}, new Object[] {Payment.TYPE_CREDIT_CARD, deal});
					}					
				} else if (url.contains(CANCEL_URL) || url.contains(OTHER_CANCEL_URL)) {
					if(!isCancelled){
						isCancelled = true;
						finish();
					}					
				}

				view.loadUrl(url);
				return true;
			}
		});

		webview.loadData(extras.getString("payment_html"), "text/html", "UTF-8");;
	}

	private void init() {
		extras = getIntent().getExtras();
		webview = new WebView(this);
		progress = new ProgressDialog(this);
		hFunction = new HelperFunctions();
		deal = (Deal) hFunction.stringToObject(extras.getString("deal"), Deal.class);
		AQUtility.debug("HTML", extras.getString("payment_html"));
	}
	
	@Override
	public void onBackPressed (){
	    if (webview.isFocused() && webview.canGoBack()) {
	    	webview.goBack();  	    	     
	    }else {
			super.onBackPressed();
			finish();
	    }
	}

}
