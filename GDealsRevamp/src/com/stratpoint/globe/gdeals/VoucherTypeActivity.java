package com.stratpoint.globe.gdeals;

import java.util.Arrays;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.stratpoint.globe.gdeals.adapters.VoucherTypeAdapter;
import com.stratpoint.globe.gdeals.base.BaseActivity;
import com.stratpoint.globe.gdeals.base.Constants;
import com.stratpoint.globe.gdeals.base.GDeals;
import com.stratpoint.globe.gdeals.helpers.AqHelper;
import com.stratpoint.globe.gdeals.models.VoucherType;
import com.stratpoint.globe.gdeals.utils.JsonUtil;
import com.stratpoint.globe.gdeals.utils.UiUtil;

import android.view.View;
import android.widget.ListView;
import android.os.Bundle;

public class VoucherTypeActivity extends BaseActivity{
	
	private AqHelper aqHelper;
	private UiUtil uiUtil;
	
	private List<VoucherType> voucherTypes;
	
	private PullToRefreshListView pull_to_refresh_list_view;
	private View loader;
	private View tap_to_refresh;
	
	private VoucherTypeAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pull_to_refresh_view);
		
		setUpHeader(getString(R.string.menu_vouchers), null, false, false, true, null);
		
		init();	
		
		pull_to_refresh_list_view = (PullToRefreshListView) findViewById(R.id.pull_to_refresh_list_view);
		pull_to_refresh_list_view.setVisibility(View.GONE);
		
		pull_to_refresh_list_view.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				refresh(null);
			}
		});
		
		loader = uiUtil.loader();
		tap_to_refresh = uiUtil.tapToRefresh();
		
		tap_to_refresh.setVisibility(View.GONE);
		loader.setVisibility(View.VISIBLE);
		uiUtil.showView(R.id.main_container, loader);
		uiUtil.showView(R.id.main_container, tap_to_refresh);
		getVoucherList(Constants.CACHE_RESET);
			
    }	
	
	private void init(){
		aqHelper = new AqHelper(this);	
		uiUtil = new UiUtil(this, aq);
	}	
	
	private void getVoucherList(long expire){
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.VOUCHERS_URL+"?access_token="+GDeals.appAccessToken(), "getVoucherListCb", expire);  
		aq.ajax(cb);
	}
	
	public void getVoucherListCb(String url, JSONObject jo, AjaxStatus status) throws JSONException{
		AQUtility.debug(jo);
		AQUtility.debug(status.getCode());
		
		if (status.getCode() == 200){			
			Gson gson = new GsonBuilder().create();
			voucherTypes = Arrays.asList(gson.fromJson(JsonUtil.getJSONArray("result", jo).toString(), VoucherType[].class));
			adapter = new VoucherTypeAdapter(this, R.layout.voucher_type_row, voucherTypes, aq);
			pull_to_refresh_list_view.setAdapter(adapter);
			pull_to_refresh_list_view.setVisibility(View.VISIBLE);
		}else{
			getVoucherList(Constants.NO_EXPIRATION);
			errorMsg(status.getMessage());
			loader.setVisibility(View.GONE);
			tap_to_refresh.setVisibility(View.VISIBLE);
			pull_to_refresh_list_view.setVisibility(View.GONE);
			AQUtility.debug("Error", status.getCode() + " " + "Status:" + status.getMessage());
		}
		pull_to_refresh_list_view.onRefreshComplete();
	}

	public void refresh(View v){
		getVoucherList(Constants.CACHE_RESET);
		loader.setVisibility(View.VISIBLE);
		tap_to_refresh.setVisibility(View.GONE);
	}
	
	
}
