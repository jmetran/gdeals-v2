package com.stratpoint.globe.gdeals;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Required;
import com.stratpoint.globe.gdeals.base.BaseActivity;
import com.stratpoint.globe.gdeals.base.Constants;
import com.stratpoint.globe.gdeals.events.SMSEvent;
import com.stratpoint.globe.gdeals.helpers.AqHelper;
import com.stratpoint.globe.gdeals.helpers.MenuHelper;
import com.stratpoint.globe.gdeals.helpers.PhoneHelper;
import com.stratpoint.globe.gdeals.helpers.RegistrationState;
import com.stratpoint.globe.gdeals.helpers.SharedPrefHelper;
import com.stratpoint.globe.gdeals.helpers.SignatureHelper;
import com.stratpoint.globe.gdeals.helpers.UserRegistrationHelper;
import com.stratpoint.globe.gdeals.models.User;
import com.stratpoint.globe.gdeals.utils.JsonUtil;
import com.stratpoint.globe.gdeals.utils.KeyboardUtil;

import de.greenrobot.event.EventBus;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

public class VerificationCodeActivity extends BaseActivity implements ValidationListener{

	@Required(order = 1)
	private EditText edit_txt_verification_code;
	
	private ProgressDialog progress;
	
	private Validator validator;
	
	private AqHelper aqHelper;
	private SharedPrefHelper sharedPrefHelper;
	private KeyboardUtil keyboardUtil;
	
	private User user;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_verification_code);
		
		EventBus.getDefault().register(this);
		
		setUpHeader(getString(R.string.back), null, false, false, true, MobileRegistrationActivity.class);
		
		init();
		setState();
		
		validator.setValidationListener(this);
		
		aq.id(R.id.txt_view_header).text(new PhoneHelper().displayMobTel(sharedPrefHelper.getString(SharedPrefHelper.MOBILE_NO)));
		
		edit_txt_verification_code = aq.id(R.id.edit_txt_verification_code).getEditText();		
		
	}	
	
	private void validateVerificationCode(){
		keyboardUtil.hide();
		if(sharedPrefHelper.getString(SharedPrefHelper.VERIFICATION_CODE).equalsIgnoreCase(edit_txt_verification_code.getText().toString())){			
			checkIfRegistered();			
		}else{
			alert(getString(R.string.verification_invalid_code));
		}
	}
	
	private void checkIfRegistered(){
		progress = new ProgressDialog(this);
		progress.setMessage("Please wait...");
		progress.setCancelable(false);
		progress.show();
		
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.VERIFY_MOBILE_NO_URL+ "?mobile_number="+ sharedPrefHelper.getString(SharedPrefHelper.MOBILE_NO), "checkIfRegisteredCb");  
		aq.progress(progress).ajax(cb);
	}
	
	public void checkIfRegisteredCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		
		if (_status.getCode() == 200){	
			
			if(JsonUtil.getString("status", _jo).equalsIgnoreCase("error")){
				finish();
				iUtil.startIntent(UserRegistrationActivity.class, true);
			}else{
				login();
			}
			
		}else{
			errorMsg(_status.getMessage());
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}		
	}

	private void login(){
		progress = new ProgressDialog(this);
		progress.setMessage("Please wait...");
		progress.setCancelable(false);
		progress.show();
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("mobile_number", sharedPrefHelper.getString(SharedPrefHelper.MOBILE_NO));
	    
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.MOBILE_LOGIN_URL, "loginCb");  
		cb.params(params);
		cb.method(AQuery.METHOD_POST);
		aq.progress(progress).ajax(cb);
		
	}
	
	public void loginCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		
		if (_status.getCode() == 200){	
			
			user = new UserRegistrationHelper(this).parseAndSaveUserDetails(_jo);		
			getOAuthRequestToken();
		}else{
			errorMsg(_status.getMessage());
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}		
	}
	
	//2. GET REQUEST TOKEN
	private void getOAuthRequestToken(){
		progress = new ProgressDialog(this);
		progress.setMessage("Please wait...");
		progress.setCancelable(false);
		progress.show();
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("client_id", Constants.GDEALS_APP_CLIENT_ID);
	    params.put("client_secret", Constants.GDEALS_APP_CLIENT_SECRET);
	    params.put("user_id", user.userId);
	    params.put("response_type", "request_token");
	    params.put("scope", "app.mobile");
	    
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.REQUEST_ACCESS_TOKEN_URL, "getOAuthRequestTokenCb");  
		cb.params(params);
		cb.method(AQuery.METHOD_POST);
		aq.progress(progress).ajax(cb);
	}
	
	public void getOAuthRequestTokenCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		
		if (_status.getCode() == 200){			
			sharedPrefHelper.updateSharedPreferences(SharedPrefHelper.REQUEST_TOKEN, JsonUtil.getString("request_token", JsonUtil.getJSONObject("result", _jo)));			
			getOAuthAccessToken();
		}else{
			errorMsg(_status.getMessage());
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}		
	}
	
	//3. GET ACCESS TOKEN
	private void getOAuthAccessToken(){
		progress = new ProgressDialog(this);
		progress.setMessage("Please wait...");
		progress.setCancelable(false);
		progress.show();
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("client_id", Constants.GDEALS_APP_CLIENT_ID);
	    params.put("client_secret", Constants.GDEALS_APP_CLIENT_SECRET);
	    params.put("grant_type", "authorization_code");
	    params.put("request_token", sharedPrefHelper.getString(SharedPrefHelper.REQUEST_TOKEN));
	    
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.ACCESS_TOKEN_URL, "getOAuthAccessTokenCb");  
		cb.params(params);
		cb.method(AQuery.METHOD_POST);
		aq.progress(progress).ajax(cb);
	}
	
	public void getOAuthAccessTokenCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		
		if (_status.getCode() == 200){		
			AQUtility.debug(JsonUtil.getString("access_token", JsonUtil.getJSONObject("result", _jo)));
			sharedPrefHelper.updateSharedPreferences(SharedPrefHelper.ACCESS_TOKEN, JsonUtil.getString("access_token", JsonUtil.getJSONObject("result", _jo)));			
			sharedPrefHelper.updateSharedPreferences(SharedPrefHelper.REGISTRATION_STATE, RegistrationState.DONE);
			sharedPrefHelper.updateSharedPreferences(SharedPrefHelper.LOGGED_IN, true);
			sharedPrefHelper.updateSharedPreferences(SharedPrefHelper.NEW_ACCOUNT, false);
			iUtil.startIntent(MainActivity.class, true);
		}else{
			errorMsg(_status.getMessage());
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}		
	}
		
	private void init(){
		aqHelper = new AqHelper(this);
		validator = new Validator(this);
		sharedPrefHelper = new SharedPrefHelper(this);
		keyboardUtil = new KeyboardUtil(this);
	}
	
	private void setState(){
		sharedPrefHelper.updateSharedPreferences(SharedPrefHelper.REGISTRATION_STATE, RegistrationState.VERIFICATION_CODE);
	}
	
	public void resendVerification(View v){
		requestVerificationCode();
	}
	
	private void requestVerificationCode(){
		progress = new ProgressDialog(this);
		progress.setMessage("Please wait...");
		progress.setCancelable(false);
		progress.show();
		
		String mobileNumber = new PhoneHelper().formatMobtel(sharedPrefHelper.getString(SharedPrefHelper.MOBILE_NO));
		String verificationCode = sharedPrefHelper.getString(SharedPrefHelper.VERIFICATION_CODE);
		
		Map<String, Object> params = new HashMap<String, Object>();
	    params.put("mobile_number", mobileNumber);
	    params.put("verification_code", verificationCode);
	    params.put("signature", new SignatureHelper().generateSignature(new String[] { mobileNumber, verificationCode }));
	    
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.REQUEST_VERIFICATION_CODE_URL, "requestVerificationCodeCb");  
		cb.params(params);
		cb.method(AQuery.METHOD_POST);
		aq.progress(progress).ajax(cb);
	}
	
	public void requestVerificationCodeCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		
		if (_status.getCode() == 200){			
			info(R.string.verification_resend_code_successful);
		}else{
			errorMsg(_status.getMessage());
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}		
	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.done_action, menu);
	    
	    MenuItem item = menu.findItem(R.id.btn_done);
	    
	    MenuHelper.setMenuItemLabel(item.getActionView(), getString(R.string.verify), R.drawable.img_ic_check, new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				validator.validate();
			}
		});

	    return super.onCreateOptionsMenu(menu);
    }
	
	@Override
	public void onResume ()
	{
	   super.onResume();
	   setState();
	}

	@Override
	public void preValidation() {}

	@Override
	public void onSuccess() {
		validateVerificationCode();
		flurryLogEvent(getString(R.string.verify));
	}
	
	@Override
	public void onFailure(View failedView, Rule<?> failedRule) {
		String message = failedRule.getFailureMessage();
        if (failedView instanceof EditText) {
            failedView.requestFocus();
            ((EditText) failedView).setError(message);
        }
	}

	@Override
	public void onValidationCancelled() {}

	public void onEvent(SMSEvent event) {
		if(!TextUtils.isEmpty(event.code)){
			edit_txt_verification_code.setText(event.code);
			validateVerificationCode();
			flurryLogEvent(getString(R.string.verify));
		}		
	}
	
	@Override
	protected void onDestroy() {
		EventBus.getDefault().unregister(this);
		super.onDestroy();
	}
}
