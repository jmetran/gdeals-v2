package com.stratpoint.globe.gdeals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.LocationAjaxCallback;
import com.androidquery.util.AQUtility;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.stratpoint.globe.gdeals.base.BaseActivity;
import com.stratpoint.globe.gdeals.base.Constants;
import com.stratpoint.globe.gdeals.base.GDeals;
import com.stratpoint.globe.gdeals.fragments.ParticipatingBranchesDialog;
import com.stratpoint.globe.gdeals.helpers.AqHelper;
import com.stratpoint.globe.gdeals.helpers.DateHelper;
import com.stratpoint.globe.gdeals.models.Category;
import com.stratpoint.globe.gdeals.models.Deal;
import com.stratpoint.globe.gdeals.models.Merchant;
import com.stratpoint.globe.gdeals.models.ResultMessage;
import com.stratpoint.globe.gdeals.models.StoreDetails;
import com.stratpoint.globe.gdeals.utils.AlertUtil;
import com.stratpoint.globe.gdeals.utils.HelperFunctions;
import com.stratpoint.globe.gdeals.utils.JsonUtil;
import com.stratpoint.globe.gdeals.utils.RemoveUnnecessaryField;
import com.stratpoint.globe.gdeals.utils.UiUtil;

import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Paint;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

public class DealDetailsActivity extends BaseActivity{
	
	private AqHelper aqHelper;
	private HelperFunctions hFunction;
	private UiUtil uiUtil;
	private DateHelper dateHelper;
	private AlertUtil alertUtil;
	
	private Deal deal;
	private List<Merchant> merchants;
	private List<Deal> recommendedDeals;
	private String sourcePage = Constants.RECO_ENGINE_PAGE_LEGEND_DEAL_DETAILS;
	private StoreDetails storeDetails;
	private ResultMessage result;
	
	private Bundle extras;
	
	private LocationAjaxCallback location;
	protected LocationManager locManager;
	
	private View loader;
	private LinearLayout main_container;
	private LinearLayout carousel_container;
	private ProgressDialog progress;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_deal_details);
		
		setUpHeader(getString(R.string.back), null, false, false, true, null);
		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		
		init();
		
		carousel_container = (LinearLayout) aq.id(R.id.carousel_container).getView();
		
		if(extras.containsKey("source_page")){
			sourcePage = extras.getString("source_page");
		}		
		
		if(extras.containsKey("deal_id")){
			loader = uiUtil.loader();
			
			main_container = (LinearLayout) aq.id(R.id.main_container).getView();
			main_container.addView(loader);
			aq.id(R.id.scroll_view).gone();
			aq.id(R.id.btn_container).gone();
			
			getDealDetails();
		}else{
			deal = (Deal) hFunction.stringToObject(extras.getString("deal"), Deal.class);
			setUpDetails();			
		}
		
    }
	
	private void getDealDetails(){
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.SEARCH_DEAL_DETAILS_URL+ "?deal_id="+extras.getString("deal_id") , "getDealDetailsCb", Constants.CACHE_DEFAULT_EXPIRE);  
		aq.ajax(cb);
	}
	
	public void getDealDetailsCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		
		Gson gson = new GsonBuilder().setExclusionStrategies(new RemoveUnnecessaryField()).create();
		if (_status.getCode() == 200){	
			
			deal = gson.fromJson(JsonUtil.getJSONObject("result", _jo).toString(), Deal.class);
			setUpDetails();
			aq.id(R.id.scroll_view).visible();
			aq.id(R.id.btn_container).visible();
			loader.setVisibility(View.GONE);
			
		}else{
			getDealDetails();
			errorMsg(_status.getMessage());
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}		
	}
	
	private void getRecommendation(){
		
		String category = "";
		
		if(!TextUtils.isEmpty(deal.categoryId)){
			category = "&category_id="+deal.categoryId;
		}
		
		if(!TextUtils.isEmpty(deal.categoryIdNew)){
			category+="&category_id_new="+deal.categoryIdNew;
		}
		
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.DEALS_FEATURED_URL+"?action="+Constants.RECO_ENGINE_ACTION_VIEW+"&deal_id="+deal.id+"&page_legend="+Constants.RECO_ENGINE_PAGE_LEGEND_DEAL_DETAILS+"&source_page="+sourcePage+"&access_token="+GDeals.appAccessToken() + category +"&client_version=1.13.003&strip_html_tags=1" , "getRecommendationCb", Constants.CACHE_DEFAULT_EXPIRE);  
		aq.progress(R.id.reco_progress_bar).ajax(cb);
	}
	
	public void getRecommendationCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		
		Gson gson = new GsonBuilder().setExclusionStrategies(new RemoveUnnecessaryField()).create();
		if (_status.getCode() == 200){	
			recommendedDeals = new ArrayList<Deal>();
			
			if(JsonUtil.getJSONObject("result", _jo) !=null){
				recommendedDeals = Arrays.asList(gson.fromJson(JsonUtil.getJSONArray("deals", JsonUtil.getJSONObject("result", _jo)).toString(), Deal[].class));
			}
			
			for(final Deal deal: recommendedDeals){
				View v = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.recommended_deals, null, false);
				
				ImageView img_item = (ImageView) v.findViewById(R.id.img_item);
				TextView txt_view_header = (TextView) v.findViewById(R.id.txt_view_header);
				TextView txt_view_price = (TextView) v.findViewById(R.id.txt_view_price);
				TextView txt_view_discounted_price = (TextView) v.findViewById(R.id.txt_view_discounted_price);
				
				aq.id(img_item).image(deal.getLargeImageUrl(), false, true, 0, 0, null, AQuery.FADE_IN_NETWORK, 0);
				
				uiUtil.showData(deal.name, txt_view_header);
				uiUtil.showData(Double.parseDouble(deal.regularPrice), hFunction.formatPrice(deal.regularPrice), txt_view_price);
				uiUtil.showData(Double.parseDouble(deal.discountedPrice), hFunction.formatPrice(deal.discountedPrice), txt_view_discounted_price);
				
				if(deal.source.equalsIgnoreCase(Constants.GLOBE_REWARDS)){
					aq.id(txt_view_price).text("Min. Globe Rewards").visible();
					aq.id(txt_view_price).getTextView().setPaintFlags(aq.id(txt_view_price).getTextView().getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
					aq.id(txt_view_discounted_price).text(deal.details.dealMinRewardsPoints+ " Points").visible();
				}else{
					aq.id(txt_view_price).getTextView().setPaintFlags(aq.id(txt_view_price).getTextView().getPaintFlags()  | Paint.STRIKE_THRU_TEXT_FLAG);
				}
				
				v.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						iUtil.startIntent(DealDetailsActivity.class, new String[] { "deal", "source_page" }, new Object[] { deal, Constants.RECO_ENGINE_PAGE_LEGEND_DEAL_DETAILS });						
					}
				});
				
				carousel_container.addView(v);
			}
			
			if(recommendedDeals.size() == 0){
				aq.id(R.id.recommend_layout_holder).gone();
			}
			
		}else{
			getRecommendation();
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}		
	}
	
	
	private void setUpDetails(){
		aq.id(R.id.img_view).image(deal.getLargeImageUrl(), false, true, 0, 0, null, AQuery.FADE_IN_NETWORK, 0);
		
		List<Category>  categories = Category.find(Category.class, "category_id=?", new String[] { deal.categoryIdNew }, "", "", "1");
		
		if(categories.size() != 0){
			aq.id(R.id.img_category_icon).image(categories.get(0).categoryIcon(categories.get(0).name, getApplicationContext()));
		}else{
			aq.id(R.id.img_category_icon).gone();
		}
		
		uiUtil.showData(deal.name, R.id.txt_view_deal_name);
		
		uiUtil.showData(deal.details.dealSourceText, R.id.txt_view_deal_exclusive);
		
		uiUtil.showData(deal.details.purchaseCount, R.id.txt_view_deal_bought);
		
		uiUtil.showData(Double.parseDouble(deal.regularPrice), hFunction.formatPrice(deal.regularPrice), R.id.txt_view_deal_original_price);
		uiUtil.showData(Double.parseDouble(deal.discountedPrice), hFunction.formatPrice(deal.discountedPrice), R.id.txt_view_deal_discounted_price);
		
		uiUtil.showData(TextUtils.isEmpty(deal.discountPercentage) ? "" : Integer.parseInt(deal.discountPercentage), deal.discountPercentage + "%", R.id.txt_view_deal_discounted_percent_price);
		
		if(deal.source.equalsIgnoreCase(Constants.GLOBE_REWARDS)){
			
			aq.id(R.id.container_save).gone();
			aq.id(R.id.container_original).gone();
			
			aq.id(R.id.txt_view_deal_discounted_label).text("Min. Globe Rewards").visible();
			aq.id(R.id.txt_view_deal_discounted_price).text(deal.details.dealMinRewardsPoints+ " Points").visible();
		}else{
			aq.id(R.id.txt_view_deal_original_price).getTextView().setPaintFlags(aq.id(R.id.txt_view_deal_original_price).getTextView().getPaintFlags()  | Paint.STRIKE_THRU_TEXT_FLAG);
		}
		
		uiUtil.showData(deal.dealEndDate, dateHelper.formatDate(deal.dealEndDate, "MMM dd, yyyy"), R.id.txt_view_deal_validity);
		
		uiUtil.showData(deal.details.description, TextUtils.isEmpty(deal.details.description) ? "": Html.fromHtml(deal.details.description), R.id.txt_view_deal_descriptions);
		uiUtil.showData(deal.details.terms, TextUtils.isEmpty(deal.details.terms) ? "": Html.fromHtml(deal.details.terms), R.id.txt_view_deal_terms_and_conditions);
		
		if(TextUtils.isEmpty(deal.details.terms)){
			aq.id(R.id.txt_view_deal_terms_and_conditions).text(getString(R.string.no_terms_and_condition)).visible();
		}
		
		if(!TextUtils.isEmpty(deal.redemptionStart) && !TextUtils.isEmpty(deal.redemptionEnd) ){
			aq.id(R.id.txt_view_deal_redemption_period).text(String.format(getString(R.string.redemption_period), dateHelper.formatDate(deal.redemptionStart, "MMM dd, yyyy"), dateHelper.formatDate(deal.redemptionEnd, "MMM dd, yyyy")));
		}else{
			aq.id(R.id.txt_view_deal_redemption_period).gone();
		}
		
		if(TextUtils.isEmpty(deal.dealEndDate)){
			aq.id(R.id.deal_validity_container).gone();
		}
		
		if(deal.source.equalsIgnoreCase(Constants.OKAYOKAY)){
			aq.id(R.id.progress_bar).gone();			
		}else{
			getLocation();
		}
		
		getRecommendation();
		analytics();
	}
	
	
	private void buyNow(){
		AQUtility.debug("source", deal.source);
		//Clean deals
		deal.details.terms = "";
		deal.details.description = "";
		
		if(Constants.REDIRECT_DEALS.contains(deal.source)){
			iUtil.startIntent(WebViewActivity.class, "url", deal.details.vendorUrl );
		}else if(deal.source.equals(Constants.ADEALS)){
			getStores();
		}else if(deal.source.equals(Constants.ZAP)){
			
		}else if(deal.source.equals(Constants.OKAYOKAY)){
			iUtil.startIntent(CartOkayOkayActivity.class, new String[] {"deal"}, new Object[] { deal } );
		}else if(deal.source.equals(Constants.GLOBE_REWARDS)){
			checkRewardsBalance();
		}
	}
	
	private void analytics(){
		Map<String, Object> params = new HashMap<String, Object>();
	    params.put("access_token", GDeals.appAccessToken());
		params.put("deal_id", deal.id);
	    
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.ANALYTICS_DEAL_VIEWS_URL, "analyticsCb");  
		cb.params(params);
		cb.method(AQuery.METHOD_POST);
		aq.ajax(cb);
	}
	
	public void analyticsCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
	}
	
	private void getStores(){
		progress = new ProgressDialog(this);
		progress.setMessage("Please wait...");
		progress.setCancelable(false);
		progress.show();
			    
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.PARTICIPATING_STORE_URL+ "?access_token=" +GDeals.appAccessToken() + "&deal_id="+deal.id , "getStoresCb", Constants.CACHE_DEFAULT_EXPIRE);  
		aq.progress(progress).ajax(cb);
	}
	
	public void getStoresCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		
		Gson gson = new GsonBuilder().setExclusionStrategies(new RemoveUnnecessaryField()).create();
		if (_status.getCode() == 200){	
			storeDetails = gson.fromJson(JsonUtil.getJSONObject("result", _jo).toString(), StoreDetails.class);
			
			if(storeDetails.stores.size()==1){
				iUtil.startIntent(CartAdealsActivity.class, new String[] { "deal", "store", "storeDetails" }, new Object[] { deal, storeDetails.stores.get(0), storeDetails });
			}else{
				iUtil.startIntent(StoreSelectionActivity.class, new String[] {"deal", "storeDetails"}, new Object[] { deal, storeDetails } );
			}			
			
		}else if(_status.getCode() == 400){	
			result = gson.fromJson(_status.getError(), ResultMessage.class);
			alert(result.message);
		}else{
			errorMsg(_status.getMessage());
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}		
	}
	
	private void checkRewardsBalance(){
		progress = new ProgressDialog(this);
		progress.setMessage("Please wait...");
		progress.setCancelable(false);
		progress.show();
			    
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.GLOBE_REWARDS_URL+ "?access_token=" +GDeals.appAccessToken() , "checkRewardsBalanceCb");  
		aq.progress(progress).ajax(cb);
	}
	
	public void checkRewardsBalanceCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		
		if (_status.getCode() == 200){	
			
			String available_points = JsonUtil.getString("result", _jo);
			AQUtility.debug(deal.details.dealMinRewardsPoints);
			if (Double.parseDouble(deal.details.dealMinRewardsPoints) > Double.parseDouble(available_points)) {
				alertUtil.alertInfo(getString(R.string.rewards_insufficient_notification_title), String.format(getString(R.string.rewards_insufficient_notification_allowed_points), deal.details.dealMinRewardsPoints)  , null);
			}else{
				iUtil.startIntent(RewardInstructionsActivity.class, new String[] {"deal", "available_points"} , new Object[] { deal, available_points });
			}
						
		}else{
			errorMsg(_status.getMessage());
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}		
	}
	
	
	private void init(){
		extras = getIntent().getExtras();
		aqHelper = new AqHelper(this);	
		hFunction = new HelperFunctions();		
		uiUtil = new UiUtil(this, aq);
		dateHelper = new DateHelper();
		locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);		
		merchants = new ArrayList<Merchant>();
		alertUtil = new AlertUtil(this);		
		result = new ResultMessage();
		storeDetails = new StoreDetails();
	}
	
	public void onClick(View v){
		switch (v.getId()) {
		case R.id.btn_share:
			
			break;
		case R.id.btn_show_branches:
			
			Bundle args = new Bundle();
			args.putString("merchants", hFunction.objectToString(merchants));
			
			FragmentManager fr = getSupportFragmentManager();
			ParticipatingBranchesDialog branchesDialog = new ParticipatingBranchesDialog();
			branchesDialog.setRetainInstance(true);
			branchesDialog.setArguments(args);
			branchesDialog.show(fr, "branches");
			
			flurryLogEvent(getString(R.string.see_participating_branches));
			break;
		case R.id.btn_buy:
			buyNow();
			flurryLogEvent(getString(R.string.buy));
			break;
		}
	}
	
	public void getLocation(){
		if (locManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
			location = new LocationAjaxCallback();
			location.weakHandler(this, "locationCb").timeout(40*1000);
			location.async(this);
		}else{
			getBranches(null);
		}
	}
	
	public void locationCb(String url, Location loc, AjaxStatus _status){
		if(loc != null){
			getBranches(loc);		
		}else{			
			getBranches(null);
		}
		location.stop();
	}
	
	private void getBranches(Location loc){
		String url = Constants.PARTICIPATING_MERCHANTS_URL+"?access_token="+GDeals.appAccessToken()+"&deal_id="+deal.id ;
		if(loc != null){
			url +="&latitude="+loc.getLatitude()+"&longitude="+loc.getLongitude();
		}else{
			url +="&latitude="+"&longitude=";
		}			
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(url, "getBranchesCb", Constants.CACHE_DEFAULT_EXPIRE);  
		aq.progress(R.id.progress_bar).ajax(cb);
	}
	
	public void getBranchesCb(String url, JSONObject jo, AjaxStatus status) throws JSONException{
		AQUtility.debug(jo);
		AQUtility.debug(status.getCode());
		
		if (status.getCode() == 200){			
			Gson gson = new GsonBuilder().setExclusionStrategies(new RemoveUnnecessaryField()).create();
			merchants = Arrays.asList(gson.fromJson(JsonUtil.getJSONArray("merchants", JsonUtil.getJSONObject("result", jo)).toString(), Merchant[].class));			
			aq.id(R.id.btn_show_branches).visible();
		}else{
			errorMsg(status.getMessage());
			AQUtility.debug("Error", status.getCode() + " " + "Status:" + status.getMessage());
		}	
	}	
	
}
