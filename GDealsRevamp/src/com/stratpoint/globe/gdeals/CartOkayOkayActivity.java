package com.stratpoint.globe.gdeals;

import java.util.ArrayList;
import java.util.List;

import com.nineoldandroids.view.ViewHelper;
import com.stratpoint.globe.gdeals.R.layout;
import com.stratpoint.globe.gdeals.adapters.VariantAdapter;
import com.stratpoint.globe.gdeals.base.CartBaseActivity;
import com.stratpoint.globe.gdeals.base.Constants;
import com.stratpoint.globe.gdeals.models.Payment;
import com.stratpoint.globe.gdeals.models.Variant;
import com.stratpoint.globe.gdeals.models.VariantItem;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioButton;
import android.widget.Spinner;

public class CartOkayOkayActivity extends CartBaseActivity{
	
	private Spinner spinner_item;
	
	private Variant variant;
	private List<VariantItem> items;
	private VariantItem selectedItem;
	private String itemLabel = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		init();
		
		spinner_item = aq.id(R.id.spinner_item).getSpinner();
		
		aq.id(R.id.rdo_payment_option_via_cod).gone();
		aq.id(R.id.rdo_payment_option_via_load).gone();
		aq.id(R.id.txt_view_deal_exclusive).gone();
		
		RadioButton rdo_payment_option_via_cc = (RadioButton) aq.id(R.id.rdo_payment_option_via_cc).getView();
		rdo_payment_option_via_cc.setText(getText(R.string.payment_option_via_cc_okayokay));
		rdo_payment_option_via_cc.setChecked(true);
		ViewHelper.setAlpha(rdo_payment_option_via_cc , 1);
		
		items = new ArrayList<VariantItem>();
		
		if(variant.colors.size() != 0){
			items = variant.colors;
			itemLabel = Variant.LABEL_COLOR;
		}else if(variant.designs.size() != 0){
			items = variant.designs;
			itemLabel = Variant.LABEL_DESIGN;
		}else if(variant.sizes.size() != 0){
			items = variant.sizes;
			itemLabel = Variant.LABEL_SIZE;
		}else if(variant.paxes.size() != 0){
			items = variant.paxes;
			itemLabel = Variant.LABEL_PAX;
		}else if(variant.hotels.size() != 0){
			items = variant.hotels;
			itemLabel = Variant.LABEL_HOTEL;
		}else if(variant.length.size() != 0){
			items = variant.length;
			itemLabel = Variant.LABEL_LENGTH;
		}else{
			aq.id(R.id.view_linear_variant).gone();
			aq.id(R.id.layout_holder).gone();
			selectedItem = new VariantItem(variant.id, variant.name, variant.qty, variant.price, variant.discountedPrice);	
			updateQty(1, Integer.parseInt(!TextUtils.isEmpty(selectedItem.qty) ? selectedItem.qty : "0"));
		}
		
		aq.id(R.id.txt_view_item).text(itemLabel);
		
		
		if(items.size() != 0){
			spinner_item.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					selectedItem = items.get(position);
					updateQty(1, Integer.parseInt(!TextUtils.isEmpty(selectedItem.qty) ? selectedItem.qty : "0"));
					uiUtil.showData(Double.parseDouble(selectedItem.discountedPrice), "for " + hFunction.formatPrice(selectedItem.discountedPrice), R.id.txt_view_deal_discounted_price);				
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {}
			});
			
			spinner_item.setAdapter(new VariantAdapter(this, layout.text_view, items, aq));
		}
		
		spinner_qty.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {				
				cartItem.qty = position+1;
				totalPrice = (double) Double.parseDouble(selectedItem.discountedPrice) * cartItem.qty;	
				updateTotalPrice();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});
		
	}
	
	private void init(){
		variant = new Variant();
		variant = deal.variant;
		selectedItem = new VariantItem();
		
	}
	
	public void onClick(View v){
		switch (v.getId()) {
		case R.id.btn_continue:
			cartItem.paymentType = Payment.TYPE_CREDIT_CARD;
			cartItem.dealProvider = Constants.OKAYOKAY;
			cartItem.variantItem = selectedItem;
			iUtil.startIntent(DeliveryAddressActivity.class, new String[] {"cartItem", "deal"}, new Object[] {cartItem, deal});
			flurryLogEvent(getString(R.string.btn_continue));
			break;
		}
	}
}
