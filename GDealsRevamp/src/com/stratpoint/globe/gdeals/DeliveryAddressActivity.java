package com.stratpoint.globe.gdeals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import com.orm.query.Select;
import com.stratpoint.globe.gdeals.adapters.DeliveryAddressAdapter;
import com.stratpoint.globe.gdeals.base.BaseActivity;
import com.stratpoint.globe.gdeals.base.Constants;
import com.stratpoint.globe.gdeals.base.GDeals;
import com.stratpoint.globe.gdeals.events.ItemSelectedEvent;
import com.stratpoint.globe.gdeals.helpers.AqHelper;
import com.stratpoint.globe.gdeals.helpers.DateHelper;
import com.stratpoint.globe.gdeals.helpers.MenuHelper;
import com.stratpoint.globe.gdeals.helpers.SignatureHelper;
import com.stratpoint.globe.gdeals.models.CartItem;
import com.stratpoint.globe.gdeals.models.Deal;
import com.stratpoint.globe.gdeals.models.DeliveryAddress;
import com.stratpoint.globe.gdeals.models.Payment;
import com.stratpoint.globe.gdeals.utils.HelperFunctions;
import com.stratpoint.globe.gdeals.utils.JsonUtil;

import de.greenrobot.event.EventBus;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.app.ProgressDialog;
import android.os.Bundle;

public class DeliveryAddressActivity extends BaseActivity{

	private AqHelper aqHelper;
	private HelperFunctions hFunction;
	
	private Bundle extras;
	
	private List<DeliveryAddress> addresses;
	private CartItem cartItem;
	private Deal deal;
	
	private DeliveryAddressAdapter adapter;
	private ListView list_view;
	private ProgressDialog progress;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_delivery_address);
		
		setUpHeader(getString(R.string.select_delivery_address), null, false, false, true, null);
		
		init();	
		
		EventBus.getDefault().register(this);
		
		list_view = aq.id(R.id.list_view).getListView();
		
		list_view.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				for (int i=0; i < addresses.size(); i++){
					adapter.getItem(i).isSelected = false;					
				}
				adapter.getItem(position).isSelected = true;
				adapter.notifyDataSetChanged();
				
			}
		});
		
		getDeliveryAddresses();
		
    }
	
	private DeliveryAddress getSelectedAddress(){
		for (int i=0; i < addresses.size(); i++){
			if (adapter.getItem(i).isSelected){
				return adapter.getItem(i);
			}			
		}
		return null;
	}
	
	
	private void getDeliveryAddresses(){
		addresses = new ArrayList<DeliveryAddress>();

		List<DeliveryAddress> tmpAddresses = Select.from(DeliveryAddress.class).orderBy("is_default DESC").list();
		
		for(DeliveryAddress address: tmpAddresses){
			if(address.isDefault.equalsIgnoreCase("1")){
				address.isSelected = true;
			}
			addresses.add(address);
		}
		
		if(addresses.size() == 0){
			aq.id(R.id.list_view).gone();
			aq.id(R.id.empty_layout_holder).visible();
		}else{
			aq.id(R.id.empty_layout_holder).gone();
			aq.id(R.id.list_view).visible();
		}
		
		adapter = new DeliveryAddressAdapter(this, R.layout.address_row, addresses, aq);
		list_view.setAdapter(adapter);
	}
	
	private void init(){
		extras = getIntent().getExtras();
		aqHelper = new AqHelper(this);	
		hFunction = new HelperFunctions();
		addresses = new ArrayList<DeliveryAddress>();
		cartItem = (CartItem) hFunction.stringToObject(extras.getString("cartItem"), CartItem.class);
		
		if(extras !=null && extras.containsKey("deal")){
			deal = (Deal) hFunction.stringToObject(extras.getString("deal"), Deal.class);
		}
	}
	
	public void onEvent(ItemSelectedEvent event) {
		if(event.msg !=null){
			info(event.msg);
			getDeliveryAddresses();
		}
    }
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.menu.done_action, menu);
	    
	    MenuItem item = menu.findItem(R.id.btn_done);
	    MenuHelper.setMenuItemLabel(item.getActionView(), getString(R.string.add_new), R.drawable.img_ic_add, new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				iUtil.startIntent(DeliveryAddressFormsActivity.class);
				flurryLogEvent(getString(R.string.add_new));
			}
		});
	    return super.onCreateOptionsMenu(menu);
    }
	
	public void onClick(View v){
		switch (v.getId()) {
		case R.id.img_view:
			iUtil.startIntent(DeliveryAddressFormsActivity.class);
			flurryLogEvent(getString(R.string.add_new));
			break;
		case R.id.btn_checkout:			
			if(addresses.size() == 0){
				alert(getString(R.string.delivery_address_empty_message));
			}else{
				
				if(cartItem.dealProvider.equals(Constants.ADEALS)){
					if(cartItem.paymentType.equals(Payment.TYPE_COD)){
						paymentViaCodForAdeals();
					}
				}else if(cartItem.dealProvider.equals(Constants.OKAYOKAY)){
					if(cartItem.paymentType.equals(Payment.TYPE_CREDIT_CARD)){
						paymentViaCreditCardForOkayOkay();
					}
				}
				flurryLogEvent(getString(R.string.checkout));
			}
			break;
		}
	}
	
	//OKAYOKAY PAYMENT VIA CREDIT CARD START
	private void paymentViaCreditCardForOkayOkay(){
		progress = new ProgressDialog(this);
		progress.setMessage("Please wait...");
		progress.setCancelable(false);
		progress.show();
		
		String accessToken = GDeals.appAccessToken();
		String dealId = cartItem.deal.id;
		String deliveryAddressId = getSelectedAddress().addressId;
		String paymentType = cartItem.paymentType;
		String discountedPrice = cartItem.variantItem.discountedPrice;
		String qty = String.valueOf(cartItem.qty);
		String thirdPartyDealId = cartItem.variantItem.id;
		
		Map<String, Object> params = new HashMap<String, Object>();
	    params.put("access_token", accessToken);
		params.put("deal_id", dealId);
	    params.put("delivery_address_id", deliveryAddressId);
	    params.put("payment_type", paymentType);
	    params.put("price", discountedPrice);
	    params.put("quantity", qty);
	    params.put("third_party_deal_id", thirdPartyDealId);
	    params.put("signature", new SignatureHelper().generateSignature(new String[] { accessToken, dealId, deliveryAddressId, paymentType, discountedPrice, qty, thirdPartyDealId }));
	    
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.PAYMENT_OKAYOKAY_URL, "paymentViaCreditCardForOkayOkayCb");  
		cb.params(params);
		cb.method(AQuery.METHOD_POST);
		aq.progress(progress).ajax(cb);
	}
	
	public void paymentViaCreditCardForOkayOkayCb(String url, JSONObject jo, AjaxStatus status) throws JSONException{
		AQUtility.debug(jo);
		AQUtility.debug(status.getCode());
		
		if (status.getCode() == 200){
			String html = JsonUtil.getString("checkout_html", JsonUtil.getJSONObject("result", jo));
			iUtil.startIntent(PaymentGatewayOkayOkayActivity.class, new String[] { "payment_html", "deal" }, new Object[] { html, deal });			
		}else{
			errorMsg(status.getMessage());
			AQUtility.debug("Error", status.getCode() + " " + "Status:" + status.getMessage());
		}	
	}	
	//END
	
	//ADEALS PAYMENT VIA COD START
	private void paymentViaCodForAdeals(){
		progress = new ProgressDialog(this);
		progress.setMessage("Please wait...");
		progress.setCancelable(false);
		progress.show();
		
		String accessToken = GDeals.appAccessToken();
		String dealId = cartItem.deal.id;
		String deliveryAddressId = getSelectedAddress().addressId;
		String paymentType = Payment.TYPE_COD;
		String qty = String.valueOf(cartItem.qty);
		String storeId = cartItem.store.id;
		String timeStamp = new DateHelper().getCurrentDate("yyyyMMddhhmmssss");
		
		Map<String, Object> params = new HashMap<String, Object>();
	    params.put("access_token", accessToken);
		params.put("deal_id", dealId);
	    params.put("delivery_address_id", deliveryAddressId);
	    params.put("payment_type", paymentType);
	    params.put("quantity", qty);
	    params.put("store_id", storeId);
	    params.put("ts", timeStamp);
	    params.put("signature", new SignatureHelper().generateSignature(new String[] { accessToken, dealId, deliveryAddressId, paymentType, qty, storeId, timeStamp }));
	    
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.PAYMENT_ADEALS_URL, "paymentViaCodForAdealsCb");  
		cb.params(params);
		cb.method(AQuery.METHOD_POST);
		aq.progress(progress).ajax(cb);
	}
	
	public void paymentViaCodForAdealsCb(String url, JSONObject jo, AjaxStatus status) throws JSONException{
		AQUtility.debug(jo);
		AQUtility.debug(status.getCode());
		
		if (status.getCode() == 200){
			iUtil.startIntent(GenericThankYouPageActivity.class, new String[] { "paymentType", "deal" }, new Object[] { Payment.TYPE_COD, deal });
		}else{
			errorMsg(status.getMessage());
			AQUtility.debug("Error", status.getCode() + " " + "Status:" + status.getMessage());
		}	
	}
	//END
	
	@Override
	protected void onDestroy() {
		EventBus.getDefault().unregister(this);
		super.onDestroy();
	}
	
	/*
	private void deleteAddress(DeliveryAddress address){
		deletedAddress = address;
		
		Map<String, Object> params = new HashMap<String, Object>();
	    params.put("access_token", GDeals.appAccessToken());
		params.put("delivery_address_id", address.addressId);
	    
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.DELIVERY_ADDRESS_URL, "deleteAddressCb");  
		cb.params(params);
		cb.method(AQuery.METHOD_DELETE);
		aq.ajax(cb);
	}
	
	public void deleteAddressCb(String url, JSONObject jo, AjaxStatus status) throws JSONException{
		AQUtility.debug(jo);
		AQUtility.debug(status.getCode());
		
		if (status.getCode() == 200){
			List<DeliveryAddress>  foundDeliveryAddresses = DeliveryAddress.find(DeliveryAddress.class, "address_id=?", new String[] { deletedAddress.addressId }, "", "", null);
			for(DeliveryAddress address: foundDeliveryAddresses){
				address.delete();
			}
		}else{
			errorMsg(status.getMessage());
			AQUtility.debug("Error", status.getCode() + " " + "Status:" + status.getMessage());
		}	
	}*/
	
}
