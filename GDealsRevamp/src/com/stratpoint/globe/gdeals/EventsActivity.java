package com.stratpoint.globe.gdeals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.nhaarman.listviewanimations.swinginadapters.prepared.SwingBottomInAnimationAdapter;
import com.stratpoint.globe.gdeals.adapters.DealsAdapter;
import com.stratpoint.globe.gdeals.base.BaseActivity;
import com.stratpoint.globe.gdeals.base.Constants;
import com.stratpoint.globe.gdeals.base.GDeals;
import com.stratpoint.globe.gdeals.helpers.AqHelper;
import com.stratpoint.globe.gdeals.models.Deal;
import com.stratpoint.globe.gdeals.utils.JsonUtil;
import com.stratpoint.globe.gdeals.utils.UiUtil;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.content.Context;
import android.os.Bundle;

public class EventsActivity extends BaseActivity{

	private DealsAdapter adapter;
	
	private PullToRefreshListView pull_to_refresh_list_view;
	private ListView list_view;
	private SwingBottomInAnimationAdapter swingBottomInAnimationAdapter;
	private View loader;
	private View tap_to_refresh;
	private View empty_view;
	private View loading_view;
	
	private AqHelper aqHelper;
	private UiUtil uiUtil;
	private ProgressBar progress_bar;
	
	private int page = 0;
	private List<Deal> deals;
	private boolean isLoading = false;
	private boolean isLastPage = false;
	
	private Bundle extras;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
						
		init();	
		
		setUpHeader(getString(R.string.back), null, false, false, true, null);
		
		pull_to_refresh_list_view.setVisibility(View.GONE);
		pull_to_refresh_list_view.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				refresh(null);
			}
		});
		
		pull_to_refresh_list_view.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

			@Override
			public void onLastItemVisible() {
				if(!isLoading && !isLastPage){
					getDeals();		
				}							
			}
		});
		
		getDeals();			
		
		loader = uiUtil.loader();
		tap_to_refresh = uiUtil.tapToRefresh();
		empty_view = uiUtil.empty();
		empty_view.setVisibility(View.GONE);
		tap_to_refresh.setVisibility(View.GONE);
		loader.setVisibility(View.VISIBLE);
		uiUtil.showView(R.id.main_container, empty_view);
		uiUtil.showView(R.id.main_container, loader);
		uiUtil.showView(R.id.main_container, tap_to_refresh);
		
    }
	
	public void refresh(View v){
		refreshDeals();
		loader.setVisibility(View.VISIBLE);
		tap_to_refresh.setVisibility(View.GONE);
	}
	
	private void refreshDeals(){
		loading_view.setVisibility(View.VISIBLE);
		resetDealsData();
		getDeals();
	}
	
	private void resetDealsData(){
		page = 0;
		deals = new ArrayList<Deal>();
	}	
	
	private void init(){
		extras = getIntent().getExtras();
		aqHelper = new AqHelper(this);
		pull_to_refresh_list_view = (PullToRefreshListView) findViewById(R.id.list_view);
		
		list_view = pull_to_refresh_list_view.getRefreshableView();
		
		uiUtil = new UiUtil(this, aq);
		deals = new ArrayList<Deal>();
		
		adapter = new DealsAdapter(this, R.layout.deals_row, deals, aq);	
		swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(adapter);
		swingBottomInAnimationAdapter.setInitialDelayMillis(300);
		swingBottomInAnimationAdapter.setAbsListView(list_view);
		
		loading_view = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.loader, null, false);
		progress_bar = (ProgressBar) loading_view.findViewById(R.id.progress_bar);
		
		list_view.addFooterView(loading_view, null, false);
		list_view.setAdapter(swingBottomInAnimationAdapter);		
	}
	
	private void getDeals(){
		isLastPage = false;
		progress_bar.setVisibility(View.VISIBLE);
		
		String url = Constants.EVENT_DEALS_URL+"?page="+page+"&access_token="+GDeals.appAccessToken()+"&client_version=1.13.003&strip_html_tags=1&flag="+ extras.getString("eventType");
		
		isLoading = true;
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(url, "getDealsCb", Constants.CACHE_DEFAULT_EXPIRE);  
		aq.ajax(cb);
	}
	
	public void getDealsCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		
		if (_status.getCode() == 200){	
			if(page == 0){
				adapter.clear();
			}			
			
			Gson gson = new GsonBuilder().create();
			JSONArray arrEmpty = JsonUtil.getJSONArray("result", _jo);
			
			if(arrEmpty == null){
				
				deals = Arrays.asList(gson.fromJson(JsonUtil.getJSONArray("deals", JsonUtil.getJSONObject("result", _jo)).toString(), Deal[].class));
				
				for(Deal deal: deals){
					adapter.add(deal);
				}
				
				adapter.notifyDataSetChanged();
				
				if(page == 0 && deals.size() < 10){
					AQUtility.debug("last page");
					isLastPage = true;
					progress_bar.setVisibility(View.GONE);
					loading_view.setVisibility(View.GONE);
				}
				
			}else{
				isLastPage = true;
				progress_bar.setVisibility(View.GONE);
				loader.setVisibility(View.GONE);
				AQUtility.debug(arrEmpty.toString());
				AQUtility.debug("last page");
			}
			page++;
			pull_to_refresh_list_view.setVisibility(View.VISIBLE);
			
			if(adapter.isEmpty()){
				pull_to_refresh_list_view.setVisibility(View.GONE);
				empty_view.setVisibility(View.VISIBLE);
			}
		}else{
			errorMsg(_status.getMessage());
			
			if(page == 0){
				loader.setVisibility(View.GONE);
				tap_to_refresh.setVisibility(View.VISIBLE);
				pull_to_refresh_list_view.setVisibility(View.GONE);
			}
			
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}	
		pull_to_refresh_list_view.onRefreshComplete();
		isLoading = false;		
	}	

}
