package com.stratpoint.globe.gdeals;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.facebook.Session;
import com.facebook.SessionState;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.nhaarman.listviewanimations.swinginadapters.prepared.SwingBottomInAnimationAdapter;
import com.stratpoint.globe.gdeals.adapters.DealsAdapter;
import com.stratpoint.globe.gdeals.base.Constants;
import com.stratpoint.globe.gdeals.base.GDeals;
import com.stratpoint.globe.gdeals.events.CategorySelectedEvent;
import com.stratpoint.globe.gdeals.events.ItemSelectedEvent;
import com.stratpoint.globe.gdeals.events.SearchDealsEvent;
import com.stratpoint.globe.gdeals.helpers.AqHelper;
import com.stratpoint.globe.gdeals.helpers.SharedPrefHelper;
import com.stratpoint.globe.gdeals.models.Banner;
import com.stratpoint.globe.gdeals.models.Deal;
import com.stratpoint.globe.gdeals.models.DealMenuChildItem;
import com.stratpoint.globe.gdeals.models.DeliveryAddress;
import com.stratpoint.globe.gdeals.models.Location;
import com.stratpoint.globe.gdeals.models.Banner.BannerItem;
import com.stratpoint.globe.gdeals.models.User;
import com.stratpoint.globe.gdeals.push.GcmRegistration;
import com.stratpoint.globe.gdeals.utils.AlertUtil;
import com.stratpoint.globe.gdeals.utils.HelperFunctions;
import com.stratpoint.globe.gdeals.utils.JsonUtil;
import com.stratpoint.globe.gdeals.utils.RemoveUnnecessaryField;
import com.stratpoint.globe.gdeals.utils.UiUtil;

import de.greenrobot.event.EventBus;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends GcmRegistration  implements BaseSliderView.OnSliderClickListener{
	
	private DealsAdapter adapter;
	
	private AqHelper aqHelper;
	private UiUtil uiUtil;
	private SharedPrefHelper sharedPrefHelper;
	private AlertUtil alertUtil;
	private HelperFunctions hFunction;
	
	private PullToRefreshListView pull_to_refresh_list_view;
	private SwingBottomInAnimationAdapter swingBottomInAnimationAdapter;
	private ListView list_view;
	private ProgressBar progress_bar;
	private View loader;
	private View tap_to_refresh;
	private View empty_view;
	private View loading_view;
	private View banner_view;
	private SlidingMenu menu;
	private SliderLayout slider_layout;
	
	private List<Deal> deals;
	private String categoryId = "";
	
	private int page = 0;
	
	private boolean isLoading = false;
	private boolean isLastPage = false;
	private boolean isSearchActive;
	
	FragmentManager fm;
	Fragment searchFragment;
	FragmentTransaction ft;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		
		registerPushNotification();
				
		init();	

		if(getIntent().getExtras() != null && getIntent().getExtras().containsKey("deal_id")){
			iUtil.startIntent(DealDetailsActivity.class, new String[] { "deal_id", "source_page" }, new Object[] { getIntent().getExtras().getString("deal_id"), getIntent().getExtras().getString("source_page") });			
		}
		
		EventBus.getDefault().register(this);
		
		setUpHeader(getString(R.string.app_name), null, true, true, false, null);
		
		menu.setMode(SlidingMenu.LEFT);
		menu.setFadeDegree(0.35f);
		menu.setShadowWidthRes(R.dimen.shadow_width);
		menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		menu.setSelectorEnabled(true);
		menu.setSelectorDrawable(R.drawable.img_ic_list);
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		menu.attachToActivity(this, SlidingMenu.SLIDING_WINDOW);
		menu.setMenu(R.layout.left_menu);
		
		pull_to_refresh_list_view.setVisibility(View.GONE);
		pull_to_refresh_list_view.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				refresh(null);
			}
		});
		
		pull_to_refresh_list_view.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

			@Override
			public void onLastItemVisible() {
				if(!isLoading && !isLastPage){
					getDeals();		
				}							
			}
		});
		
		User user = User.find(User.class, "user_id=?", new String[] { sharedPrefHelper.getString(SharedPrefHelper.USER_ID) }, "", "", "1").get(0);
		
		if(TextUtils.isEmpty(user.facebookId)){
			getDeals();	
		}else{
			AQUtility.debug("FB_ID",user.facebookId);
			fbLogin();
		}
		
		if(!sharedPrefHelper.getBoolean(SharedPrefHelper.ADDRESS_SYNC)){
			syncAddress();
		}
		
		syncLocation();
		
		loader = uiUtil.loader();
		tap_to_refresh = uiUtil.tapToRefresh();
		empty_view = uiUtil.empty();
		empty_view.setVisibility(View.GONE);
		tap_to_refresh.setVisibility(View.GONE);
		loader.setVisibility(View.VISIBLE);
		uiUtil.showView(R.id.main_container, empty_view);
		uiUtil.showView(R.id.main_container, loader);
		uiUtil.showView(R.id.main_container, tap_to_refresh);
		
		slider_layout.setVisibility(View.GONE);
		getBanners();
    }
	
	private void getBanners(){
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.BANNER_URL, "getBannersCb", Constants.CACHE_DEFAULT_EXPIRE);  
		aq.ajax(cb);
	}
	
	public void getBannersCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		
		if (_status.getCode() == 200){	
			Gson gson = new GsonBuilder().create();
			
			Banner banner = gson.fromJson(JsonUtil.getJSONObject("result", _jo).toString(), Banner.class);
			AQUtility.debug(banner.banners.size());
			for(BannerItem item: banner.banners){
				DefaultSliderView defaultSliderView = new DefaultSliderView(this);
	        	defaultSliderView.image(item.image);
	        	defaultSliderView.getBundle().putString("name",item.name);
	        	defaultSliderView.getBundle().putString("eventItem", "yes");
	        	defaultSliderView.setOnSliderClickListener(this);
	        	defaultSliderView.setScaleType(BaseSliderView.ScaleType.CenterCrop);
	           slider_layout.addSlider(defaultSliderView);
			}
			
			for(Deal item: banner.deals){
				DefaultSliderView defaultSliderView = new DefaultSliderView(this);
	        	defaultSliderView.image(item.imageCarousel);
	        	defaultSliderView.getBundle().putString("name",item.name);
	        	defaultSliderView.getBundle().putString("deal", hFunction.objectToString(item));
	        	defaultSliderView.setOnSliderClickListener(this);
	        	defaultSliderView.setScaleType(BaseSliderView.ScaleType.Fit);
	           slider_layout.addSlider(defaultSliderView);
			}
	        
	        slider_layout.setPresetTransformer(SliderLayout.Transformer.Default);
	        slider_layout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
	        slider_layout.setDuration(4000);
	        slider_layout.setVisibility(View.VISIBLE);
			
		}else{			
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}			
	}
	
	
	@Override
    public void onSliderClick(BaseSliderView slider) {
        if(slider.getBundle().containsKey("eventItem")){
        	iUtil.startIntent(EventsActivity.class, "eventType", slider.getBundle().get("name"));
        }else{
        	iUtil.startIntent(DealDetailsActivity.class, "deal", slider.getBundle().get("deal"));
        }        
    }
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.search_action, menu);
	    return true;
    }
	
	public void refresh(View v){
		refreshDeals();
		loader.setVisibility(View.VISIBLE);
		tap_to_refresh.setVisibility(View.GONE);
	}
	
	private void refreshDeals(){
		loading_view.setVisibility(View.VISIBLE);
		resetDealsData();
		getDeals();
	}
	
	private void resetDealsData(){
		page = 0;
		deals = new ArrayList<Deal>();
	}	
	
	private void init(){
		hFunction = new HelperFunctions();
		menu = new SlidingMenu(this);
		aqHelper = new AqHelper(this);
		pull_to_refresh_list_view = (PullToRefreshListView) findViewById(R.id.list_view);
		
		list_view = pull_to_refresh_list_view.getRefreshableView();
		
		uiUtil = new UiUtil(this, aq);
		deals = new ArrayList<Deal>();
		
		adapter = new DealsAdapter(this, R.layout.deals_row, deals, aq);	
		swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(adapter);
		swingBottomInAnimationAdapter.setInitialDelayMillis(300);
		swingBottomInAnimationAdapter.setAbsListView(list_view);
		
		loading_view = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.loader, null, false);
		progress_bar = (ProgressBar) loading_view.findViewById(R.id.progress_bar);
		
		list_view.addFooterView(loading_view, null, false);
		
		banner_view = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.banner, null, false);
		
		slider_layout = (SliderLayout) banner_view.findViewById(R.id.slider);
		
		list_view.addHeaderView(banner_view, null, false);
		
		list_view.setAdapter(swingBottomInAnimationAdapter);
		
		sharedPrefHelper = new SharedPrefHelper(this);
		
		alertUtil = new AlertUtil(this);
	}
	
	private void getDeals(){
		isLastPage = false;
		progress_bar.setVisibility(View.VISIBLE);
		
		String url = Constants.DEALS_FEATURED_URL+"?page="+page+"&action="+Constants.RECO_ENGINE_ACTION_VIEW+"&page_legend="+Constants.RECO_ENGINE_PAGE_LEGEND_LANDING_PAGE+"&source_page="+Constants.RECO_ENGINE_PAGE_LEGEND_LANDING_PAGE+"&access_token="+GDeals.appAccessToken()+"&client_version=1.13.003&strip_html_tags=1";
		
		isLoading = true;
		if(!TextUtils.isEmpty(categoryId)){
			url+="&category_id_new="+ categoryId;
		}
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(url, "getDealsCb", Constants.CACHE_DEFAULT_EXPIRE);  
		aq.ajax(cb);
	}
	
	public void getDealsCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		
		if (_status.getCode() == 200){	
			if(page == 0){
				adapter.clear();
			}
			
			Gson gson = new GsonBuilder().create();
			JSONArray arrEmpty = JsonUtil.getJSONArray("result", _jo);
			
			if(arrEmpty == null){
				
				deals = Arrays.asList(gson.fromJson(JsonUtil.getJSONArray("deals", JsonUtil.getJSONObject("result", _jo)).toString(), Deal[].class));
				
				for(Deal deal: deals){
					adapter.add(deal);
				}
				
				adapter.notifyDataSetChanged();
				
				if(page == 0 && deals.size() < 10){
					AQUtility.debug("last page");
					isLastPage = true;
					progress_bar.setVisibility(View.GONE);
					loading_view.setVisibility(View.GONE);
				}
				
			}else{
				isLastPage = true;
				progress_bar.setVisibility(View.GONE);
				loader.setVisibility(View.GONE);
				AQUtility.debug(arrEmpty.toString());
				AQUtility.debug("last page");
			}
			page++;
			pull_to_refresh_list_view.setVisibility(View.VISIBLE);
			
			if(adapter.isEmpty()){
				pull_to_refresh_list_view.setVisibility(View.GONE);
				empty_view.setVisibility(View.VISIBLE);
			}
		}else{
			errorMsg(_status.getMessage());
			
			if(page == 0){
				loader.setVisibility(View.GONE);
				tap_to_refresh.setVisibility(View.VISIBLE);
				pull_to_refresh_list_view.setVisibility(View.GONE);
			}
			
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}	
		pull_to_refresh_list_view.onRefreshComplete();
		isLoading = false;
		slider_layout.startAutoCycle();
	}
	
	public void searchDealsCb(String _url, JSONObject _jo, AjaxStatus _status) throws JSONException{
		AQUtility.debug(_jo);
		AQUtility.debug(_status.getCode());
		List<Deal> searchDeals = new ArrayList<Deal>();
		String msg = "";
		if (_status.getCode() == 200){	
			Gson gson = new GsonBuilder().create();
			searchDeals = Arrays.asList(gson.fromJson(JsonUtil.getJSONArray("deals", JsonUtil.getJSONObject("result", _jo)).toString(), Deal[].class));			
		}else{
			errorMsg(_status.getMessage());
			msg = _status.getMessage();
			AQUtility.debug("Error", _status.getCode() + " " + "Status:" + _status.getMessage());
		}	
		
		EventBus.getDefault().post(new SearchDealsEvent(searchDeals, msg));
	}
	
	
	/*
	 * Sync Address
	 */
	private void syncAddress(){
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.DELIVERY_ADDRESS_URL + "?access_token="+GDeals.appAccessToken()+"&user_id="+GDeals.userId(), "syncAddressCb");  
		aq.ajax(cb);		
	}
	
	public void syncAddressCb(String url, JSONObject jo, AjaxStatus status) throws JSONException{
		AQUtility.debug(jo);
		AQUtility.debug(status.getCode());
		
		if (status.getCode() == 200){
			
			Gson gson = new GsonBuilder().setExclusionStrategies(new RemoveUnnecessaryField()).create();
			List<DeliveryAddress> addresses = new ArrayList<DeliveryAddress>();
			addresses = Arrays.asList(gson.fromJson(JsonUtil.getJSONArray("result", jo).toString(), DeliveryAddress[].class));
			
			for(DeliveryAddress address: addresses){
				
				List<DeliveryAddress>  foundAddresses = DeliveryAddress.find(DeliveryAddress.class, "address_id=?", new String[] { address.addressId }, "", "", "1");
				
				if(foundAddresses.size() == 0){
					address.save();
				}else{
					DeliveryAddress updateAddress = foundAddresses.get(0);
					updateAddress = address.updateDeliveryAddress(updateAddress.getId(), address);
					updateAddress.save();
				}
			}
			sharedPrefHelper.updateSharedPreferences(SharedPrefHelper.ADDRESS_SYNC, true);
			
		}else{
			AQUtility.debug("Error", status.getCode() + " " + "Status:" + status.getMessage());
		}	
	}
	
	/*
	 * Sync Location
	 */
	private void syncLocation(){
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.LOCATIONS_URL + "?access_token="+GDeals.appAccessToken(), "syncLocationCb", Constants.CACHE_EXPIRE_BY_WEEK);  
		aq.ajax(cb);		
	}
	
	public void syncLocationCb(String url, JSONObject jo, AjaxStatus status) throws JSONException{
		AQUtility.debug(jo);
		AQUtility.debug(status.getCode());
		AQUtility.debug("status.getSource()", status.getSource());
		
		if (status.getCode() == 200){
			
			if(status.getSource() != AjaxStatus.FILE){
				Gson gson = new GsonBuilder().setExclusionStrategies(new RemoveUnnecessaryField()).create();
				List<Location> locations = new ArrayList<Location>();
				locations = Arrays.asList(gson.fromJson(JsonUtil.getJSONArray("result", jo).toString(), Location[].class));
				
				for(Location location: locations){
					
					List<Location>  foundLocations = Location.find(Location.class, "location_id=?", new String[] { location.locationId }, "", "", "1");
					
					if(foundLocations.size() == 0){
						location.save();
					}else{
						Location updateLocation = foundLocations.get(0);
						updateLocation = location.updateLocation(updateLocation.getId(), location);
						updateLocation.save();
					}
				}
			}

		}else{
			AQUtility.debug("Error", status.getCode() + " " + "Status:" + status.getMessage());
		}	
	}	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    AQUtility.debug("clicked");
		switch (item.getItemId()) {
	    case android.R.id.home:
	    	if (menu.isMenuShowing()) {
	    		menu.showContent();
			} else {
				menu.showMenu();
			}	    	
	        return true;
	    case R.id.mnu_search:
	    	AQUtility.debug("clicked");
	    	searchFragment = new SearchActivity();
	    	fm = getSupportFragmentManager();
	    	ft = fm.beginTransaction();
	    	ft.add(R.id.overlay_fragment_container, searchFragment);
	    	ft.addToBackStack(null);
	    	ft.commit();
	    	isSearchActive = true;
	    	return true;
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}
		
	@Override
	public void onBackPressed() {
		if(isSearchActive){
			isSearchActive = false;
			getSupportActionBar().show();
			getSupportFragmentManager().beginTransaction().remove(searchFragment).commit();
			return;
		}		
		
		if (menu.isMenuShowing()) {
			menu.showContent();
		} else {
			
			alertUtil.alertConfirmation(
					getString(R.string.app_name),
					getString(R.string.exit_message),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface arg0, int arg1) {
							finish();
						}
					}, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface arg0, int arg1) {
							
						}
					});							
		}
	}	
	
	public void onEvent(CategorySelectedEvent event) {
		
		if(event.object !=null){
			DealMenuChildItem category = (DealMenuChildItem) event.object;
			categoryId = category.categoryId;
			loader.setVisibility(View.GONE);
			empty_view.setVisibility(View.GONE);
			pull_to_refresh_list_view.setVisibility(View.VISIBLE);
			adapter.clear();
			
			String title = category.name;
			
			if(title.length() ==0){
				title = getString(R.string.app_name);
			}
			
			setUpHeader(title, null, true, true, false, null);
			refreshDeals();
			menu.showContent();
			
			if(!TextUtils.isEmpty(category.name)){
				flurryLogEvent(category.name);
			}
			
		}else{
			Thread splashTread = new Thread() {
	            @Override
	            public void run() {
	                try {
	                    synchronized(this){
	                    	wait(1000);
	                    }
	                } catch(InterruptedException e) {}
	                finally {
	                	
	                	runOnUiThread(new Runnable() {
							
							@Override
							public void run() {
								menu.showContent();
							}
						});
	                	
	                }
	            }
	        };
	        splashTread.start();
		}
    }
	
	public void onEvent(ItemSelectedEvent event) {
		if(event.object !=null){
			if(event.object instanceof String){
				flurryLogEvent((String) event.object);
			}
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	  super.onActivityResult(requestCode, resultCode, data);
	  if (resultCode != 0){
		  Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data); 
	  }  
	}
	
	private void fbLogin(){
		Session.openActiveSession(this, true, new Session.StatusCallback() {
			@Override
			public void call(Session session, SessionState state, Exception exception) {
				AQUtility.debug("FB TOKEN",session.getAccessToken());
				AQUtility.debug("FB EXPIRY DATE",session.getExpirationDate());
				
				updateFbExpiryDate(session);
				
			}
		});
	}
	
	private void updateFbExpiryDate(Session session){
		String expiryDate = "";
		try {
			expiryDate = URLEncoder.encode(session.getExpirationDate().toString(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		AjaxCallback<JSONObject> cb = aqHelper.ajaxCallback(Constants.FB_TOKEN_EXPIRATION_UPDATE + "?access_token="+GDeals.appAccessToken()+"&fb_access_token="+session.getAccessToken()+"&fb_access_token_expiry_date="+ expiryDate, "updateFbExpiryDateCb", Constants.CACHE_RESET);  
		aq.ajax(cb);			
	}
	
	public void updateFbExpiryDateCb(String url, JSONObject jo, AjaxStatus status) throws JSONException{
		AQUtility.debug(jo);
		AQUtility.debug(status.getCode());
		
		if (status.getCode() == 200){
			
		}else{
			AQUtility.debug("Error", status.getCode() + " " + "Status:" + status.getMessage());
		}
		getDeals();	
	}
	
	@Override
	protected void onDestroy() {
		EventBus.getDefault().unregister(this);
		super.onDestroy();
	}

}
